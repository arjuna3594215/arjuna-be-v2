package helper

import (
	"fmt"
	"math/rand"
	"mime/multipart"
	"github.com/gofiber/fiber/v2"

	"context"
	"io"
	"log"
	"strings"

	"cloud.google.com/go/storage"
	"google.golang.org/api/option"
)

var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func RandStringRunes(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

// func UploadFile(path string, file *multipart.FileHeader, c *fiber.Ctx) (string, error) {
// 	FilesName := RandStringRunes(5) + file.Filename
// 	// Save the files
// 	if err := c.SaveFile(file, fmt.Sprintf(path+"/%s", FilesName)); err != nil {
// 		return "", err
// 	}
	 
// 	return FilesName, nil
// }


func UploadFile(path string, fileHeader *multipart.FileHeader, c *fiber.Ctx) (string, error) {

	 
	// Set your Google Cloud Storage bucket name.
	 bucketName := "arjuna-files"

	// Set the path to the file you want to upload.
	// filePath := "1602481332457.jpeg"
	ctx := context.Background()

	// Path to your service account key JSON file.
	serviceAccountKeyPath := "arjuna-383107-d581642ee7fc.json"

	// Create a new Google Cloud Storage client with the service account key.
	client, err := storage.NewClient(ctx, option.WithCredentialsFile(serviceAccountKeyPath))
	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}
	// Create a new bucket handle.
	bucket := client.Bucket(bucketName)

	//file
	file, _ := fileHeader.Open()
	fileName := strings.Replace(fileHeader.Filename, " ", "_", -1)
	// Create a new object handle.
	object := bucket.Object(path + fileName)

	// Create a writer to upload the file.
	writer := object.NewWriter(ctx)

	// Copy the file's content to the writer.
	if _, err := io.Copy(writer, file); err != nil {
		log.Fatalf("Failed to upload file: %v", err)
	}

	// Close the writer to finish the upload.
	if err := writer.Close(); err != nil {
		log.Fatalf("Failed to close writer: %v", err)
	}
	// path = fmt.Sprintf("https://storage.googleapis.com/%s/%s/%s", bucketName, path,fileName)
	path = fmt.Sprintf(fileName)
	return path, nil
}


func UploadFileImage(path string, fileName string, file io.Reader) (string, error) {
	// Set your Google Cloud Storage bucket name.
	bucketName := "arjuna-files"

	ctx := context.Background()

	// Path to your service account key JSON file.
	serviceAccountKeyPath := "arjuna-383107-d581642ee7fc.json"

	// Create a new Google Cloud Storage client with the service account key.
	client, err := storage.NewClient(ctx, option.WithCredentialsFile(serviceAccountKeyPath))
	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}

	// Create a new bucket handle.
	bucket := client.Bucket(bucketName)

	// Create a new object handle.
	object := bucket.Object(path + fileName)

	// Create a writer to upload the file.
	writer := object.NewWriter(ctx)

	// Copy the file's content to the writer.
	if _, err := io.Copy(writer, file); err != nil {
		log.Fatalf("Failed to upload file: %v", err)
	}

	// Close the writer to finish the upload.
	if err := writer.Close(); err != nil {
		log.Fatalf("Failed to close writer: %v", err)
	}

	// path = fmt.Sprintf("https://storage.googleapis.com/%s/%s/%s", bucketName, path, fileName)
	path = fmt.Sprintf(fileName)
	return path, nil
}

func UploadFileXls(path string, fileName string, fileXLS []byte) (string, error) {
	// Set your Google Cloud Storage bucket name.
	bucketName := "arjuna-files"

	ctx := context.Background()

	// Path to your service account key JSON file.
	serviceAccountKeyPath := "arjuna-383107-d581642ee7fc.json"

	// Create a new Google Cloud Storage client with the service account key.
	client, err := storage.NewClient(ctx, option.WithCredentialsFile(serviceAccountKeyPath))
	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}

	// Create a new bucket handle.
	bucket := client.Bucket(bucketName)

	// Create a new object handle.
	object := bucket.Object(path + fileName)

	// Create a writer to upload the file.
	writer := object.NewWriter(ctx)

	// Write the Excel bytes directly to the writer.
	if _, err := writer.Write(fileXLS); err != nil {
		log.Fatalf("Failed to write Excel data to writer: %v", err)
	}

	// Close the writer to finish the upload.
	if err := writer.Close(); err != nil {
		log.Fatalf("Failed to close writer: %v", err)
	}

	// path = fmt.Sprintf("https://storage.googleapis.com/%s/%s/%s", bucketName, path, fileName)
	path = fmt.Sprintf(fileName)
	return path, nil
}


 

