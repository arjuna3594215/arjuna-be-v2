package helper

import "strings"

type Response struct {
	Status  bool        `json:"status"`
	Message string      `json:"message"`
	Errors  interface{} `json:"error"`
	Data    interface{} `json:"data"`
}

type EmptyObj struct{}

// Response Success
func BuildResponse(status bool, message string, data interface{}) Response {
	res := Response{
		Status:  status,
		Message: message,
		Errors:  nil,
		Data:    data,
	}
	return res
}

// Response Error
func BuildErrorResponse(message string, err string, data interface{}) Response {
	splittedError := strings.Split(err, "/n")
	res := Response{
		Status:  false,
		Message: message,
		Errors:  splittedError,
		Data:    data,
	}
	return res
}

// func BuildValidationResponse(e interface{}) interface{} {
// 	res := map[string]interface{}{"validationError": e}
// 	return res
// }
