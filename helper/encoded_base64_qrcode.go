package helper

import (
	"errors"
	"github.com/skip2/go-qrcode"
	"io/ioutil"
	"strconv"
)

func TextToQrCode(dataUrl string, IdIdentitasJurnal int) error {
	qrData := strconv.Itoa(IdIdentitasJurnal) + "-" + dataUrl

	// generate qr code
	qrCode, err := qrcode.New(dataUrl, qrcode.Medium)
	if err != nil {
		return errors.New("error when create qrcode: " + err.Error())
	}

	// save qr code image
	img, err := qrCode.PNG(256)
	if err != nil {
		return errors.New("error" + err.Error())
	}

	// save file to directory
	err = ioutil.WriteFile("files/qrcode/"+qrData+".png", img, 0644)
	if err != nil {
		return errors.New("error when save file: " + err.Error())
	}
	return nil
}
