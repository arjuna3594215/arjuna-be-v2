package controller

import (
	"arjuna-api/entity"
	"arjuna-api/helper"
	"arjuna-api/service"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
)

// Lists User  	godoc
// @Security 				bearerAuth
// @Summary      		Lists User
// @Description  		Lists User API calls
// @Tags         		User
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		search query string false "Search By Nama / Username"
// @Param        		kewenangan query string false "Sort By Kewenangan"
// @Param        		page query string true "Page"
// @Param        		row query string true "Row"
// @Param        		active query string true "Sort By Active user" Enums(1, 0)
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/user [get]
func GetAllUser(c *fiber.Ctx) error {
	search := c.Query("search")
	kewenangan := c.Query("kewenangan")
	page := c.Query("page")
	row := c.Query("row")
	active := c.Query("active")
	data, err := service.ListUser(search, kewenangan, page, row, active)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Save User  			godoc
// @Security 				bearerAuth
// @Summary      		Save User
// @Description  		Save User API calls
// @Tags         		User
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		user body entity.UserDTO true "Save User"
// @Success      		200  {object}  helper.Response
// @Failure      		422  {object}  helper.EmptyObj
// @Failure      		401  {object}  helper.Response
// @Failure      		400  {object}  helper.Response
// @Router       		/user [post]
func SaveUser(c *fiber.Ctx) error {
	// Body To Struct
	userDto := new(entity.UserDTO)
	if err := c.BodyParser(userDto); err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	// Validasi
	validate := validator.New()
	err := validate.Struct(userDto)
	if err != nil {
		res := helper.BuildValidationResponse(err)
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}
	// Check Duplicate Email Personal
	if !service.CheckDuplicateEmailPersonal(userDto.Email) {
		res := helper.BuildSingleValidationResponse("Email", "duplicate", "")
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}
	// Check Duplicate Username Pengguna
	if !service.CheckDuplicateUsernamePengguna(userDto.Username) {
		res := helper.BuildSingleValidationResponse("Username", "duplicate", "")
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}

	// Insert User
	data, err := service.InsertUser(*userDto)
	if err != nil {
		res := helper.BuildErrorResponse("Create User Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	res := helper.BuildResponse(true, "Create User Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Detail User  	godoc
// @Security 				bearerAuth
// @Summary      		Detail User
// @Description  		Detail User API calls
// @Tags         		User
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		id path int true "IDPengguna"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Success      		404  {object}  helper.EmptyObj
// @Router       		/user/{id} [get]
func GetUser(c *fiber.Ctx) error {
	id, err := c.ParamsInt("id")
	data, err := service.ShowUser(id)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	if v, ok := data.(entity.User); ok {
		res := helper.BuildResponse(true, "Success", v)
		return c.Status(fiber.StatusOK).JSON(res)
	}
	res := helper.BuildErrorResponse("Not Found", err.Error(), helper.EmptyObj{})
	return c.Status(fiber.StatusNotFound).JSON(res)
}

// Update User  			godoc
// @Security 				bearerAuth
// @Summary      		Update User
// @Description  		Update User API calls
// @Tags         		User
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param  					id path int true "ID Pengguna"
// @Param        		user body entity.UserUpdateDTO false "Update User"
// @Success      		200  {object}  helper.Response
// @Failure      		422  {object}  helper.EmptyObj
// @Failure      		401  {object}  helper.Response
// @Failure      		400  {object}  helper.Response
// @Failure      		404  {object}  helper.Response
// @Router       		/user/{id} [put]
func UpdateUser(c *fiber.Ctx) error {
	id, _ := c.ParamsInt("id")
	// Body To Struct
	userDto := new(entity.UserUpdateDTO)
	if err := c.BodyParser(userDto); err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	// Validasi
	validate := validator.New()
	err := validate.Struct(userDto)
	if err != nil {
		res := helper.BuildValidationResponse(err)
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}

	dataUser, err := service.ShowUser(id)
	if v, ok := dataUser.(entity.User); ok {
		if v.Email != userDto.Email {
			// Check Duplicate Email Personal
			if !service.CheckDuplicateEmailPersonal(userDto.Email) {
				res := helper.BuildSingleValidationResponse("Email", "duplicate", "")
				return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
			}
		}

		// Update User
		data, err := service.UpdateUser(*userDto, int(v.IDPengguna), int(v.IDPersonal))
		if err != nil {
			res := helper.BuildErrorResponse("Update User Failed", err.Error(), helper.EmptyObj{})
			return c.Status(fiber.StatusBadRequest).JSON(res)
		}
		res := helper.BuildResponse(true, "Update User Success", data)
		return c.Status(fiber.StatusOK).JSON(res)
	}
	res := helper.BuildErrorResponse("Not Found", err.Error(), helper.EmptyObj{})
	return c.Status(fiber.StatusNotFound).JSON(res)
}

// Update Kewenangan User  			godoc
// @Security 				bearerAuth
// @Summary      		Update Kewenangan User
// @Description  		Update Kewenangan User API calls
// @Tags         		User
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param  					id path int true "ID Pengguna"
// @Param        		user body entity.KewenanganUserDTO false "Update Kewenangan User"
// @Success      		200  {object}  helper.Response
// @Failure      		422  {object}  helper.EmptyObj
// @Failure      		401  {object}  helper.Response
// @Failure      		400  {object}  helper.Response
// @Failure      		404  {object}  helper.Response
// @Router       		/user/kewenangan/{id} [post]
func UpdateKewenanganUser(c *fiber.Ctx) error {
	id, _ := c.ParamsInt("id")
	// Body To Struct
	userDto := new(entity.KewenanganUserDTO)
	if err := c.BodyParser(userDto); err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	// Validasi
	validate := validator.New()
	err := validate.Struct(userDto)
	if err != nil {
		res := helper.BuildValidationResponse(err)
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}

	dataUser, err := service.ShowUser(id)
	if v, ok := dataUser.(entity.User); ok {

		// Update kewenagan User
		data, err := service.UpdateKewenanganUser(*userDto, int(v.IDPengguna))
		if err != nil {
			res := helper.BuildErrorResponse("Update Kewenangan User Failed", err.Error(), helper.EmptyObj{})
			return c.Status(fiber.StatusBadRequest).JSON(res)
		}
		res := helper.BuildResponse(true, "Update Kewenangan User Success", data)
		return c.Status(fiber.StatusOK).JSON(res)
	}
	res := helper.BuildErrorResponse("Not Found", err.Error(), helper.EmptyObj{})
	return c.Status(fiber.StatusNotFound).JSON(res)
}

// Lists User Assessor  	godoc
// @Security 				bearerAuth
// @Summary      		Lists User Assessor
// @Description  		Lists User Assessor API calls
// @Tags         		User
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		search query string false "Search By Nama / Bidang Ilmu"
// @Param        		kewenangan query string true "Sort By Kewenangan 4: Issue, 5: Manajemen, 7: Evaluator" Enums(4, 5, 7)
// @Param        		page query string true "Page"
// @Param        		row query string true "Row"
// @Param        		filter query string true "0: sort by name, 1: sort by bidang ilmu" Enums(0, 1)
// @Param        		id_usulan_akreditasi query string true "ID Usulan Akreditasi"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/user/assessor [get]
func GetAllUserAssessor(c *fiber.Ctx) error {
	search := c.Query("search")
	kewenangan := c.Query("kewenangan")
	page := c.Query("page")
	row := c.Query("row")
	filter := c.Query("filter")
	id_usulan_akreditasi := c.Query("id_usulan_akreditasi")
	data, err := service.ListUserAssessor(search, kewenangan, page, row, filter, id_usulan_akreditasi)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}
