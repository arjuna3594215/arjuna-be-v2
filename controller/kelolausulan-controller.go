package controller

import (
	"arjuna-api/entity"
	"arjuna-api/helper"
	"arjuna-api/service"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
)

// Lists Usulan Akreditasi Baru  	godoc
// @Security 				bearerAuth
// @Summary      		Lists Usulan Akreditasi Baru
// @Description  		Lists Usulan Akreditasi Baru API calls
// @Tags         		Kelola Usulan
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		search query string false "Search By Nama Jurnal"
// @Param        		status query string true "Sort BY Status (1: >= 70 | 0: <70) " Enums(1, 0)
// @Param        		page query string true "Page"
// @Param        		row query string true "Row"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/kelola-usulan/akreditasi-baru [get]
func GetAllUsulanAkreditasiBaru(c *fiber.Ctx) error {
	search := c.Query("search")
	status := c.Query("status")
	page := c.Query("page")
	row := c.Query("row")
	data, err := service.ListUsulanAkreditasiBaru(search, page, row, status)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Detail Usulan Akreditasi Baru  	godoc
// @Security 				bearerAuth
// @Summary      		Detail Usulan Akreditasi Baru
// @Description  		Detail Usulan Akreditasi Baru API calls
// @Tags         		Kelola Usulan
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		id path int true "ID Usulan Akreditasi"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/kelola-usulan/akreditasi-baru/{id} [get]
func GetDetailUsulanAkreditasiBaru(c *fiber.Ctx) error {
	id, _ := c.ParamsInt("id")
	data, err := service.DetailUsulanAkreditasiBaru(id)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	if v, ok := data.(entity.DetailUsulanAkreditasiBaru); ok {
		res := helper.BuildResponse(true, "Success", v)
		return c.Status(fiber.StatusOK).JSON(res)
	}
	res := helper.BuildErrorResponse("Not Found", err.Error(), helper.EmptyObj{})
	return c.Status(fiber.StatusNotFound).JSON(res)
}

// Usulan Action  			godoc
// @Security 				bearerAuth
// @Summary      		Usulan Action
// @Description  		Usulan Action API calls
// @Tags         		Kelola Usulan
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		usulan_action body entity.UsulanActionDTO true "Usulan Action"
// @Success      		200  {object}  helper.Response
// @Failure      		422  {object}  helper.EmptyObj
// @Failure      		401  {object}  helper.Response
// @Failure      		400  {object}  helper.Response
// @Router       		/kelola-usulan/akreditasi-baru [post]
func UsulanAction(c *fiber.Ctx) error {
	// Body To Struct
	usulanactionDto := new(entity.UsulanActionDTO)
	if err := c.BodyParser(usulanactionDto); err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	// Validasi
	validate := validator.New()
	err := validate.Struct(usulanactionDto)
	if err != nil {
		res := helper.BuildValidationResponse(err)
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}

	// Usulan Action
	id_personal := service.GetIDPersonalAuthenticated(c)
	data, err := service.UsulanAction(*usulanactionDto, id_personal)
	if err != nil {
		res := helper.BuildErrorResponse("Create Usulan Action Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	res := helper.BuildResponse(true, "Create Usulan Action Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Usulan Cancel Action  			godoc
// @Security 				bearerAuth
// @Summary      		Usulan Cancel Action
// @Description  		Usulan Cancel Action API calls
// @Tags         		Kelola Usulan
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		usulan_cancel_action body entity.UsulanActionDTO true "Usulan Cancel Action"
// @Success      		200  {object}  helper.Response
// @Failure      		422  {object}  helper.EmptyObj
// @Failure      		401  {object}  helper.Response
// @Failure      		400  {object}  helper.Response
// @Router       		/kelola-usulan/akreditasi-baru/cancel [post]
func UsulanCancelAction(c *fiber.Ctx) error {
	// Body To Struct
	usulanactionDto := new(entity.UsulanActionDTO)
	if err := c.BodyParser(usulanactionDto); err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	// Validasi
	validate := validator.New()
	err := validate.Struct(usulanactionDto)
	if err != nil {
		res := helper.BuildValidationResponse(err)
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}

	// Usulan Action
	id_personal := service.GetIDPersonalAuthenticated(c)
	data, err := service.UsulanCancelAction(*usulanactionDto, id_personal)
	if err != nil {
		res := helper.BuildErrorResponse("Create Usulan Cancel Action Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	res := helper.BuildResponse(true, "Create Usulan Cancel Action Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

//======================================================================================================================

// Lists Evaluasi Dokumen  	godoc
// @Security 				bearerAuth
// @Summary      		Lists Evaluasi Dokumen
// @Description  		Lists Evaluasi Dokumen API calls
// @Tags         		Kelola Usulan
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		search query string false "Search By Nama Jurnal"
// @Param        		status query string true "Sort BY Status (1: >= 70 | 0: <70) " Enums(1, 0)
//// @Param        		evaluasi query string true "Sort BY Status (1: Lulus | 0: Tidak Lulus) " Enums(1, 0)
// @Param        		page query string true "Page"
// @Param        		row query string true "Row"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/kelola-usulan/evaluasi-dokumen [get]
func GetAllEvaluasiDokumen(c *fiber.Ctx) error {
	search := c.Query("search")
	status := c.Query("status")
	evaluasi := c.Query("evaluasi")
	page := c.Query("page")
	row := c.Query("row")
	data, err := service.ListEvaluasiDokumen(search, page, row, status, evaluasi)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Lists Distribusi Penilaian  	godoc
// @Security 				bearerAuth
// @Summary      		Lists Distribusi Penilaian
// @Description  		Lists Distribusi Penilaian API calls
// @Tags         		Kelola Usulan
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		search query string false "Search By Nama Jurnal"
// @Param        		status query string true "Sort BY Status (1: >= 70 | 0: <70) " Enums(1, 0)
// @Param        		page query string true "Page"
// @Param        		row query string true "Row"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/kelola-usulan/distribusi-penilaian [get]
func GetAllDistribusiPenilaian(c *fiber.Ctx) error {
	search := c.Query("search")
	status := c.Query("status")
	page := c.Query("page")
	row := c.Query("row")
	data, err := service.ListDistribusiPenilaian(search, page, row, status)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Lists Hasil Akhir Penilaian  	godoc
// @Security 				bearerAuth
// @Summary      		Lists Hasil Akhir Penilaian
// @Description  		Lists Hasil Akhir Penilaian API calls
// @Tags         		Kelola Usulan
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		search query string false "Search By Nama Jurnal"
// @Param        		status query string true "Sort BY Status (1: >= 70 | 0: <70) " Enums(1, 0)
// @Param        		page query string true "Page"
// @Param        		row query string true "Row"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/kelola-usulan/hasil-akhir-penilaian [get]
func GetAllHasiAkhirPenilaian(c *fiber.Ctx) error {
	search := c.Query("search")
	status := c.Query("status")
	page := c.Query("page")
	row := c.Query("row")
	data, err := service.ListsHasilAkhirPenilaian(search, page, row, status)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Lists Hasil Akreditasi  	godoc
// @Security 				bearerAuth
// @Summary      		Lists Hasil Akreditasi
// @Description  		Lists Hasil Akreditasi API calls
// @Tags         		Kelola Usulan
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		search query string false "Search By Nama Jurnal"
// @Param        		status query string true "Sort BY Status (1: >= 70 | 0: <70) " Enums(1, 0)
// @Param        		page query string true "Page"
// @Param        		row query string true "Row"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/kelola-usulan/hasil-akreditasi [get]
func GetAllHasiAkreditasi(c *fiber.Ctx) error {
	search := c.Query("search")
	status := c.Query("status")
	page := c.Query("page")
	row := c.Query("row")
	data, err := service.ListsHasilAkreditasi(search, page, row, status)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Detail Hasil Akreditasi  	godoc
// @Security 				bearerAuth
// @Summary      		Detail Hasil Akreditasi
// @Description  		Detail Hasil Akreditasi API calls
// @Tags         		Kelola Usulan
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		id path int true "ID SK Akreditasi"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/kelola-usulan/hasil-akreditasi/{id} [get]
func GetDetailHasilAkreditasi(c *fiber.Ctx) error {
	id, _ := c.ParamsInt("id")
	data, err := service.DetailHasilAkreditasi(id)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	if v, ok := data.(entity.DetailHasilAkreditasi); ok {
		res := helper.BuildResponse(true, "Success", v)
		return c.Status(fiber.StatusOK).JSON(res)
	}
	res := helper.BuildErrorResponse("Not Found", err.Error(), helper.EmptyObj{})
	return c.Status(fiber.StatusNotFound).JSON(res)
}

// Hasil Akreditasi Permanent  	godoc
// @Security 				bearerAuth
// @Summary      		Hasil Akreditasi Permanent
// @Description  		Hasil Akreditasi Permanent API calls
// @Tags         		Kelola Usulan
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		id path int true "ID Usulan Akreditasi"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/kelola-usulan/hasil-akreditasi/{id}/permanent [put]
func HasilAkreditasiPermanent(c *fiber.Ctx) error {
	id, _ := c.ParamsInt("id")
	data, err := service.HasilAkreditasiPermanent(id)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Hasil Akreditasi Permanent All Assesment  	godoc
// @Security 				bearerAuth
// @Summary      		Hasil Akreditasi Permanent All Assesment
// @Description  		Hasil Akreditasi Permanent All Assesment API calls
// @Tags         		Kelola Usulan
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		id path int true "ID Usulan Akreditasi"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/kelola-usulan/hasil-akreditasi/permanent-all-assesment [put]
func HasilAkreditasiPermanentAllAssesment(c *fiber.Ctx) error {
	data, err := service.HasilAkreditasiPermanentAllAssesment()
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Hasil Akreditasi Cancel Permanent  	godoc
// @Security 				bearerAuth
// @Summary      		Hasil Akreditasi Cancel Permanent
// @Description  		Hasil Akreditasi Cancel Permanent API calls
// @Tags         		Kelola Usulan
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		id path int true "ID Usulan Akreditasi"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/kelola-usulan/hasil-akreditasi/{id}/cancel-permanent [put]
func HasilAkreditasiCancelPermanent(c *fiber.Ctx) error {
	id, _ := c.ParamsInt("id")
	data, err := service.HasilAkreditasiCancelPermanent(id)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

//=============================================================================================================================

// Lists Progres Penilaian  	godoc
// @Security 				bearerAuth
// @Summary      		Lists Progres Penilaian
// @Description  		Lists Progres Penilaian API calls
// @Tags         		Kelola Usulan
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		search query string false "Search By Nama Jurnal"
// @Param        		status query string true "Sort BY Status (1: >= 70 | 0: <70) " Enums(1, 0)
// @Param        		proses query string true "Sort By Proses Penilai" Enums(Semua Proses, Belum Dinilai, Perlu Penyesuaian, Siap Permanen)
// @Param        		page query string true "Page"
// @Param        		row query string true "Row"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/kelola-usulan/progres-penilaian [get]
func GetAllProgresPenilaian(c *fiber.Ctx) error {
	search := c.Query("search")
	status := c.Query("status")
	page := c.Query("page")
	row := c.Query("row")
	proses := c.Query("proses")
	data, err := service.ListsProgresPenilaian(search, page, row, status, proses)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Lists Detail Penilaian Manajemen  	godoc
// @Security 				bearerAuth
// @Summary      		Lists Detail Penilaian Manajemen
// @Description  		Lists Detail Penilaian Manajemen API calls
// @Tags         		Kelola Usulan
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		id path int true "ID Usulan Akreditasi"
// @Param        	  	lang query string true "Language" Enums(en, id)
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/kelola-usulan/penilaian-manajemen/{id} [get]
func DetailPenilaianManajemen(c *fiber.Ctx) error {
	id, _ := c.ParamsInt("id")
	lang := c.Query("lang")

	data, err := service.DetailListHasilPenilaianManajemen(id, lang)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Lists Detail Penilaian Substansi  	godoc
// @Security 				bearerAuth
// @Summary      		Lists Detail Penilaian Substansi
// @Description  		Lists Detail Penilaian Substansi API calls
// @Tags         		Kelola Usulan
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		id path int true "ID Usulan Akreditasi"
// @Param        	  	lang query string true "Language" Enums(en, id)
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/kelola-usulan/penilaian-substansi/{id} [get]
func DetailPenilaianSubstansi(c *fiber.Ctx) error {
	id, _ := c.ParamsInt("id")
	lang := c.Query("lang")

	data, err := service.DetailListHasilPenilaianSubstansi(id, lang)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Detail Progres Penilaian  	godoc
// @Security 				bearerAuth
// @Summary      		Detail Progres Penilaian
// @Description  		Detail Progres Penilaian API calls
// @Tags         		Kelola Usulan
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		id path int true "ID Usulan Akreditasi"
// @Param        		penilai query string true "Penilai" Enums(Management, Issue)
// @Param        		status query string true "Type" Enums(Progres, Revoke)
// @Param        		id_personal query string true "ID Personal"
// @Param        		page query string true "Page"
// @Param        		row query string true "Row"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/kelola-usulan/progres-penilaian/{id} [get]
func GetDetailProgresPenilaian(c *fiber.Ctx) error {
	id_personal := c.Query("id_personal")
	status := c.Query("status")
	penilai := c.Query("penilai")
	id, _ := c.ParamsInt("id")
	row := c.Query("row")
	page := c.Query("page")
	if penilai == "Management" {
		data, err := service.GetDetailProgresPenilaianManagement(id, row, page, id_personal, status)
		if err != nil {
			res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
			return c.Status(fiber.StatusInternalServerError).JSON(res)
		}
		res := helper.BuildResponse(true, "Success", data)
		return c.Status(fiber.StatusOK).JSON(res)
	} else if penilai == "Issue" {
		data, err := service.GetDetailProgresPenilaianIssue(id, row, page, id_personal, status)
		if err != nil {
			res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
			return c.Status(fiber.StatusInternalServerError).JSON(res)
		}
		res := helper.BuildResponse(true, "Success", data)
		return c.Status(fiber.StatusOK).JSON(res)
	}
	res := helper.BuildErrorResponse("Not Found", "Penilai Harus Management atau Issue", helper.EmptyObj{})
	return c.Status(fiber.StatusNotFound).JSON(res)
}

//  Penilaian Set Permanent  			godoc
// @Security 				bearerAuth
// @Summary      		Progres Penilaian Set Permanent
// @Description  		Progres Penilaian Set Permanent API calls
// @Tags         		Kelola Usulan
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		set_permanenr body entity.ProgresPenilaianSetPermanentDTO true "Progres Penilaian Set Permanent"
// @Success      		200  {object}  helper.Response
// @Failure      		422  {object}  helper.EmptyObj
// @Failure      		401  {object}  helper.Response
// @Failure      		400  {object}  helper.Response
// @Router       		/kelola-usulan/progres-penilaian/set-permanent [post]
func ProgresPenilaianSetPermanent(c *fiber.Ctx) error {
	// Body To Struct
	request := new(entity.ProgresPenilaianSetPermanentDTO)
	if err := c.BodyParser(request); err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	// Validasi
	validate := validator.New()
	err := validate.Struct(request)
	if err != nil {
		res := helper.BuildValidationResponse(err)
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}

	if request.Penilai == "Issue" {
		data_count, err := service.CountUnsurPenilaian(*request)
		if err != nil {
			res := helper.BuildErrorResponse("Set Permanent Action Failed", err.Error(), helper.EmptyObj{})
			return c.Status(fiber.StatusBadRequest).JSON(res)
		}
		if data_count < 11 {
			res := helper.BuildErrorResponse("Set Permanent Action Failed", "Unsur yang di nilai harus lebih dari 11", helper.EmptyObj{})
			return c.Status(fiber.StatusBadRequest).JSON(res)
		}
	}

	// Progres Penilaian Set Permanent Action
	data, err := service.ProgresPenilaianSetPermanent(*request)
	if err != nil {
		res := helper.BuildErrorResponse("Set Permanent Action Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	res := helper.BuildResponse(true, "Set Permanent Action Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Penilaian Cabut Permanent  			godoc
// @Security 				bearerAuth
// @Summary      		Progres Penilaian Cabut Permanent
// @Description  		Progres Penilaian Cabut Permanent API calls
// @Tags         		Kelola Usulan
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		cabut_permanenr body entity.ProgresPenilaianSetPermanentDTO true "Progres Penilaian Cabut Permanent"
// @Success      		200  {object}  helper.Response
// @Failure      		422  {object}  helper.EmptyObj
// @Failure      		401  {object}  helper.Response
// @Failure      		400  {object}  helper.Response
// @Router       		/kelola-usulan/progres-penilaian/cabut-permanent [post]
func ProgresPenilaianCabutPermanent(c *fiber.Ctx) error {
	// Body To Struct
	request := new(entity.ProgresPenilaianSetPermanentDTO)
	if err := c.BodyParser(request); err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	// Validasi
	validate := validator.New()
	err := validate.Struct(request)
	if err != nil {
		res := helper.BuildValidationResponse(err)
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}

	// if request.Penilai == "Issue" {
	// 	data_count, err := service.CountUnsurPenilaian(*request)
	// 	if err != nil {
	// 		res := helper.BuildErrorResponse("Set Permanent Action Failed", err.Error(), helper.EmptyObj{})
	// 		return c.Status(fiber.StatusBadRequest).JSON(res)
	// 	}
	// 	if data_count < 11 {
	// 		res := helper.BuildErrorResponse("Set Permanent Action Failed", "Unsur yang di nilai harus lebih dari 11", helper.EmptyObj{})
	// 		return c.Status(fiber.StatusBadRequest).JSON(res)
	// 	}
	// }

	// Progres Penilaian Cabut Permanent Action
	data, err := service.ProgresPenilaianCabutPermanent(*request)
	if err != nil {
		res := helper.BuildErrorResponse("Cabut Permanent Action Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	res := helper.BuildResponse(true, "Cabut Permanent Action Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Progres Penilaian Penyesuaian Nilai  			godoc
// @Security 				bearerAuth
// @Summary      		Progres Penilaian Penyesuaian Nilai
// @Description  		Progres Penilaian Penyesuaian Nilai API calls
// @Tags         		Kelola Usulan
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		set_permanenr body entity.ProgresPenilaianPenyesuaianNilaiDTO true "Progres Penilaian Penyesuaian Nilai"
// @Success      		200  {object}  helper.Response
// @Failure      		422  {object}  helper.EmptyObj
// @Failure      		401  {object}  helper.Response
// @Failure      		400  {object}  helper.Response
// @Router       		/kelola-usulan/progres-penilaian/penyesuaian-nilai [post]
func ProgresPenilaianPenyesuaianNilai(c *fiber.Ctx) error {
	// Body To Struct
	request := new(entity.ProgresPenilaianPenyesuaianNilaiDTO)
	if err := c.BodyParser(request); err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	// Validasi
	validate := validator.New()
	err := validate.Struct(request)
	if err != nil {
		res := helper.BuildValidationResponse(err)
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}

	// Progres Penilaian Penyesuaian Nilai Action
	data, err := service.ProgresPenilaianPenyesuaianNilai(*request)
	if err != nil {
		res := helper.BuildErrorResponse("Penyesuaian Nilai Action Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	res := helper.BuildResponse(true, "Penyesuaian Nilai Action Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Progres Penilaian Penyesuaian Nilai Asessor  			godoc
// @Security 				bearerAuth
// @Summary      		Progres Penilaian Penyesuaian Nilai Asessor
// @Description  		Progres Penilaian Penyesuaian Nilai Asessor API calls
// @Tags         		Kelola Usulan
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		set_permanenr body entity.ProgresPenilaianPenyesuaianNilaiAsessorDTO true "Progres Penilaian Penyesuaian Nilai Asessor"
// @Success      		200  {object}  helper.Response
// @Failure      		422  {object}  helper.EmptyObj
// @Failure      		401  {object}  helper.Response
// @Failure      		400  {object}  helper.Response
// @Router       		/kelola-usulan/progres-penilaian/penyesuaian-nilai/asessor [post]
func ProgresPenilaianPenyesuaianNilaiAsessor(c *fiber.Ctx) error {
	// Body To Struct
	request := new(entity.ProgresPenilaianPenyesuaianNilaiAsessorDTO)
	if err := c.BodyParser(request); err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	// Validasi
	validate := validator.New()
	err := validate.Struct(request)
	if err != nil {
		res := helper.BuildValidationResponse(err)
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}

	// Progres Penilaian Penyesuaian Nilai Action
	data, err := service.ProgresPenilaianPenyesuaianNilaiAsessor(*request)
	if err != nil {
		res := helper.BuildErrorResponse("Penyesuaian Nilai Action Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	res := helper.BuildResponse(true, "Penyesuaian Nilai Action Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// ListUsulanItemPenolakan  			godoc
// @Security 				bearerAuth
// @Summary      		List Usulan Item Penolakan
// @Description  		List Usulan Item Penolakan API calls
// @Tags         		Kelola Usulan
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Success      		200  {object}  helper.Response
// @Failure      		422  {object}  helper.EmptyObj
// @Failure      		401  {object}  helper.Response
// @Failure      		400  {object}  helper.Response
// @Router       		/kelola-usulan/akreditasi-baru/list-usulan-item-penolakan [get]
func ListUsulanItemPenolakan(c *fiber.Ctx) error {
	data, err := service.UsulanItemPenolakan()
	if err != nil {
		res := helper.BuildErrorResponse("List Usulan Item Penolakan Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	res := helper.BuildResponse(true, "List Usulan Item Penolakan Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Komentar Hasil Penilaian Akreditasi  	godoc
// @Security 				bearerAuth
// @Summary      		Komentar Hasil Penilaian Akreditasi
// @Description  		Komentar Hasil Penilaian Akreditasi API calls
// @Tags         		Kelola Usulan
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		id_personal query int true "ID Personal"
// @Param        		id_usulan_akreditasi query int true "ID Usulan Akreditasi"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/kelola-usulan/komentar-hasil-penilaian [get]
func GetKomentarHasilPenilaianAkreditasi(c *fiber.Ctx) error {

	idPersonal := c.Query("id_personal")
	idUsulanAkreditasi := c.Query("id_usulan_akreditasi")

	data, err := service.KomentarHasilPenilaianAkreditasi(idPersonal, idUsulanAkreditasi)
	if err != nil {
		res := helper.BuildErrorResponse("Get Komentar Hasil Penilaian Akreditasi Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	res := helper.BuildResponse(true, "Get Komentar Hasil Penilaian Akreditasi Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}
