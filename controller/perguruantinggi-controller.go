package controller

import (
	"arjuna-api/entity"
	"arjuna-api/helper"
	"arjuna-api/service"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
)

// Lists Perguruan Tinggi	godoc
// @Security 			bearerAuth
// @Summary      		Lists Perguruan Tinggi
// @Description  		Lists Perguruan Tinggi API calls
// @Tags         		Utility
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		search query string false "Search By PT"
// @Param        		page query string false "Page"
// @Param        		row query string false "Row"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/utility/perguruan-tinggi [get]
func GetAllPerguruanTinggi(c *fiber.Ctx) error {
	search := c.Query("search")
	row := c.Query("row")
	page := c.Query("page")

	data, err := service.ListPerguruanTinggi(search, page, row)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}

	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Create Perguruan Tinggi  godoc
// @Security 				bearerAuth
// @Summary      			Create Perguruan Tinggi
// @Description  			Create Perguruan Tinggi API calls
// @Tags         			Utility
// @Accept       			json
// @Consume      			json
// @Produce      			json
// @Param        			perguruan_tinggi body entity.PerguruanTinggiDTO true "Create Perguruan Tinggi"
// @Success      			200  {object}  helper.Response
// @Failure      			422  {object}  helper.EmptyObj{}
// @Failure      			401  {object}  helper.EmptyObj{}
// @Failure      			400  {object}  helper.EmptyObj{}
// @Router       			/utility/perguruan-tinggi [post]
func CreatePerguruanTinggi(c *fiber.Ctx) error {
	perguruanReq := new(entity.PerguruanTinggiDTO)
	if err := c.BodyParser(perguruanReq); err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}

	validate := validator.New()
	if err := validate.Struct(perguruanReq); err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}

	ok := service.InsertPerguruanTinggi(*perguruanReq)
	if !ok {
		res := helper.BuildErrorResponse("Failed", "Failed to create data", helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	res := helper.BuildResponse(true, "Create Perguruan Tinggi Success", ok)
	return c.Status(fiber.StatusOK).JSON(res)
}
