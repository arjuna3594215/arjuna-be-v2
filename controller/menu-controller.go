package controller

import (
	"arjuna-api/helper"
	"arjuna-api/service"

	"github.com/gofiber/fiber/v2"
)

// Lists Menu  	godoc
// @Security 				bearerAuth
// @Summary      		Lists Menu
// @Description  		Lists Menu API calls
// @Tags         		Menu
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/menu [get]
func GetAllMenu(c *fiber.Ctx) error {
	data, err := service.ListMenu()
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}
