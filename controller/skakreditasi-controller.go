package controller

import (
	"arjuna-api/entity"
	"arjuna-api/helper"
	"arjuna-api/service"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
)

// Lists SK Akreditasi  	godoc
// @Security 				bearerAuth
// @Summary      		Lists SK Akreditasi
// @Description  		Lists SK Akreditasi API calls
// @Tags         		SK Akreditasi
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		search query string false "Search"
// @Param        		status query string false "Sort BY Status" Enums(1, 0)
// @Param        		page query string true "Page"
// @Param        		row query string true "Row"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/sk-akreditasi [get]
func GetAllSKAkreditasi(c *fiber.Ctx) error {
	status := c.Query("status")
	page := c.Query("page")
	row := c.Query("row")
	search := c.Query("search")
	data, err := service.ListSKAkreditasi(page, row, status, search)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Get All Lists SK Akreditasi  	godoc
// @Security 				bearerAuth
// @Summary      		Get All Lists SK Akreditasi
// @Description  		Get All Lists SK Akreditasi API calls
// @Tags         		SK Akreditasi
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		status query string false "Sort BY Status" Enums(1, 0)
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/sk-akreditasi/all [get]
func GetAllListSKAkreditasi(c *fiber.Ctx) error {
	status := c.Query("status")
	data, err := service.GetAllListSKAkreditasi(status)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Create SK Akreditasi  			godoc
// @Security 				bearerAuth
// @Summary      		Create SK Akreditasi
// @Description  		Create SK Akreditasi API calls
// @Tags         		SK Akreditasi
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		sk_akreditasi body entity.SKAkreditasiDTO true "Update SK Akreditasi"
// @Success      		200  {object}  helper.Response
// @Failure      		422  {object}  helper.EmptyObj
// @Failure      		401  {object}  helper.Response
// @Failure      		400  {object}  helper.Response
// @Router       		/sk-akreditasi [post]
func CreateSKAkreditasi(c *fiber.Ctx) error {
	// Body To Struct
	SKAkreditasiDto := new(entity.SKAkreditasiDTO)
	if err := c.BodyParser(SKAkreditasiDto); err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}

	// Validasi
	validate := validator.New()
	err := validate.Struct(SKAkreditasiDto)
	if err != nil {
		res := helper.BuildValidationResponse(err)
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}

	// Check Duplicate No Sk
	if !service.CheckDuplicateNoSK(SKAkreditasiDto.NO_SK) {
		res := helper.BuildSingleValidationResponse("No SK", "duplicate", "")
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}

	// Create Akreditasi
	data, err := service.CreateSKAkreditasi(*SKAkreditasiDto)
	if err != nil {
		res := helper.BuildErrorResponse("Create SK Akreditasi Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	res := helper.BuildResponse(true, "Create SK Akreditasi Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Update SK Akreditasi  			godoc
// @Security 				bearerAuth
// @Summary      		Update SK Akreditasi
// @Description  		Update SK Akreditasi API calls
// @Tags         		SK Akreditasi
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		id path int true "ID SK Akreditasi"
// @Param        		sk_akreditasi body entity.SKAkreditasiDTO true "Update SK Akreditasi"
// @Success      		200  {object}  helper.Response
// @Failure      		422  {object}  helper.EmptyObj
// @Failure      		401  {object}  helper.Response
// @Failure      		400  {object}  helper.Response
// @Router       		/sk-akreditasi/{id} [post]
func UpdateSKAkreditasi(c *fiber.Ctx) error {
	id, _ := c.ParamsInt("id")
	// Body To Struct
	SKAkreditasiDto := new(entity.SKAkreditasiDTO)
	if err := c.BodyParser(SKAkreditasiDto); err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}

	// Validasi
	validate := validator.New()
	err := validate.Struct(SKAkreditasiDto)
	if err != nil {
		res := helper.BuildValidationResponse(err)
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}

	// Update Akreditasi
	data, err := service.UpdateSKAkreditasi(*SKAkreditasiDto, id)
	if err != nil {
		res := helper.BuildErrorResponse("Update SK Akreditasi Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	res := helper.BuildResponse(true, "Update SK Akreditasi Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Publish SK Akreditasi  			godoc
// @Security 				bearerAuth
// @Summary      		Publish SK Akreditasi
// @Description  		Publish SK Akreditasi API calls
// @Tags         		SK Akreditasi
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		id path int true "ID SK Akreditasi"
// @Success      		200  {object}  helper.Response
// @Failure      		422  {object}  helper.EmptyObj
// @Failure      		401  {object}  helper.Response
// @Failure      		400  {object}  helper.Response
// @Router       		/sk-akreditasi/publish/{id} [post]
func PublishSKAkreditasi(c *fiber.Ctx) error {
	id, _ := c.ParamsInt("id")

	data_sk, err := service.ShowSKAkreditasi(id)
	if err != nil {
		res := helper.BuildErrorResponse("Publish SK Akreditasi Not Found", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	if v, ok := data_sk.(entity.SKAkreditasi); ok {
		// Update Akreditasi
		data, err := service.PublishOrUnPublishSKAkreditasi(v.ID_SK_Akreditasi, "1")
		if err != nil {
			res := helper.BuildErrorResponse("Publish SK Akreditasi Failed", err.Error(), helper.EmptyObj{})
			return c.Status(fiber.StatusBadRequest).JSON(res)
		}
		res := helper.BuildResponse(true, "Publish SK Akreditasi Success", data)
		return c.Status(fiber.StatusOK).JSON(res)
	}
	res := helper.BuildErrorResponse("SK Akreditasi Not Found", err.Error(), helper.EmptyObj{})
	return c.Status(fiber.StatusBadRequest).JSON(res)
}

// UnPublish SK Akreditasi  			godoc
// @Security 				bearerAuth
// @Summary      		UnPublish SK Akreditasi
// @Description  		UnPublish SK Akreditasi API calls
// @Tags         		SK Akreditasi
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		id path int true "ID SK Akreditasi"
// @Success      		200  {object}  helper.Response
// @Failure      		422  {object}  helper.EmptyObj
// @Failure      		401  {object}  helper.Response
// @Failure      		400  {object}  helper.Response
// @Router       		/sk-akreditasi/unpublish/{id} [post]
func UnPublishSKAkreditasi(c *fiber.Ctx) error {
	id, _ := c.ParamsInt("id")

	data_sk, err := service.ShowSKAkreditasi(id)
	if err != nil {
		res := helper.BuildErrorResponse("UnPublish SK Akreditasi Not Found", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	if v, ok := data_sk.(entity.SKAkreditasi); ok {
		// Update Akreditasi
		data, err := service.PublishOrUnPublishSKAkreditasi(v.ID_SK_Akreditasi, "0")
		if err != nil {
			res := helper.BuildErrorResponse("UnPublish SK Akreditasi Failed", err.Error(), helper.EmptyObj{})
			return c.Status(fiber.StatusBadRequest).JSON(res)
		}
		res := helper.BuildResponse(true, "UnPublish SK Akreditasi Success", data)
		return c.Status(fiber.StatusOK).JSON(res)
	}
	res := helper.BuildErrorResponse("SK Akreditasi Not Found", err.Error(), helper.EmptyObj{})
	return c.Status(fiber.StatusBadRequest).JSON(res)
}

// Delete SK Akreditasi  			godoc
// @Security 				bearerAuth
// @Summary      		Delete SK Akreditasi
// @Description  		Delete SK Akreditasi API calls
// @Tags         		SK Akreditasi
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		id path int true "ID SK Akreditasi"
// @Success      		200  {object}  helper.Response
// @Failure      		422  {object}  helper.EmptyObj
// @Failure      		401  {object}  helper.Response
// @Failure      		400  {object}  helper.Response
// @Router       		/sk-akreditasi/{id} [delete]
func DeleteSKAkreditasi(c *fiber.Ctx) error {
	id, _ := c.ParamsInt("id")

	data_sk, err := service.ShowSKAkreditasi(id)
	if err != nil {
		res := helper.BuildErrorResponse("Delete SK Akreditasi Not Found", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	if v, ok := data_sk.(entity.SKAkreditasi); ok {
		// Delete Akreditasi
		data, err := service.DeleteSKAkreditasi(v.ID_SK_Akreditasi)
		if err != nil {
			res := helper.BuildErrorResponse("Delete SK Akreditasi Failed", err.Error(), helper.EmptyObj{})
			return c.Status(fiber.StatusBadRequest).JSON(res)
		}
		res := helper.BuildResponse(true, "Delete SK Akreditasi Success", data)
		return c.Status(fiber.StatusOK).JSON(res)
	}
	res := helper.BuildErrorResponse("SK Akreditasi Not Found", err.Error(), helper.EmptyObj{})
	return c.Status(fiber.StatusBadRequest).JSON(res)
}

// Lists Terakdetasi Belum SK 	godoc
// @Security 				bearerAuth
// @Summary      		Lists Terakdetasi Belum SK
// @Description  		Lists Terakdetasi Belum SK API calls
// @Tags         		SK Akreditasi
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		page query string true "Page"
// @Param        		row query string true "Row"
// @Param        		jurnal query string false "Search Jurnal"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/sk-akreditasi/list-terakdetasi-belum-sk [get]
func GetAllTerakdetasiBelumSK(c *fiber.Ctx) error {
	page := c.Query("page")
	row := c.Query("row")
	jurnal := c.Query("jurnal")
	data, err := service.GetTerakdetasiBelumSK(page, row, jurnal)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Set SK Akreditasi  			godoc
// @Security 				bearerAuth
// @Summary      		Set SK Akreditasi
// @Description  		Set SK Akreditasi API calls
// @Tags         		SK Akreditasi
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		set-sk-akreditasi body entity.SetAkreditasiDTO true "Set SK Akreditasi"
// @Success      		200  {object}  helper.Response
// @Failure      		422  {object}  helper.EmptyObj
// @Failure      		401  {object}  helper.Response
// @Failure      		400  {object}  helper.Response
// @Router       		/sk-akreditasi/set-sk-akreditasi [post]
func SetSKAkreditasi(c *fiber.Ctx) error {
	// Body To Struct
	skAkreditasiDTO := new(entity.SetAkreditasiDTO)
	if err := c.BodyParser(skAkreditasiDTO); err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	// Validasi
	validate := validator.New()
	err := validate.Struct(skAkreditasiDTO)
	if err != nil {
		res := helper.BuildValidationResponse(err)
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}

	// check apakah id_penetapan_akreditasi exist
	if !service.CheckIDPenetapanAkreditasi(skAkreditasiDTO.IDPenetapanAkreditasi) {
		res := helper.BuildSingleValidationResponse("id_penetapan_akreditasi", "notfound", "")
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}

	// check apakah id_sk_akreditasi exist
	if !service.CheckIDSKAkreditasi(skAkreditasiDTO.IDSKAkreditasi) {
		res := helper.BuildSingleValidationResponse("id_sk_akreditasi", "notfound", "")
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}

	// Set SK Akreditasi
	data, err := service.SetSKAkreditasi(*skAkreditasiDTO)
	if err != nil {
		res := helper.BuildErrorResponse("Set SK Akreditasi Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	res := helper.BuildResponse(true, "Set SK Akreditasi Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Unset SK Akreditasi  			godoc
// @Security 				bearerAuth
// @Summary      		Unset SK Akreditasi
// @Description  		Unset SK Akreditasi  API calls
// @Tags         		SK Akreditasi
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		unset-sk-akreditasi body entity.UnsetAkreditasiDTO true "Unset SK Akreditasi"
// @Success      		200  {object}  helper.Response
// @Failure      		422  {object}  helper.EmptyObj
// @Failure      		401  {object}  helper.Response
// @Failure      		400  {object}  helper.Response
// @Router       		/sk-akreditasi/unset-sk-akreditasi [post]
func UnsetSKAkreditasi(c *fiber.Ctx) error {
	// Body To Struct
	unsetAkreditasi := new(entity.UnsetAkreditasiDTO)
	if err := c.BodyParser(unsetAkreditasi); err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	// Validasi
	validate := validator.New()
	err := validate.Struct(unsetAkreditasi)
	if err != nil {
		res := helper.BuildValidationResponse(err)
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}

	// check apakah id_penetapan_akreditasi exist
	if !service.CheckIDPenetapanAkreditasi(unsetAkreditasi.IDPenetapanAkreditasi) {
		res := helper.BuildSingleValidationResponse("id_penetapan_akreditasi", "Not Found", "")
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}

	// check apakah id_sk_akreditasi exist
	if !service.CheckIDSKAkreditasi(unsetAkreditasi.IDSKAkreditasi) {
		res := helper.BuildSingleValidationResponse("id_sk_akreditasi", "Not Found", "")
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}

	// Set SK Akreditasi
	data, err := service.UnsetSKAkreditasi(*unsetAkreditasi)
	if err != nil {
		res := helper.BuildErrorResponse("Unset SK Akreditasi Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	res := helper.BuildResponse(true, "Unset SK Akreditasi Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}
