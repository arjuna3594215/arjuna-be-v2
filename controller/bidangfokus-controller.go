package controller

import (
	"arjuna-api/helper"
	"arjuna-api/service"

	"github.com/gofiber/fiber/v2"
)

// Lists Bidang Ilmu  	godoc
// @Security 				bearerAuth
// @Summary      		Lists Bidang Fokus
// @Description  		Lists Bidang Fokus API calls
// @Tags         		Utility
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/utility/bidang-fokus [get]
func GetAllBidangFokus(c *fiber.Ctx) error {
	data, err := service.GetBidangFokus()
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}

	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}
