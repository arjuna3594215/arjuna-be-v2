package controller

import (
	"arjuna-api/entity"
	"arjuna-api/helper"
	"arjuna-api/service"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
)

// Get All Lists Bidang Rumpun Ilmu  	godoc
// @Security 							bearerAuth
// @Summary      						Get All Lists Bidang Rumpun Ilmu
// @Description  						Get All Lists Bidang Rumpun Ilmu API calls
// @Tags         						Bidang Ilmu
// @Accept       						json
// @Consume      						json
// @Produce      						json
// @Success      						200  {object}  helper.Response
// @Success      						500  {object}  helper.EmptyObj
// @Router       						/bidang-ilmu/ [get]
func GetAllRumpunIlmu(c *fiber.Ctx) error {
	data, err := service.ListRumpunIlmu()
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Lists Bidang Ilmu  	godoc
// @Security 			bearerAuth
// @Summary      		Lists Bidang Ilmu
// @Description  		Lists Bidang Ilmu API calls
// @Tags         		Bidang Ilmu
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		id path string true "IDBidangIlmu"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/bidang-ilmu/{id} [get]
func GetAllBidangIlmu(c *fiber.Ctx) error {
	id := c.Params("id")

	data, err := service.ListBidangIlmu(id)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Get All Lists Bidang Ilmu  	godoc
// @Security 				bearerAuth
// @Summary      		Get All Lists Bidang Ilmu
// @Description  		Get All Lists Bidang Ilmu API calls
// @Tags         		Bidang Ilmu
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/bidang-ilmu/all [get]
func GetAllListBidangIlmu(c *fiber.Ctx) error {
	data, err := service.GetAllListBidangIlmu()
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Lists Sub Bidang Ilmu  	godoc
// @Security 				bearerAuth
// @Summary      		Lists Sub Bidang Ilmu
// @Description  		Lists Sub Bidang Ilmu API calls
// @Tags         		Bidang Ilmu
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		id path string true "IDBidangIlmu"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/bidang-ilmu/sub/{id} [get]
func GetAllSubBidangIlmu(c *fiber.Ctx) error {
	id := c.Params("id")
	data, err := service.ListSubBidangIlmu(id)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Delete Bidang Ilmu Jurnal Jurnal  	godoc
// @Security 				bearerAuth
// @Summary      		Delete Bidang Ilmu Jurnal Jurnal
// @Description  		Delete Bidang Ilmu Jurnal Jurnal API calls
// @Tags         		Jurnal
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		id path int true "ID Bidang Ilmu Jurnal"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Success      		404  {object}  helper.EmptyObj
// @Router       		/jurnal/bidang-ilmu-jurnal/{id} [delete]
func DeleteBidangIlmuJurnal(c *fiber.Ctx) error {
	id, err := c.ParamsInt("id")
	data, err := service.GetBidangIlmuJurnalByIDBidangIlmuJurnal(id)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	if _, ok := data.(entity.BidangIlmuJurnal); ok {
		_, err := service.DeleteBidangIlmuJurnal(id)
		if err != nil {
			res := helper.BuildErrorResponse("Delete Bidang Ilmu Jurnal Failed", err.Error(), helper.EmptyObj{})
			return c.Status(fiber.StatusInternalServerError).JSON(res)
		}
		res := helper.BuildResponse(true, "Delete Bidang Ilmu Jurnal Success", helper.EmptyObj{})
		return c.Status(fiber.StatusOK).JSON(res)
	}
	res := helper.BuildErrorResponse("Not Found", err.Error(), helper.EmptyObj{})
	return c.Status(fiber.StatusNotFound).JSON(res)
}

// Save Bidang Ilmu Jurnal  			godoc
// @Security 				bearerAuth
// @Summary      		Save Bidang Ilmu Jurnal
// @Description  		Save Bidang Ilmu Jurnal API calls
// @Tags         		Jurnal
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		bidang_ilmu_jurnal body entity.BidangIlmuJurnalDTO true "Save Bidang Ilmu Jurnal"
// @Success      		200  {object}  helper.Response
// @Failure      		422  {object}  helper.EmptyObj
// @Failure      		401  {object}  helper.Response
// @Failure      		400  {object}  helper.Response
// @Router       		/jurnal/bidang-ilmu-jurnal [post]
func SaveBidangIlmuJurnal(c *fiber.Ctx) error {
	// Body To Struct
	bijurnalDto := new(entity.BidangIlmuJurnalDTO)
	if err := c.BodyParser(bijurnalDto); err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	// Validasi
	validate := validator.New()
	err := validate.Struct(bijurnalDto)
	if err != nil {
		res := helper.BuildValidationResponse(err)
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}

	// Insert Bidang Ilmi Jurnal
	data, err := service.InsertBidangIlmuJurnal(*bijurnalDto)
	if err != nil {
		res := helper.BuildErrorResponse("Create Bidang Ilmu Jurnal Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	res := helper.BuildResponse(true, "Create Bidang Ilmu Jurnal Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}
