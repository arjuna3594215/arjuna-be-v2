package controller

import (
	"arjuna-api/entity"
	"arjuna-api/helper"
	"arjuna-api/service"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
)

// Send Kontak  			godoc
// @Summary      		Send Kontak
// @Description  		Send Kontak API calls
// @Tags         		Front Page Kontak
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		sendKontak body entity.KontakDTO true "Send Kontak"
// @Success      		200  {object}  helper.Response
// @Failure      		422  {object}  helper.EmptyObj
// @Failure      		401  {object}  helper.Response
// @Failure      		400  {object}  helper.Response
// @Failure      		404  {object}  helper.Response
// @Router       		/kontak/send [post]
func SendKontak(c *fiber.Ctx) error {
	// Body To Struct
	kontakDTO := new(entity.KontakDTO)
	if err := c.BodyParser(kontakDTO); err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	// Validasi
	validate := validator.New()
	err := validate.Struct(kontakDTO)
	if err != nil {
		res := helper.BuildValidationResponse(err)
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}
	data, err := service.SendKontak(*kontakDTO)
	if err != nil {
		res := helper.BuildErrorResponse("Send Kontak Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	res := helper.BuildResponse(true, "Send Kontak Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}
