CREATE OR REPLACE FUNCTION z_new_arjuna_list_penilaian_selesai_lb_70(p_isakreditasi bpchar= '1'::bpchar, p_nama_jurnal varchar= ''::character varying)
  RETURNS TABLE("no" int8, "id_usulan_akreditasi" int8, "tgl_usulan" date, "url_statistik_pengunjung" varchar, "nama_jurnal" varchar, "eissn" varchar, "pissn" varchar, "publisher" varchar, "society" varchar, "nama_awal_jurnal" varchar, "tgl_pembuatan" date, "url_jurnal" varchar, "url_contact" varchar, "url_editor" varchar, "country" varchar, "city" varchar, "alamat" varchar, "no_telepon" varchar, "alamat_surel" varchar, "tgl_akhir_terakreditasi" date, "nama_pic" varchar, "nama_institusi" varchar, "nama_eic" varchar, "alamat_surel_eic" varchar, "alamat_oai" varchar, "doi_jurnal" varchar, "total_nilai_mgmt" numeric, "disinsentif_nilai_mgmt" numeric, "jml_item_nilai_mgmt" int8, "jml_item_nilai_mgmt_selesai" int8, "total_nilai_issue" numeric, "disinsentif_nilai_issue" numeric, "detail_nilai_issue" text, "jml_item_nilai_issue" int8, "jml_item_nilai_issue_selesai" int8, "grade_akreditasi" bpchar, "asesor_mgmt" text, "asesor_issue" text, "bidang_ilmu" varchar, "rekap_issue" text, "jml_record" int8) AS $BODY$
BEGIN

	RETURN QUERY	(
	select table_temp.*,
		count(table_temp.*) over()::bigint as jml_record from (
	WITH datausulan AS (	
		SELECT distinct t1.id_usulan_akreditasi
		FROM (	SELECT t1.id_usulan_akreditasi
			FROM public.penugasan_penilaian_mgmt t1
			WHERE t1.sts_penerimaan_penugasan = '1'
				AND t1.sts_pelaksanaan_tugas = '1'
				AND NOT t1.id_usulan_akreditasi IN (
					SELECT distinct ta.id_usulan_akreditasi
					FROM public.penugasan_penilaian_mgmt ta
					WHERE ta.sts_penerimaan_penugasan = '1'
						AND ta.sts_pelaksanaan_tugas != '1'	-- Belum disimpan permanen oleh penilai manajemen
					) 
			INTERSECT
			SELECT t1.id_usulan_akreditasi
			FROM public.penugasan_penilaian_issue t1
			WHERE t1.sts_penerimaan_penugasan = '1'
				AND t1.sts_pelaksanaan_tugas = '1'
				AND NOT t1.id_usulan_akreditasi IN (
					SELECT distinct ta.id_usulan_akreditasi
					FROM public.penugasan_penilaian_issue ta
					WHERE ta.sts_penerimaan_penugasan = '1'
						AND ta.sts_pelaksanaan_tugas != '1' -- Belum disimpan permanen oleh penilai issue
					) 
			) t1
        INNER JOIN (SELECT ta.* FROM public.list_pengajuan_baru(p_isakreditasi) ta) t2 
        	ON t2.id_usulan_akreditasi = t1.id_usulan_akreditasi      
		INNER JOIN public.usulan_akreditasi t3 ON t3.id_usulan_akreditasi = t1.id_usulan_akreditasi
        INNER JOIN public.identitas_jurnal t4 ON t3.id_identitas_jurnal = t4.id_identitas_jurnal
        	AND (t4.nama_jurnal ~* COALESCE(p_nama_jurnal, '.') or lower(t4.eissn) ~* lower(p_nama_jurnal))
        WHERE NOT t1.id_usulan_akreditasi IN (
			SELECT ta.id_usulan_akreditasi FROM public.hasil_akreditasi ta
            	
            	--WHERE ta.tgl_created > t3.tgl_updated -- UN COMMENT KALAU ADA YANG ERROR DIHASIL, KARENA SEMPAT ADA 21 JURNAL YANG KETINGGALAN DEBBUG 02 JULI 2021
            )	
	),
	data_jml_item_nilai_mgmt AS (
		SELECT t1.id_usulan_akreditasi, count(*) as jml_item_nilai_mgmt
			, array_agg(t3.nama) as nama_penilai_manajemen
		FROM datausulan t1
		INNER JOIN public.penugasan_penilaian_mgmt t2 ON t1.id_usulan_akreditasi = t2.id_usulan_akreditasi
			AND t2.sts_penerimaan_penugasan = '1'
		INNER JOIN public.personal t3 ON t2.id_personal = t3.id_personal
		GROUP BY  t1.id_usulan_akreditasi
	),
	data_progres_penilaian_mgmt AS (
		SELECT t1.id_usulan_akreditasi
			, array_to_string(array_agg(concat(nama, ': ', jml_usulan, ' - selesai: ', jml_selesai::character varying)),'; ') as asesor_mgmt
			, sum(jml_selesai)::bigint as jml_item_nilai_mgmt_selesai
		FROM (	SELECT t1.id_usulan_akreditasi
				, t3.nama, count(*)::character varying jml_usulan
				, SUM(CASE WHEN t2.sts_pelaksanaan_tugas = '1' THEN 1 ELSE 0 END) as jml_selesai
			FROM datausulan t1
			INNER JOIN public.penugasan_penilaian_mgmt t2 ON t1.id_usulan_akreditasi = t2.id_usulan_akreditasi
				AND t2.sts_penerimaan_penugasan != '0'
			INNER JOIN public.personal t3 ON t2.id_personal = t3.id_personal
			GROUP BY t1.id_usulan_akreditasi, t3.nama
			) t1
		GROUP BY t1.id_usulan_akreditasi
	),
	data_nilai_mgmt_dari_penilai_mgmt AS (
		SELECT t1.id_usulan_akreditasi, t2.id_penugasan_penilaian_manajemen, 
			t2.id_personal, t4.nama as nama_penilai_manajemen,
			sum(nilai) as nilai	
		FROM datausulan t1
		INNER JOIN public.penugasan_penilaian_mgmt t2 ON t1.id_usulan_akreditasi = t2.id_usulan_akreditasi
			AND t2.sts_penerimaan_penugasan != '0'
		INNER JOIN public.hasil_penilaian_mgmt t3 ON t2.id_penugasan_penilaian_manajemen = t3.id_penugasan_penilaian_manajemen
		INNER JOIN public.personal t4 ON t2.id_personal = t4.id_personal
		INNER JOIN public.sub_unsur_penilaian t5 ON t3.id_sub_unsur_penilaian = t5.id_sub_unsur_penilaian
		INNER JOIN public.unsur_penilaian t6 ON t5.id_unsur_penilaian = t6.id_unsur_penilaian
			AND t6.id_kelompok_unsur_perhitungan_nilai = 2 -- Manajemen		
		GROUP BY t1.id_usulan_akreditasi, t2.id_penugasan_penilaian_manajemen, 
			t2.id_personal, t4.nama 
	) ,
	data_nilai_mgmt_dari_penilai_issue AS (
		SELECT t1.id_usulan_akreditasi, t2.id_penugasan_penilaian_issue
			, t2.id_personal
			, t4.nama as nama_penilai_issue
			, sum(t3.nilai) as nilai	
		FROM datausulan t1
		INNER JOIN public.penugasan_penilaian_issue t2 ON t1.id_usulan_akreditasi = t2.id_usulan_akreditasi
			AND t2.sts_penerimaan_penugasan != '0'
		INNER JOIN public.hasil_penilaian_issue t3 ON t2.id_penugasan_penilaian_issue = t3.id_penugasan_penilaian_issue
		INNER JOIN public.personal t4 ON t2.id_personal = t4.id_personal
		INNER JOIN public.sub_unsur_penilaian t5 ON t3.id_sub_unsur_penilaian = t5.id_sub_unsur_penilaian
		INNER JOIN public.unsur_penilaian t6 ON t5.id_unsur_penilaian = t6.id_unsur_penilaian
			AND t6.id_kelompok_unsur_perhitungan_nilai = 2 -- manajemen			
		GROUP BY t1.id_usulan_akreditasi, t2.id_penugasan_penilaian_issue, 
			t2.id_personal, t4.nama 
	) ,	
	data_jml_item_nilai_issue AS (
		SELECT t1.id_usulan_akreditasi, count(*) as jml_item_nilai_issue
		FROM datausulan t1
		INNER JOIN public.penugasan_penilaian_issue t2 ON t1.id_usulan_akreditasi = t2.id_usulan_akreditasi
			AND t2.sts_penerimaan_penugasan != '0'
		GROUP BY t1.id_usulan_akreditasi
	),
	data_progres_penilaian_issue AS (
		SELECT t1.id_usulan_akreditasi
			, array_to_string(array_agg(concat(nama, ': ', jml_issue, ' - selesai: ', jml_selesai::character varying)),'; ') as asesor_issue
			, sum(jml_selesai)::bigint as jml_item_nilai_issue_selesai
		FROM (	SELECT t1.id_usulan_akreditasi
				, t3.nama, count(*)::character varying jml_issue
				, SUM(CASE WHEN t2.sts_pelaksanaan_tugas = '1' THEN 1 ELSE 0 END) as jml_selesai
			FROM datausulan t1
			INNER JOIN public.penugasan_penilaian_issue t2 ON t1.id_usulan_akreditasi = t2.id_usulan_akreditasi
				AND t2.sts_penerimaan_penugasan != '0'
			INNER JOIN public.personal t3 ON t2.id_personal = t3.id_personal
			GROUP BY t1.id_usulan_akreditasi, t3.nama
			) t1
		GROUP BY t1.id_usulan_akreditasi
	),
	data_nilai_issue_dari_penilai_mgmt AS (
		SELECT t1.id_usulan_akreditasi, t2.id_penugasan_penilaian_manajemen, 
			t2.id_personal, t4.nama as nama_penilai_manajemen,
			sum(nilai) as nilai	
		FROM datausulan t1
		INNER JOIN public.penugasan_penilaian_mgmt t2 ON t1.id_usulan_akreditasi = t2.id_usulan_akreditasi
			AND t2.sts_penerimaan_penugasan != '0'
		INNER JOIN public.hasil_penilaian_mgmt t3 ON t2.id_penugasan_penilaian_manajemen = t3.id_penugasan_penilaian_manajemen
		INNER JOIN public.personal t4 ON t2.id_personal = t4.id_personal
		INNER JOIN public.sub_unsur_penilaian t5 ON t3.id_sub_unsur_penilaian = t5.id_sub_unsur_penilaian
		INNER JOIN public.unsur_penilaian t6 ON t5.id_unsur_penilaian = t6.id_unsur_penilaian
			AND t6.id_kelompok_unsur_perhitungan_nilai = 1 -- issue		
		GROUP BY t1.id_usulan_akreditasi, t2.id_penugasan_penilaian_manajemen, 
			t2.id_personal, t4.nama 
	) ,
	data_nilai_issue_dari_penilai_issue AS (
		SELECT t1.id_usulan_akreditasi, t2.id_penugasan_penilaian_issue, 
			t2.id_personal, t4.nama as nama_penilai_issue,
			sum(nilai) as nilai	
		FROM datausulan t1
		INNER JOIN public.penugasan_penilaian_issue t2 ON t1.id_usulan_akreditasi = t2.id_usulan_akreditasi
			AND t2.sts_penerimaan_penugasan != '0'
		INNER JOIN public.hasil_penilaian_issue t3 ON t2.id_penugasan_penilaian_issue = t3.id_penugasan_penilaian_issue
		INNER JOIN public.personal t4 ON t2.id_personal = t4.id_personal
		INNER JOIN public.sub_unsur_penilaian t5 ON t3.id_sub_unsur_penilaian = t5.id_sub_unsur_penilaian
		INNER JOIN public.unsur_penilaian t6 ON t5.id_unsur_penilaian = t6.id_unsur_penilaian
			AND t6.id_kelompok_unsur_perhitungan_nilai = 1 -- issue		
		GROUP BY t1.id_usulan_akreditasi, t2.id_penugasan_penilaian_issue, 
			t2.id_personal, t4.nama 
	),	
	data_nilai_mgmt AS (
		SELECT t1.id_usulan_akreditasi, t1.total_nilai_mgmt
			, COALESCE(min(t3.nilai), 0)::numeric(5,2) as disinsentif_nilai_mgmt
		FROM (	SELECT t1.id_usulan_akreditasi
				, (sum(t1.nilai)/count(*))::numeric(5,2) as total_nilai_mgmt
			FROM (	
				SELECT t1.id_usulan_akreditasi, t1.nilai
				FROM data_nilai_mgmt_dari_penilai_mgmt t1
				UNION
				SELECT t1.id_usulan_akreditasi, t1.nilai
				FROM data_nilai_mgmt_dari_penilai_issue t1
				) t1	
			GROUP BY t1.id_usulan_akreditasi
			) t1
		INNER JOIN public.penugasan_penilaian_mgmt t2 ON t1.id_usulan_akreditasi = t2.id_usulan_akreditasi
			AND t2.sts_penerimaan_penugasan = '1'
		LEFT JOIN public.disinsentif_penilaian_mgmt t3 ON t2.id_penugasan_penilaian_manajemen = t3.id_penugasan_penilaian_manajemen
		GROUP BY t1.id_usulan_akreditasi, t1.total_nilai_mgmt		
	),
    data_hasil_penilaian_issue AS (
		select t6.id_usulan_akreditasi,  array_to_string(array_agg(t6.nilai_per_penilai),'//')  as nilai_detil_issue from (																																		  
		select t4.id_usulan_akreditasi, concat(t5.nama , ': ' , array_agg(t4.nilai_per_issue_per_penilai)) as nilai_per_penilai from (
		select t2.id_usulan_akreditasi, concat('v', ti.volume_issue , '.i',ti.nomor_issue,'-', sum(t1.nilai))  as nilai_per_issue_per_penilai,t2.id_personal from hasil_penilaian_issue t1
		inner join penugasan_penilaian_issue t2 on t1.id_penugasan_penilaian_issue=t2.id_penugasan_penilaian_issue
		inner join issue ti on t2.id_issue=ti.id_issue
		inner join datausulan t3 on t2.id_usulan_akreditasi = t3.id_usulan_akreditasi
		group by t2.id_penugasan_penilaian_issue,t2.id_personal, t2.id_usulan_akreditasi, ti.volume_issue, ti.nomor_issue
		order by ti.volume_issue desc, ti.nomor_issue desc																						  
		) t4
		inner join personal t5 on t4.id_personal=t5.id_personal
		group by t4.id_personal, t5.nama, t4.id_usulan_akreditasi
		) t6
		group by t6.id_usulan_akreditasi
	),
	data_nilai_issue AS (
		SELECT t1.id_usulan_akreditasi, t1.total_nilai_issue
			, COALESCE(min(t3.nilai), 0)::numeric(5,2) as disinsentif_nilai_issue
		FROM (	SELECT t1.id_usulan_akreditasi
				, sum(t1.nilai)::numeric(5,2) as total_nilai_issue
			FROM (	
				SELECT t1.id_usulan_akreditasi, avg(t1.nilai) as nilai
				FROM data_nilai_issue_dari_penilai_mgmt t1
				GROUP BY t1.id_usulan_akreditasi
				UNION
				SELECT t1.id_usulan_akreditasi, avg(t1.nilai) as nilai
				FROM data_nilai_issue_dari_penilai_issue t1
				GROUP BY t1.id_usulan_akreditasi
				) t1		
			GROUP BY t1.id_usulan_akreditasi
			) t1			
		INNER JOIN public.penugasan_penilaian_issue t2 ON t1.id_usulan_akreditasi = t2.id_usulan_akreditasi
			AND t2.sts_penerimaan_penugasan = '1'
		LEFT JOIN public.disinsentif_penilaian_issue t3 ON t2.id_penugasan_penilaian_issue = t3.id_penugasan_penilaian_issue
		GROUP BY t1.id_usulan_akreditasi, t1.total_nilai_issue
	)

	SELECT row_number() over() as no, t0.id_usulan_akreditasi, 
		t3.tgl_updated::date as tgl_usulan,
		t5.url_statistik_pengunjung,
		t4.nama_jurnal, t4.eissn, t4.pissn, t4.publisher, t4.society,
		t5.nama_awal_jurnal, t5.tgl_pembuatan, t5.url_jurnal, t5.url_contact, t5.url_editor,
		t5.country, t5.city, t5.alamat, t5.no_telepon, t5.alamat_surel,
		t5.tgl_akhir_terakreditasi,
		t7.nama as nama_pic, t7.nama_institusi,
		t8.nama_eic, t8.alamat_surel as alamat_surel_eic,
		t4.alamat_oai, t4.doi_jurnal,	
		t1.total_nilai_mgmt, t1.disinsentif_nilai_mgmt,
		t9.jml_item_nilai_mgmt, t11.jml_item_nilai_mgmt_selesai,
		t2.total_nilai_issue, t2.disinsentif_nilai_issue,
        t14.nilai_detil_issue,
		t10.jml_item_nilai_issue, t12.jml_item_nilai_issue_selesai,
		CASE	WHEN (t1.total_nilai_mgmt + t1.disinsentif_nilai_mgmt + t2.total_nilai_issue + t2.disinsentif_nilai_issue)::numeric(5,2) > t13.minimal_nilai_a THEN '1'
			WHEN (t1.total_nilai_mgmt + t1.disinsentif_nilai_mgmt + t2.total_nilai_issue + t2.disinsentif_nilai_issue)::numeric(5,2) >= t13.minimal_nilai_b THEN '2'
			ELSE 'T'
		END::character(1) as grade_akreditasi,
		t11.asesor_mgmt, t12.asesor_issue,
		(select array_to_string(array_agg(th4.bidang_ilmu),' - ')::character varying
				 from public.identitas_jurnal th1 inner join public.usulan_akreditasi th2 ON 
				(th1.id_identitas_jurnal=th2.id_identitas_jurnal) left join 
				public.bidang_ilmu_jurnal th3 ON (th1.id_jurnal = th3.id_jurnal) 
					AND th3.sts_aktif_bidang_ilmu_jurnal = '1'
				LEFT JOIN public.bidang_ilmu th4 ON th3.id_bidang_ilmu = th4.id_bidang_ilmu   
				WHERE th2.id_usulan_akreditasi = t0.id_usulan_akreditasi)
		, (SELECT string_agg(temp_issue.issue,' ### ')
		FROM PUBLIC.arjuna_get_issue_by_usulan_akreditasi_print(t0.id_usulan_akreditasi::integer)temp_issue)
	FROM datausulan t0
	LEFT JOIN data_nilai_mgmt t1 ON t0.id_usulan_akreditasi = t1.id_usulan_akreditasi
	LEFT JOIN data_nilai_issue t2 ON t0.id_usulan_akreditasi = t2.id_usulan_akreditasi
	INNER JOIN public.usulan_akreditasi t3 ON t0.id_usulan_akreditasi = t3.id_usulan_akreditasi
	INNER JOIN public.identitas_jurnal t4 ON t3.id_identitas_jurnal = t4.id_identitas_jurnal
		 
	INNER JOIN public.jurnal t5 ON t4.id_jurnal = t5.id_jurnal
	INNER JOIN public.pic t6 ON t3.id_pic = t6.id_pic
	INNER JOIN public.personal t7 ON t6.id_personal = t7.id_personal
	INNER JOIN public.eic t8 ON t3.id_eic = t8.id_eic
-- 	INNER JOIN public.bidang_ilmu_jurnal t16 ON t16.id_jurnal = t4.id_jurnal		
-- 	INNER JOIN public.bidang_ilmu t17 ON t17.id_bidang_ilmu = t16.id_bidang_ilmu 
-- 		AND t16.sts_aktif_bidang_ilmu_jurnal = '1'
-- 		AND t17.sts_aktif_bidang_ilmu = '1'	
	LEFT JOIN data_jml_item_nilai_mgmt t9 ON t0.id_usulan_akreditasi = t9.id_usulan_akreditasi
	LEFT JOIN data_jml_item_nilai_issue t10 ON t0.id_usulan_akreditasi = t10.id_usulan_akreditasi
	LEFT JOIN data_progres_penilaian_mgmt t11 ON t0.id_usulan_akreditasi = t11.id_usulan_akreditasi
	LEFT JOIN data_progres_penilaian_issue t12 ON t0.id_usulan_akreditasi = t12.id_usulan_akreditasi
    LEFT JOIN data_hasil_penilaian_issue t14 ON t0.id_usulan_akreditasi = t14.id_usulan_akreditasi
	CROSS JOIN (SELECT * FROM public.configurasi WHERE sts_aktif_configurasi = '1') t13
	WHERE (t4.nama_jurnal ~* COALESCE(p_nama_jurnal, '.') or lower(t4.eissn) ~* lower(p_nama_jurnal))
	ORDER BY t3.tgl_updated DESC
	) table_temp);
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000