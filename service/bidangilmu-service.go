package service

import (
	"arjuna-api/config"
	"arjuna-api/entity"
)

func GetAllListBidangIlmu() ([]entity.BidangIlmu, error) {
	var res []entity.BidangIlmu

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("bidang_ilmu")
	data.Where("level_taksonomi = ?", 2)
	data.Find(&res)
	if data.Error != nil {
		return nil, data.Error
	}
	return res, nil
}

func ListRumpunIlmu() ([]entity.BidangIlmu, error) {
	var res []entity.BidangIlmu

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("bidang_ilmu")
	data.Where("level_taksonomi = ?", 0)
	data.Where("sts_aktif_bidang_ilmu = ?", "1")
	data.Find(&res)
	if data.Error != nil {
		return nil, data.Error
	}
	return res, nil
}

func ListBidangIlmu(id_bidang_ilmu_parent string) ([]entity.BidangIlmu, error) {
	var resp []entity.BidangIlmu

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("bidang_ilmu")
	data.Where("id_bidang_ilmu_parent = ?", id_bidang_ilmu_parent)
	data.Where("level_taksonomi = ?", 1)
	data.Where("sts_aktif_bidang_ilmu = ?", "1")
	data.Order("kode_bidang_ilmu desc")
	data.Find(&resp)
	if data.Error != nil {
		return nil, data.Error
	}

	return resp, nil
}

func getBidangIlmuByKode(code string) string {
	var res string
	db := config.Con()
	defer config.CloseCon(db)

	data := db.Select("id_bidang_ilmu").Table("bidang_ilmu").Where("kode_bidang_ilmu = ?", code).Take(&res)
	data.Find(&res)
	if data.Error != nil {
		return ""
	}
	return res
}

func ListSubBidangIlmu(id_parent string) ([]entity.BidangSubIlmu, error) {
	var res []entity.BidangSubIlmu

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("bidang_ilmu")
	data.Where("id_bidang_ilmu_parent = ?", id_parent)
	data.Where("level_taksonomi = ?", 2)
	data.Where("sts_aktif_bidang_ilmu = ?", "1")
	data.Order("kode_bidang_ilmu desc")
	data.Find(&res)
	if data.Error != nil {
		return nil, data.Error
	}
	return res, nil
}

func ListBidangIlmuByPersonal(id_personal int) []entity.BidangIlmuInterest {
	var bidang_ilmu []entity.BidangIlmuInterest

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("bidang_ilmu_interest")
	data.Select("bidang_ilmu_interest.id_personal, bidang_ilmu_interest.id_bidang_ilmu, bi.bidang_ilmu")
	data.Joins("join bidang_ilmu as bi on bi.id_bidang_ilmu = bidang_ilmu_interest.id_bidang_ilmu")
	data.Where("bidang_ilmu_interest.id_personal = ?", id_personal)
	data.Find(&bidang_ilmu)
	if data.Error != nil {
		return nil
	}
	return bidang_ilmu
}

func getBidangIlmuByJurnal(id_jurnal int64) []entity.BidangIlmuJurnal {
	var bidang_ilmu []entity.BidangIlmuJurnal

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("bidang_ilmu_jurnal as t1")
	data.Select("t1.id_jurnal, t1.id_bidang_ilmu_jurnal, t1.id_bidang_ilmu, t2.kode_bidang_ilmu, t2.bidang_ilmu, t2.level_taksonomi, t2.id_bidang_ilmu_parent")
	data.Joins("join bidang_ilmu as t2 on t2.id_bidang_ilmu = t1.id_bidang_ilmu")
	data.Joins("join identitas_jurnal as t3 on t3.id_jurnal = t1.id_jurnal")
	data.Where("t1.id_jurnal = ?", id_jurnal)
	data.Where("t1.sts_aktif_bidang_ilmu_jurnal = ?", "1")
	data.Find(&bidang_ilmu)
	if data.Error != nil {
		return nil
	}
	return bidang_ilmu

}
