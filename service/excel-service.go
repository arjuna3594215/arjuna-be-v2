package service

import (
	"arjuna-api/config"
	"arjuna-api/entity"
	"arjuna-api/helper"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/360EntSecGroup-Skylar/excelize"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
)

func ExportDaftarTerbitanJurnal(search string, status string, personal string, baseUrl string) (string, error) {
	var jurnal []entity.DaftarTerbitanJurnal

	db := config.Con()
	defer config.CloseCon(db)

	nilai_total_sa := db.Table("usulan_akreditasi as ua")
	nilai_total_sa.Select("hs.nilai_total")
	nilai_total_sa.Where("ua.id_identitas_jurnal = ij.id_identitas_jurnal")
	nilai_total_sa.Order("ua.id_usulan_akreditasi DESC")
	nilai_total_sa.Joins("Left JOIN hasil_sa as hs on hs.id_usulan_akreditasi = ua.id_usulan_akreditasi AND ua.id_identitas_jurnal = ij.id_identitas_jurnal")
	nilai_total_sa.Limit(1)

	grade_akreditasi_sa := db.Table("usulan_akreditasi as ua")
	grade_akreditasi_sa.Select("hs.grade_akreditasi")
	grade_akreditasi_sa.Where("ua.id_identitas_jurnal = ij.id_identitas_jurnal")
	grade_akreditasi_sa.Order("ua.id_usulan_akreditasi DESC")
	grade_akreditasi_sa.Joins("Left JOIN hasil_sa as hs on hs.id_usulan_akreditasi = ua.id_usulan_akreditasi AND ua.id_identitas_jurnal = ij.id_identitas_jurnal")
	grade_akreditasi_sa.Limit(1)

	nilai_total := db.Table("usulan_akreditasi as ua")
	nilai_total.Select("ha.nilai_total")
	nilai_total.Joins("JOIN hasil_akreditasi as ha on ha.id_usulan_akreditasi = ua.id_usulan_akreditasi AND ua.id_identitas_jurnal = ij.id_identitas_jurnal")
	nilai_total.Order("ua.id_usulan_akreditasi DESC")
	nilai_total.Limit(1)

	grade_akreditasi := db.Table("usulan_akreditasi as ua")
	grade_akreditasi.Select("ha.grade_akreditasi")
	grade_akreditasi.Joins("JOIN hasil_akreditasi as ha on ha.id_usulan_akreditasi = ua.id_usulan_akreditasi AND ua.id_identitas_jurnal = ij.id_identitas_jurnal")
	grade_akreditasi.Order("ua.id_usulan_akreditasi DESC")
	grade_akreditasi.Limit(1)

	progres := db.Table("usulan_akreditasi as ua")
	progres.Select("progres.progres")
	progres.Where("ua.id_identitas_jurnal = ij.id_identitas_jurnal")
	progres.Order("ua.id_usulan_akreditasi DESC")
	progres.Joins("Left JOIN progres_usulan_akreditasi as pua ON pua.id_usulan_akreditasi = ua.id_usulan_akreditasi AND ua.id_identitas_jurnal = ij.id_identitas_jurnal")
	progres.Joins("Left JOIN progres ON progres.id_progres = pua.id_progres")
	progres.Limit(1)

	data := db.Table("pic")
	data.Distinct()
	data.Select("ij.eissn, ij.pissn, ij.publisher, ij.society, j.nama_awal_jurnal, j.url_contact, j.url_editor, j.url_statistik_pengunjung, j.country, j.city, j.tgl_akhir_terakreditasi, pic.id_pic, personal.email as pic_email, personal.nama as pic_nama, personal.no_hand_phone as pic_no_telepon, j.id_jurnal, ij.id_identitas_jurnal, ij.nama_jurnal, j.tgl_pembuatan, j.tgl_created, pengguna.username, j.alamat, j.alamat_surel, j.no_telepon, j.url_jurnal, ij.sts_aktif_identitas_jurnal, j.tahun_1_terbit as tahun_terbit, ij.frekuensi_terbitan, j.tgl_usulan_akreditasi_terakhir, ij.image, ( ? ) as nilai_total_sa, ( ? ) as grade_akreditasi_sa, ( ? ) as progres, ( ? ) as nilai_total, ( ? ) as grade_akreditasi", nilai_total_sa, grade_akreditasi_sa, progres, nilai_total, grade_akreditasi)
	data.Joins("JOIN identitas_jurnal AS ij ON ij.id_identitas_jurnal = pic.id_identitas_jurnal")
	data.Joins("JOIN jurnal AS j ON j.id_jurnal = ij.id_jurnal")
	data.Joins("JOIN pengguna ON pengguna.id_personal = pic.id_personal ")
	data.Joins("JOIN personal ON personal.id_personal = pic.id_personal ")
	data.Where("pic.sts_aktif_pic = ?", "1")
	if status != "" {
		data.Where("ij.sts_aktif_identitas_jurnal = ?", status)
	}
	if personal != "" {
		data.Where("pic.id_personal = ?", personal)
	}
	if search != "" {
		data.Where("lower(ij.nama_jurnal) LIKE ?", "%"+strings.ToLower(search)+"%")
	}
	// data.Order("ij.nama_jurnal asc")
	data.Find(&jurnal)
	if data.Error != nil {
		return "", data.Error
	}

	xlsx := excelize.NewFile()
	sheet1Name := "Sheet One"
	xlsx.SetSheetName(xlsx.GetSheetName(1), sheet1Name)

	xlsx.SetCellValue(sheet1Name, "A1", "ID Identitas Jurnal")
	xlsx.SetCellValue(sheet1Name, "B1", "Nama Jurnal")
	xlsx.SetCellValue(sheet1Name, "C1", "EISSN")
	xlsx.SetCellValue(sheet1Name, "D1", "PISSN")
	xlsx.SetCellValue(sheet1Name, "E1", "Penerbit")
	xlsx.SetCellValue(sheet1Name, "F1", "Komunitas")
	xlsx.SetCellValue(sheet1Name, "G1", "Data PIC")
	xlsx.SetCellValue(sheet1Name, "H1", "Nawa Terbitan Pertama Kali")
	xlsx.SetCellValue(sheet1Name, "I1", "Tanggal Pembuatan")
	xlsx.SetCellValue(sheet1Name, "J1", "Tahun 1 Terbit")
	xlsx.SetCellValue(sheet1Name, "K1", "URL Jurnal")
	xlsx.SetCellValue(sheet1Name, "L1", "URL Kontak")
	xlsx.SetCellValue(sheet1Name, "M1", "URL Editor")
	xlsx.SetCellValue(sheet1Name, "N1", "URL Statistik")
	xlsx.SetCellValue(sheet1Name, "O1", "Negara")
	xlsx.SetCellValue(sheet1Name, "P1", "Kota")
	xlsx.SetCellValue(sheet1Name, "Q1", "Alamat")
	xlsx.SetCellValue(sheet1Name, "R1", "Nomor Telepon")
	xlsx.SetCellValue(sheet1Name, "S1", "Email")
	xlsx.SetCellValue(sheet1Name, "T1", "Tanggal Akhir Terakreditasi")
	xlsx.SetCellValue(sheet1Name, "U1", "Tanggal Usulan Akreditasi Terakhir")
	xlsx.SetCellValue(sheet1Name, "V1", "Progress")
	xlsx.SetCellValue(sheet1Name, "W1", "Nilai Total")
	xlsx.SetCellValue(sheet1Name, "X1", "Grade Akreditasi")
	// xlsx.SetCellValue(sheet1Name, "Y1", "Status Hasil Akreditasi")
	xlsx.SetCellValue(sheet1Name, "Y1", "Nilai Total SA")
	xlsx.SetCellValue(sheet1Name, "Z1", "Grade Akreditasi SA")

	err := xlsx.AutoFilter(sheet1Name, "A1", "Z1", "")
	if err != nil {
		return "", err
	}

	for i, V := range jurnal {
		datPIC := fmt.Sprintf("%s | %s | %s", V.PicNama, V.Publisher, V.PicEmail)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("A%d", i+2), V.IdIdentitasJurnal)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("B%d", i+2), V.NamaJurnal)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("C%d", i+2), V.Eissn)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("D%d", i+2), V.Pissn)

		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("E%d", i+2), V.Publisher)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("F%d", i+2), V.Society)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("F%d", i+2), datPIC)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("H%d", i+2), V.NamaAwalJurnal)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("I%d", i+2), V.TglPembuatan)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("J%d", i+2), V.TahunTerbit)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("K%d", i+2), V.UrlJurnal)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("L%d", i+2), V.UrlContact)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("M%d", i+2), V.UrlEditor)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("N%d", i+2), V.UrlStatistikPengunjung)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("O%d", i+2), V.Country)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("P%d", i+2), V.City)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("Q%d", i+2), V.Alamat)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("E%d", i+2), V.NoTelepon)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("S%d", i+2), V.AlamatSurel)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("T%d", i+2), V.TglAkhirTerakreditasi)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("U%d", i+2), V.TglUsulanAkreditasiTerakhir)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("V%d", i+2), V.Progres)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("W%d", i+2), V.NilaiTotal)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("X%d", i+2), V.GradeAkreditasi)
		// xlsx.SetCellValue(sheet1Name, fmt.Sprintf("C%d", i+2), V.s)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("Y%d", i+2), V.NilaiTotalSa)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("Z%d", i+2), V.GradeAkreditasiSa)
	}
	var sts_jurnal string
	if status == "1" {
		sts_jurnal = "Aktif"
	} else if status == "0" {
		sts_jurnal = "Tidak Aktif"
	} else {
		sts_jurnal = "All"
	}
	namaFile := fmt.Sprintf("%s_ARJUNA-DaftarTerbitan-%s.xlsx", time.Now().Format("20060130"), sts_jurnal)
	
	//remark by agus (geberate & upload to google storage)
	// path := "./files/excel/" + namaFile
	// err = xlsx.SaveAs(path)
	// if err != nil {
	// 	return "", err
	// }

	excelBytes, err := xlsx.WriteToBuffer()
	path, err  := helper.UploadFileXls("file/excel/", namaFile, excelBytes.Bytes())
	if err != nil {
		return "", err
	}
	
	// res := baseUrl + "/file/excel/" + namaFile
	result := baseUrl + "/file/excel/" + path
	return result, nil
}

func ExportDaftarUsulanJurnalTerdaftar(search string, baseUrl string) (string, error) {
	var jurnal []entity.DaftarUsulanJurnal

	db := config.Con()
	defer config.CloseCon(db)

	usulan_akreditasi := db.Table("usulan_akreditasi as ua")
	usulan_akreditasi.Select("ua.id_identitas_jurnal,MAX(ua.id_usulan_akreditasi) id_usulan_akreditasi")
	usulan_akreditasi.Where("sts_pengajuan_usulan = ?", "1")
	usulan_akreditasi.Group("ua.id_identitas_jurnal")

	penetapan_desk_evaluasi := db.Table("penetapan_desk_evaluasi as r1")
	penetapan_desk_evaluasi.Select("r1.id_usulan_akreditasi,MAX(r1.id_penetapan_desk_evaluasi) as id_penetapan_desk_evaluasi")
	penetapan_desk_evaluasi.Group("r1.id_usulan_akreditasi")

	penugasan_penilaian_issue := db.Table("penugasan_penilaian_issue as r1")
	penugasan_penilaian_issue.Select("r1.id_usulan_akreditasi, max(r1.id_penugasan_penilaian_issue) as id_penugasan_penilaian_issue, array_agg(DISTINCT r2.nama) as nama")
	penugasan_penilaian_issue.Joins("LEFT JOIN personal as r2 ON r2.id_personal = r1.id_personal")
	penugasan_penilaian_issue.Group("r1.id_usulan_akreditasi")

	penugasan_penilaian_mgmt := db.Table("penugasan_penilaian_mgmt as r1")
	penugasan_penilaian_mgmt.Select("r1.id_usulan_akreditasi, max(r1.id_penugasan_penilaian_manajemen) as id_penugasan_penilaian_manajemen, array_agg(DISTINCT r2.nama) as nama")
	penugasan_penilaian_mgmt.Joins(" JOIN personal r2 ON r2.id_personal = r1.id_personal")
	penugasan_penilaian_mgmt.Group("r1.id_usulan_akreditasi")

	hasil_penilaian_issue := db.Table("hasil_penilaian_issue as r1")
	hasil_penilaian_issue.Select("r2.id_usulan_akreditasi, SUM(r1.nilai) nilai")
	hasil_penilaian_issue.Joins("LEFT JOIN penugasan_penilaian_issue as r2 ON r2.id_penugasan_penilaian_issue = r1.id_penugasan_penilaian_issue")
	hasil_penilaian_issue.Group("r2.id_usulan_akreditasi")

	data := db.Table("identitas_jurnal as t1")
	data.Select("t1.id_identitas_jurnal, t3.id_usulan_akreditasi, t1.nama_jurnal, t1.publisher, t1b.url_jurnal, t17.bidang_ilmu, t1b.tgl_usulan_akreditasi_terakhir, t2.id_pic as id_pic1, t2.id_personal, t3.id_pic as id_pic2, t3.tgl_created as tgl_created_usulan, t3.tgl_updated as tgl_updated_usulan, t4.tgl_created as tgl_evaluasidiri, t4.nilai_total as nilai_evaluasi, t7.id_penetapan_desk_evaluasi, t7.tgl_created as desk_created, t7.tgl_updated as desk_updated, t7.sts_hasil_desk_evaluasi, t5.tgl_created as penugasan_issue_created, tb.nama as penilai_issue, t5.tgl_pelaksanaan_tugas as penugasan_issue_terlaksana, t6.tgl_created AS penugasan_mgmt_created, tc.nama as penilai_mgmt, t6.tgl_pelaksanaan_tugas as penugasan_mgmt_terlaksana, t8.tgl_updated as penetapan_updated, t8.sts_hasil_akreditasi, t9.tgl_updated as akreditasi_updated, t9.nilai_total as nilai_akreditasi, t9.grade_akreditasi")
	data.Joins("left join jurnal as t1b on t1b.id_jurnal = t1.id_jurnal")
	data.Joins("LEFT JOIN pic as t2 ON t2.id_identitas_jurnal = t1.id_identitas_jurnal")
	data.Joins("INNER JOIN bidang_ilmu_jurnal as t16 ON t16.id_jurnal = t1.id_jurnal")
	data.Joins("INNER JOIN bidang_ilmu t17 ON t17.id_bidang_ilmu = t16.id_bidang_ilmu AND t16.sts_aktif_bidang_ilmu_jurnal = '1' AND t17.sts_aktif_bidang_ilmu = '1' ")
	data.Joins("LEFT JOIN ( ? ) as ta ON t1.id_identitas_jurnal = ta.id_identitas_jurnal", usulan_akreditasi)
	data.Joins("LEFT JOIN usulan_akreditasi t3 ON t3.id_usulan_akreditasi = ta.id_usulan_akreditasi")
	data.Joins("LEFT JOIN hasil_sa t4 ON t4.id_usulan_akreditasi = t3.id_usulan_akreditasi")
	data.Joins("LEFT JOIN ( ? ) as te on te.id_usulan_akreditasi = t3.id_usulan_akreditasi", penetapan_desk_evaluasi)
	data.Joins("LEFT JOIN penetapan_desk_evaluasi as t7 ON t7.id_penetapan_desk_evaluasi = te.id_penetapan_desk_evaluasi")
	data.Joins("LEFT JOIN ( ? ) as tb  ON tb.id_usulan_akreditasi = t3.id_usulan_akreditasi", penugasan_penilaian_issue)
	data.Joins("LEFT JOIN penugasan_penilaian_issue as t5 ON t5.id_penugasan_penilaian_issue = tb.id_penugasan_penilaian_issue")
	data.Joins("LEFT JOIN ( ? ) as tc ON tc.id_usulan_akreditasi = t3.id_usulan_akreditasi", penugasan_penilaian_mgmt)
	data.Joins("LEFT JOIN penugasan_penilaian_mgmt as t6 ON t6.id_penugasan_penilaian_manajemen = tc.id_penugasan_penilaian_manajemen")
	data.Joins("LEFT JOIN ( ? ) as td ON td.id_usulan_akreditasi = t3.id_usulan_akreditasi", hasil_penilaian_issue)
	data.Joins("left join penetapan_akreditasi as t8 on t8.id_usulan_akreditasi = t3.id_usulan_akreditasi")
	data.Joins("left join hasil_akreditasi t9 on t9.id_usulan_akreditasi = t3.id_usulan_akreditasi")
	data.Where("t3.sts_pengajuan_usulan = ?", "1")
	if search != "" {
		data.Where("lower(t1.nama_jurnal) LIKE ?", "%"+strings.ToLower(search)+"%")
	}
	data.Find(&jurnal)

	if data.Error != nil {
		return "", data.Error
	}

	xlsx := excelize.NewFile()
	sheet1Name := "Sheet One"
	xlsx.SetSheetName(xlsx.GetSheetName(1), sheet1Name)

	xlsx.SetCellValue(sheet1Name, "A1", "IDIdentitasJurnal")
	xlsx.SetCellValue(sheet1Name, "B1", "IdUsulanAkreditasi")
	xlsx.SetCellValue(sheet1Name, "C1", "IdPic1")
	xlsx.SetCellValue(sheet1Name, "D1", "IdPic2")
	xlsx.SetCellValue(sheet1Name, "E1", "IdPersonal")
	xlsx.SetCellValue(sheet1Name, "F1", "NamaJurnal")
	xlsx.SetCellValue(sheet1Name, "G1", "Publisher")
	xlsx.SetCellValue(sheet1Name, "H1", "UrlJurnal")
	xlsx.SetCellValue(sheet1Name, "I1", "BidangIlmu")
	xlsx.SetCellValue(sheet1Name, "J1", "TglUsulanAkreditasiTerakhir")
	xlsx.SetCellValue(sheet1Name, "K1", "TglCreatedUsulan")
	xlsx.SetCellValue(sheet1Name, "L1", "TglUpdatedUsulan")
	xlsx.SetCellValue(sheet1Name, "M1", "TglEvaluasidiri")
	xlsx.SetCellValue(sheet1Name, "N1", "NilaiEvaluasi")
	xlsx.SetCellValue(sheet1Name, "O1", "IDPenetapanDeskEvaluasi")
	xlsx.SetCellValue(sheet1Name, "P1", "DeskCreate")
	xlsx.SetCellValue(sheet1Name, "Q1", "DeskUpdate")
	xlsx.SetCellValue(sheet1Name, "R1", "StsHasilDeskEvaluasi")
	xlsx.SetCellValue(sheet1Name, "S1", "PenugasanIssueCreated")
	xlsx.SetCellValue(sheet1Name, "T1", "PenilaiIssue")
	xlsx.SetCellValue(sheet1Name, "U1", "PenugasanIssueTerlaksana")
	xlsx.SetCellValue(sheet1Name, "V1", "PenugasanMgmtCreated")
	xlsx.SetCellValue(sheet1Name, "W1", "PenilaiMgmt")
	xlsx.SetCellValue(sheet1Name, "X1", "PenugasanMgmtTerlaksana")
	xlsx.SetCellValue(sheet1Name, "Y1", "Nilai")
	xlsx.SetCellValue(sheet1Name, "Z1", "PenetapanUpdated")
	xlsx.SetCellValue(sheet1Name, "AA1", "StsHasilAkreditasi")
	xlsx.SetCellValue(sheet1Name, "AB1", "AkreditasiUpdated")
	xlsx.SetCellValue(sheet1Name, "AC1", "NilaiAkreditasi")
	xlsx.SetCellValue(sheet1Name, "AD1", "GradeAkreditasi")

	err := xlsx.AutoFilter(sheet1Name, "A1", "AD1", "")
	if err != nil {
		return "", err
	}

	for i, V := range jurnal {
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("A%d", i+2), V.IDIdentitasJurnal)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("B%d", i+2), V.IdUsulanAkreditasi)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("C%d", i+2), V.IdPic1)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("D%d", i+2), V.IdPic2)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("E%d", i+2), V.IdPersonal)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("F%d", i+2), V.NamaJurnal)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("G%d", i+2), V.Publisher)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("H%d", i+2), V.UrlJurnal)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("I%d", i+2), V.BidangIlmu)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("J%d", i+2), V.TglUsulanAkreditasiTerakhir)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("K%d", i+2), V.TglCreatedUsulan)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("L%d", i+2), V.TglUpdatedUsulan)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("M%d", i+2), V.TglEvaluasidiri)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("N%d", i+2), V.NilaiEvaluasi)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("O%d", i+2), V.IDPenetapanDeskEvaluasi)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("P%d", i+2), V.DeskCreate)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("Q%d", i+2), V.DeskUpdate)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("R%d", i+2), V.StsHasilDeskEvaluasi)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("S%d", i+2), V.PenugasanIssueCreated)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("T%d", i+2), V.PenilaiIssue)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("U%d", i+2), V.PenugasanIssueTerlaksana)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("V%d", i+2), V.PenugasanMgmtCreated)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("W%d", i+2), V.PenilaiMgmt)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("X%d", i+2), V.PenugasanMgmtTerlaksana)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("Y%d", i+2), V.Nilai)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("Z%d", i+2), V.PenetapanUpdated)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AA%d", i+2), V.StsHasilAkreditasi)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AB%d", i+2), V.AkreditasiUpdated)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AC%d", i+2), V.NilaiAkreditasi)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AD%d", i+2), V.GradeAkreditasi)
	}
	namaFile := fmt.Sprintf("%s_ARJUNA-DaftarUsulanTerdaftar.xlsx", time.Now().Format("20060130"))
	
	//remark by agus (geberate & upload to google storage)
	// path := "./files/excel/" + namaFile
	// err = xlsx.SaveAs(path)
	// if err != nil {
	// 	return "", err
	// }
	// res := baseUrl + "/file/excel/" + namaFile
	// return res, nil
	excelBytes, err := xlsx.WriteToBuffer()
	path, err  := helper.UploadFileXls("file/excel/", namaFile, excelBytes.Bytes())
	if err != nil {
		return "", err
	}
 
	result := baseUrl + "/file/excel/" + path
	return result, nil

}

func ExportKelolaUsulanAkreditasiBaru(search string, status string, baseUrl string) (string, error) {
	var akreditasi_baru []entity.UsulanAkreditasiBaru

	db := config.Con()
	defer config.CloseCon(db)

	sub_q := db.Table("penetapan_desk_evaluasi")
	sub_q.Select("id_usulan_akreditasi")

	bidang_ilmu := db.Table("bidang_ilmu_jurnal ta")
	bidang_ilmu.Select("ta.id_jurnal, array_to_string(ARRAY_AGG(tb.bidang_ilmu), ', ') as bidang_ilmu")
	bidang_ilmu.Joins("INNER JOIN public.bidang_ilmu tb ON ta.id_bidang_ilmu = tb.id_bidang_ilmu")
	bidang_ilmu.Where("ta.sts_aktif_bidang_ilmu_jurnal = ?", "1")
	bidang_ilmu.Group("ta.id_jurnal")

	alasan_penolakan := db.Table("usulan_akreditasi_ditolak as uad")
	alasan_penolakan.Select("uad.alasan_penolakan")
	alasan_penolakan.Joins("LEFT JOIN usulan_akreditasi as ua on ua.id_usulan_akreditasi = uad.id_usulan_akreditasi")
	alasan_penolakan.Where("ua.id_identitas_jurnal = t2.id_identitas_jurnal")
	alasan_penolakan.Order("uad.tgl_created desc")
	alasan_penolakan.Limit(1)

	pemberi_komentar := db.Table("usulan_akreditasi_ditolak as uad")
	pemberi_komentar.Select("p.nama as pemberi_komentar")
	pemberi_komentar.Joins("LEFT JOIN usulan_akreditasi as ua on ua.id_usulan_akreditasi = uad.id_usulan_akreditasi")
	pemberi_komentar.Joins("LEFT JOIN personal as p on p.id_personal = uad.id_personal")
	pemberi_komentar.Where("ua.id_identitas_jurnal = t2.id_identitas_jurnal")
	pemberi_komentar.Order("uad.tgl_created desc")
	pemberi_komentar.Limit(1)

	status_usulan_akreditasi := db.Table("issue")
	status_usulan_akreditasi.Select("count(*) = 1")
	status_usulan_akreditasi.Where("id_usulan_akreditasi = t2.id_usulan_akreditasi")

	data := db.Table("usulan_akreditasi AS t2")
	data.Select("t2.id_usulan_akreditasi, t2.id_identitas_jurnal, t3.nama_jurnal, t2.id_pic, t2.id_eic, t2.user_penilai, t2.passwd_penilai, t2.file_dokumen_usulan, t2.tgl_updated AS tgl_pengajuan_usulan, t3.eissn, t3.pissn, t3.publisher, t3.society, t3.alamat_oai, t3.doi_jurnal, t5.nama_awal_jurnal, t5.tgl_pembuatan, t5.url_jurnal, t5.url_contact, t5.url_editor, t5.country, t5.city, t5.alamat AS alamat_jurnal, t5.no_telepon AS no_telepon_jurnal, t5.alamat_surel AS alamat_surel_jurnal, t5.tgl_akhir_terakreditasi, t5.url_statistik_pengunjung, t7.nama AS nama_pic, t7.nama_institusi, t7.email AS email_pic, t7.no_hand_phone AS no_hand_phone_pic, t8.nama_eic, t8.no_telepon AS no_telepon_eic, t8.alamat_surel AS alamat_surel_eic, t1.nilai_total, t9.bidang_ilmu, ( ? ) as alasan_penolakan, ( ? ) as pemberi_komentar, ( ? ) as status_usulan_akreditasi", alasan_penolakan, pemberi_komentar, status_usulan_akreditasi)
	data.Joins("INNER JOIN hasil_sa as t1 on t1.id_usulan_akreditasi = t2.id_usulan_akreditasi")
	data.Joins("INNER JOIN identitas_jurnal AS t3 ON t3.id_identitas_jurnal = t2.id_identitas_jurnal")
	data.Joins("INNER JOIN progres_usulan_akreditasi AS t4 ON t4.id_usulan_akreditasi = t2.id_usulan_akreditasi")
	data.Joins("INNER JOIN jurnal AS t5 ON t5.id_jurnal = t3.id_jurnal")
	data.Joins("INNER JOIN pic AS t6 ON t6.id_pic = t2.id_pic")
	data.Joins("INNER JOIN personal AS t7 ON t7.id_personal = t6.id_personal")
	data.Joins("INNER JOIN eic AS t8 ON t8.id_eic = t2.id_eic ")
	data.Joins("LEFT JOIN ( ? ) as t9 ON t9.id_jurnal = t5.id_jurnal", bidang_ilmu)
	data.Where("t2.id_usulan_akreditasi NOT IN ( ? )", sub_q)
	data.Where("t2.sts_pengajuan_usulan = ?", "1")

	//remark by agus (semua usulan baru diambil baik nilai >=70 or < 70)
	/*if status == "1" {
		data.Where("t1.nilai_total >= ?", "70")
	} else {
		data.Where("t1.nilai_total < ?", "70")
	}*/
	
	data.Where("t3.sts_aktif_identitas_jurnal = ?", "1")
	data.Where("t4.id_progres = ?", "1")
	data.Where("t3.nama_jurnal IS NOT NULL")
	if search != "" {
		data.Where("lower(t3.nama_jurnal) LIKE ?", "%"+strings.ToLower(search)+"%")
	}
	data.Order("t2.tgl_updated asc")
	data.Find(&akreditasi_baru)
	if data.Error != nil {
		return "", data.Error
	}

	xlsx := excelize.NewFile()
	sheet1Name := "Sheet One"
	xlsx.SetSheetName(xlsx.GetSheetName(1), sheet1Name)

	xlsx.SetCellValue(sheet1Name, "A1", "ID Usulan Akreditasi")
	xlsx.SetCellValue(sheet1Name, "B1", "ID Identitas Jurnal")
	xlsx.SetCellValue(sheet1Name, "C1", "Nama Jurnal")
	xlsx.SetCellValue(sheet1Name, "D1", "e-ISSN")
	xlsx.SetCellValue(sheet1Name, "E1", "p-ISSN")
	xlsx.SetCellValue(sheet1Name, "F1", "Penerbit")
	xlsx.SetCellValue(sheet1Name, "G1", "Penerbit	Komunitas")
	xlsx.SetCellValue(sheet1Name, "H1", "Nama Terbitan Pertama Kali")
	xlsx.SetCellValue(sheet1Name, "I1", "Tanggal Pembuatan")
	xlsx.SetCellValue(sheet1Name, "J1", "URL Journal")
	xlsx.SetCellValue(sheet1Name, "K1", "URL Contact")
	xlsx.SetCellValue(sheet1Name, "L1", "URL Editor")
	xlsx.SetCellValue(sheet1Name, "M1", "Negara")
	xlsx.SetCellValue(sheet1Name, "N1", "Kota")
	xlsx.SetCellValue(sheet1Name, "O1", "Alamat Surat")
	xlsx.SetCellValue(sheet1Name, "P1", "Nomor telepon")
	xlsx.SetCellValue(sheet1Name, "Q1", "Email")
	xlsx.SetCellValue(sheet1Name, "R1", "Tanggal Selesai Terakreditasi")
	xlsx.SetCellValue(sheet1Name, "S1", "PIC ID")
	xlsx.SetCellValue(sheet1Name, "T1", "PIC Name")
	xlsx.SetCellValue(sheet1Name, "U1", "Nama Institusi")
	xlsx.SetCellValue(sheet1Name, "V1", "Alamat Surat PIC")
	xlsx.SetCellValue(sheet1Name, "W1", "Nomor Telepon PIC")
	xlsx.SetCellValue(sheet1Name, "X1", "EIC ID")
	xlsx.SetCellValue(sheet1Name, "Y1", "EIC Name")
	xlsx.SetCellValue(sheet1Name, "Z1", "EIC Nomor telepon")
	xlsx.SetCellValue(sheet1Name, "AA1", "EIC Alamat Surat")
	xlsx.SetCellValue(sheet1Name, "AB1", "URL Statistik")
	xlsx.SetCellValue(sheet1Name, "AC1", "URL OAI")
	xlsx.SetCellValue(sheet1Name, "AD1", "DOI Journal")
	xlsx.SetCellValue(sheet1Name, "AE1", "Username Assesor")
	xlsx.SetCellValue(sheet1Name, "AF1", "Password Assesor")
	xlsx.SetCellValue(sheet1Name, "AG1", "File Docs")
	xlsx.SetCellValue(sheet1Name, "AH1", "Tanggal Terakhir Diajukan")
	xlsx.SetCellValue(sheet1Name, "AI1", "Nilai SA")
	xlsx.SetCellValue(sheet1Name, "AJ1", "Bidang")
	xlsx.SetCellValue(sheet1Name, "AK1", "Komentar")
	xlsx.SetCellValue(sheet1Name, "AL1", "Journal Status")

	err := xlsx.AutoFilter(sheet1Name, "A1", "AL1", "")
	if err != nil {
		return "", err
	}

	for i, V := range akreditasi_baru {
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("A%d", i+2), V.IdUsulanAkreditasi)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("B%d", i+2), V.IdIdentitasJurnal)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("C%d", i+2), V.NamaJurnal)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("D%d", i+2), V.Eissn)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("E%d", i+2), V.Pissn)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("F%d", i+2), V.Publisher)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("G%d", i+2), V.Society)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("H%d", i+2), V.NamaAwalJurnal)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("I%d", i+2), V.TglPembuatan)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("J%d", i+2), V.UrlJurnal)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("K%d", i+2), V.UrlContact)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("L%d", i+2), V.UrlEditor)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("M%d", i+2), V.Country)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("N%d", i+2), V.City)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("O%d", i+2), V.AlamatJurnal)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("P%d", i+2), V.NoTeleponJurnal)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("Q%d", i+2), V.AlamatSurelJurnal)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("R%d", i+2), V.TglAkhirTerakreditasi)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("S%d", i+2), V.IdPic)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("T%d", i+2), V.NamaPic)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("U%d", i+2), V.NamaInstitusi)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("V%d", i+2), V.EmailPic)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("W%d", i+2), V.NoHandPhonePic)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("X%d", i+2), V.IdEic)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("Y%d", i+2), V.NamaEic)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("Z%d", i+2), V.NoTeleponEic)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AA%d", i+2), V.AlamatSurelEic)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AB%d", i+2), V.UrlStatistikPengunjung)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AC%d", i+2), V.AlamatOai)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AD%d", i+2), V.DoiJurnal)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AE%d", i+2), V.UserPenilai)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AF%d", i+2), V.PasswdPenilai)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AG%d", i+2), V.FileDokumenUsulan)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AH%d", i+2), V.TglPengajuanUsulan)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AI%d", i+2), V.NilaiTotal)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AJ%d", i+2), V.BidangIlmu)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AK%d", i+2), V.AlasanPenolakan)
		if V.StatusUsulanAkreditasi == true {
			xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AL%d", i+2), "Re-akreditasi")
		} else {
			xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AL%d", i+2), "Usulan Baru")
		}
	}
	
	//remark by agus (semua usulan baru diambil baik nilai >=70 or < 70)
	/*var text string	
	if status == "1" {
		text = "Higher70"
	} else {
		text = "Lower70"
	}*/
	
	//remark by agus (geberate & upload to google storage)
	//namaFile := fmt.Sprintf("ARJUNA-RecentProposal(%s).xlsx", text)
	namaFile := fmt.Sprintf("%s_ARJUNA-UsulanAkreditasiBaru.xlsx", time.Now().Format("20060130"))

	// path := "./files/excel/" + namaFile
	// err = xlsx.SaveAs(path)
	// if err != nil {
	// 	return "", err
	// }
	// res := baseUrl + "/file/excel/" + namaFile
	// return res, nil

	excelBytes, err := xlsx.WriteToBuffer()
	path, err  := helper.UploadFileXls("file/excel/", namaFile, excelBytes.Bytes())
	if err != nil {
		return "", err
	}
	result := baseUrl + "/file/excel/" + path
	return result, nil

}

func ExportKelolaUsulanEvaluasiDokumen(search string, status string, baseUrl string, evaluasi string) (string, error) {
	var evaluasi_dokumen []entity.UsulanAkreditasiBaru

	db := config.Con()
	defer config.CloseCon(db)

	resubmit := db.Table("resubmit as ta")
	resubmit.Select("ta.id_resubmit, ta.id_usulan_akreditasi, ta.alasan_resubmit, ta.tgl_batas_resubmit")
	resubmit.Where("ta.kd_sts_resubmit = ?", "1")
	resubmit.Where("ta.tgl_batas_resubmit >= now()")

	status_usulan_akreditasi := db.Table("issue")
	status_usulan_akreditasi.Select("count(*) = 1")
	status_usulan_akreditasi.Where("id_usulan_akreditasi = t1.id_usulan_akreditasi")

	data := db.Table("usulan_akreditasi AS t1").Distinct()
	data.Select("t1.id_usulan_akreditasi, t1.id_identitas_jurnal, t3.nama_jurnal, t3.eissn, t3.pissn, t3.publisher, t3.society, t4.nama_awal_jurnal, t4.tgl_pembuatan, t4.url_jurnal, t4.url_contact, t4.url_editor, t4.country, t4.city, t4.alamat AS alamat_jurnal, t4.no_telepon AS no_telepon_jurnal, t4.alamat_surel AS alamat_surel_jurnal, t4.tgl_akhir_terakreditasi, t1.id_pic, t6.nama AS nama_pic, t6.nama_institusi, t6.email AS email_pic,t6.no_hand_phone AS no_hand_phone_pic, t1.id_eic, t7.nama_eic, t7.no_telepon AS no_telepon_eic, t7.alamat_surel AS alamat_surel_eic, t4.url_statistik_pengunjung, t3.alamat_oai, t3.doi_jurnal, t3.url_googlescholar, t1.user_penilai, t1.passwd_penilai, t1.file_dokumen_usulan, t1.tgl_updated AS tgl_pengajuan_usulan, t10.alasan_penolakan, t11.id_resubmit, t11.alasan_resubmit, t11.tgl_batas_resubmit, t9.bidang_ilmu, t2.nilai_total, t12.nama as pemberi_komentar, ( ? ) as status_usulan_akreditasi,t13.nama_bidang_fokus as bidang_fokus, t14.institusi as kode_pt, t3.nama_pt as institusi", status_usulan_akreditasi)
	data.Joins("INNER JOIN hasil_sa AS t2 ON t2.id_usulan_akreditasi = t1.id_usulan_akreditasi")
	data.Joins("INNER JOIN identitas_jurnal AS t3 ON t3.id_identitas_jurnal = t1.id_identitas_jurnal")
	data.Joins("INNER JOIN jurnal AS t4 ON t4.id_jurnal = t3.id_jurnal")
	data.Joins("INNER JOIN pic AS t5 ON t5.id_pic = t1.id_pic")
	data.Joins("INNER JOIN personal AS t6 ON t6.id_personal = t5.id_personal")
	data.Joins("INNER JOIN eic AS t7 ON t7.id_eic = t1.id_eic")
	data.Joins("INNER JOIN bidang_ilmu_jurnal AS t8 ON t8.id_jurnal = t3.id_jurnal")
	data.Joins("INNER JOIN bidang_ilmu AS t9 ON t9.id_bidang_ilmu = t8.id_bidang_ilmu AND t8.sts_aktif_bidang_ilmu_jurnal = '1' AND t9.sts_aktif_bidang_ilmu = '1'")
	data.Joins("INNER JOIN usulan_akreditasi_ditolak AS t10 ON t10.id_usulan_akreditasi = t1.id_usulan_akreditasi AND t10.kd_sts_aktif = '1'")
	data.Joins("LEFT JOIN ( ? ) as t11 on t11.id_usulan_akreditasi = t1.id_usulan_akreditasi", resubmit)
	data.Joins("INNER JOIN personal as t12 on t12.id_personal = t10.id_personal")
	data.Joins("LEFT JOIN bidang_fokus as t13 on t3.id_bidang_fokus = t13.id_bidang_fokus")
	data.Joins("LEFT JOIN perguruan_tinggi as t14 on t3.nama_pt = t14.nama_pt")
	data.Where("t3.sts_aktif_identitas_jurnal = ?", "1")
	if search != "" {
		data.Where("lower(t3.nama_jurnal) LIKE ?", "%"+strings.ToLower(search)+"%")
	}
	
	//remark by agus (semua usulan baru diambil baik nilai >=70 or < 70)
	/*if status == "1" {
		data.Where("t2.nilai_total >= ?", "70")
	} else {
		data.Where("t2.nilai_total < ?", "70")
	}*/

	data.Where("t3.nama_jurnal IS NOT NULL")
	data.Order("tgl_pengajuan_usulan DESC")
	data.Find(&evaluasi_dokumen)
	if data.Error != nil {
		return "", data.Error
	}

	xlsx := excelize.NewFile()
	sheet1Name := "Sheet One"
	xlsx.SetSheetName(xlsx.GetSheetName(1), sheet1Name)

	xlsx.SetCellValue(sheet1Name, "A1", "ID Usulan Akreditasi")
	xlsx.SetCellValue(sheet1Name, "B1", "ID Identitas Jurnal")
	xlsx.SetCellValue(sheet1Name, "C1", "Nama Jurnal")
	xlsx.SetCellValue(sheet1Name, "D1", "e-ISSN")
	xlsx.SetCellValue(sheet1Name, "E1", "p-ISSN")
	xlsx.SetCellValue(sheet1Name, "F1", "Penerbit")
	xlsx.SetCellValue(sheet1Name, "G1", "Penerbit	Komunitas")
	xlsx.SetCellValue(sheet1Name, "H1", "Nama Terbitan Pertama Kali")
	xlsx.SetCellValue(sheet1Name, "I1", "Tanggal Pembuatan")
	xlsx.SetCellValue(sheet1Name, "J1", "URL Journal")
	xlsx.SetCellValue(sheet1Name, "K1", "URL Contact")
	xlsx.SetCellValue(sheet1Name, "L1", "URL Editor")
	xlsx.SetCellValue(sheet1Name, "M1", "Negara")
	xlsx.SetCellValue(sheet1Name, "N1", "Kota")
	xlsx.SetCellValue(sheet1Name, "O1", "Alamat Surat")
	xlsx.SetCellValue(sheet1Name, "P1", "Nomor telepon")
	xlsx.SetCellValue(sheet1Name, "Q1", "Email")
	xlsx.SetCellValue(sheet1Name, "R1", "Tanggal Selesai Terakreditasi")
	xlsx.SetCellValue(sheet1Name, "S1", "PIC ID")
	xlsx.SetCellValue(sheet1Name, "T1", "PIC Name")
	xlsx.SetCellValue(sheet1Name, "U1", "Nama Institusi")
	xlsx.SetCellValue(sheet1Name, "V1", "Alamat Surat PIC")
	xlsx.SetCellValue(sheet1Name, "W1", "Nomor Telepon PIC")
	xlsx.SetCellValue(sheet1Name, "X1", "EIC ID")
	xlsx.SetCellValue(sheet1Name, "Y1", "EIC Name")
	xlsx.SetCellValue(sheet1Name, "Z1", "EIC Nomor telepon")
	xlsx.SetCellValue(sheet1Name, "AA1", "EIC Alamat Surat")
	xlsx.SetCellValue(sheet1Name, "AB1", "URL Statistik")
	xlsx.SetCellValue(sheet1Name, "AC1", "URL OAI")
	xlsx.SetCellValue(sheet1Name, "AD1", "DOI Journal")
	xlsx.SetCellValue(sheet1Name, "AE1", "Username Assesor")
	xlsx.SetCellValue(sheet1Name, "AF1", "Password Assesor")
	xlsx.SetCellValue(sheet1Name, "AG1", "File Docs")
	xlsx.SetCellValue(sheet1Name, "AH1", "Tanggal Terakhir Diajukan")
	xlsx.SetCellValue(sheet1Name, "AI1", "Nilai SA")
	xlsx.SetCellValue(sheet1Name, "AJ1", "Bidang Ilmu")
	// xlsx.SetCellValue(sheet1Name, "AK1", "Komentar")
	// xlsx.SetCellValue(sheet1Name, "AL1", "Journal Status")

	xlsx.SetCellValue(sheet1Name, "AK1", "Bidang Fokus")
	xlsx.SetCellValue(sheet1Name, "AL1", "Kode PT")
	xlsx.SetCellValue(sheet1Name, "AM1", "Institusi")
	xlsx.SetCellValue(sheet1Name, "AN1", "Komentar")
	xlsx.SetCellValue(sheet1Name, "AO1", "Journal Status")


	err := xlsx.AutoFilter(sheet1Name, "A1", "AL1", "")
	if err != nil {
		return "", err
	}

	for i, V := range evaluasi_dokumen {
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("A%d", i+2), V.IdUsulanAkreditasi)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("B%d", i+2), V.IdIdentitasJurnal)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("C%d", i+2), V.NamaJurnal)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("D%d", i+2), V.Eissn)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("E%d", i+2), V.Pissn)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("F%d", i+2), V.Publisher)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("G%d", i+2), V.Society)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("H%d", i+2), V.NamaAwalJurnal)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("I%d", i+2), V.TglPembuatan)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("J%d", i+2), V.UrlJurnal)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("K%d", i+2), V.UrlContact)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("L%d", i+2), V.UrlEditor)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("M%d", i+2), V.Country)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("N%d", i+2), V.City)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("O%d", i+2), V.AlamatJurnal)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("P%d", i+2), V.NoTeleponJurnal)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("Q%d", i+2), V.AlamatSurelJurnal)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("R%d", i+2), V.TglAkhirTerakreditasi)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("S%d", i+2), V.IdPic)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("T%d", i+2), V.NamaPic)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("U%d", i+2), V.NamaInstitusi)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("V%d", i+2), V.EmailPic)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("W%d", i+2), V.NoHandPhonePic)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("X%d", i+2), V.IdEic)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("Y%d", i+2), V.NamaEic)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("Z%d", i+2), V.NoTeleponEic)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AA%d", i+2), V.AlamatSurelEic)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AB%d", i+2), V.UrlStatistikPengunjung)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AC%d", i+2), V.AlamatOai)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AD%d", i+2), V.DoiJurnal)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AE%d", i+2), V.UserPenilai)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AF%d", i+2), V.PasswdPenilai)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AG%d", i+2), V.FileDokumenUsulan)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AH%d", i+2), V.TglPengajuanUsulan)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AI%d", i+2), V.NilaiTotal)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AJ%d", i+2), V.BidangIlmu)

		// xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AK%d", i+2), V.AlasanPenolakan)
		// if V.StatusUsulanAkreditasi == true {
		// 	xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AL%d", i+2), "Re-akreditasi")
		// } else {
		// 	xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AL%d", i+2), "Usulan Baru")
		// }

		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AK%d", i+2), V.BidangFokus)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AL%d", i+2), V.KodePT)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AM%d", i+2), V.Institusi)

		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AN%d", i+2), V.AlasanPenolakan)
		if V.StatusUsulanAkreditasi == true {
			xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AO%d", i+2), "Re-akreditasi")
		} else {
			xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AO%d", i+2), "Usulan Baru")
		}

	}
	//remark by agus (semua usulan baru diambil baik nilai >=70 or < 70)
	/*var text string	
	if status == "1" {
		text = "Higher70"
	} else {
		text = "Lower70"
	}*/

	//namaFile := fmt.Sprintf("ARJUNA-RejectedProposal(%s).xlsx", text)
	// namaFile := fmt.Sprintf("ARJUNA-UsulanDitolak.xlsx")
    namaFile := fmt.Sprintf("%s_ARJUNA-UsulanDitolak.xlsx", time.Now().Format("20060130"))
	 
	//remark by agus (geberate & upload to google storage)
	// path := "./files/excel/" + namaFile
	// err = xlsx.SaveAs(path)
	// if err != nil {
	// 	return "", err
	// }
	// res := baseUrl + "/file/excel/" + namaFile
	// return res, nil

	excelBytes, err := xlsx.WriteToBuffer()
	path, err  := helper.UploadFileXls("file/excel/", namaFile, excelBytes.Bytes())
	if err != nil {
		return "", err
	}

	result := baseUrl + "/file/excel/" + path
	return result, nil
}

func ExportKinerjaPenilai(req entity.KinerjaPenilaiDTO, baseUrl string) (string, error) {
	var res []entity.KinerjaPenilai
	var err error
	var text string

	if req.Penilai == "management" {
		data, errs := getKinerjaPenilaiManagement(req.Periode)
		text = "management"
		res = data
		err = errs
	} else if req.Penilai == "issue" {
		data, errs := getKinerjaPenilaiIssue(req.Periode)
		text = "issue"
		res = data
		err = errs
	} else if req.Penilai == "evaluator" {
		data, errs := getKinerjaPenilaiEvaluator(req.Periode)
		text = "evaluator"
		res = data
		err = errs
	}

	xlsx := excelize.NewFile()
	sheet1Name := "Sheet One"
	xlsx.SetSheetName(xlsx.GetSheetName(1), sheet1Name)

	xlsx.SetCellValue(sheet1Name, "A1", "ID")
	xlsx.SetCellValue(sheet1Name, "B1", "Nama Penilai")
	xlsx.SetCellValue(sheet1Name, "C1", "Ditawarkan")
	xlsx.SetCellValue(sheet1Name, "D1", "Belum Diputuskan")
	xlsx.SetCellValue(sheet1Name, "E1", "Ditolak")
	xlsx.SetCellValue(sheet1Name, "F1", "Diterima")
	xlsx.SetCellValue(sheet1Name, "G1", "Belum Dilaksanakan")
	xlsx.SetCellValue(sheet1Name, "H1", "Dilaksanakan")
	xlsx.SetCellValue(sheet1Name, "I1", "Ditetapkan")

	errs := xlsx.AutoFilter(sheet1Name, "A1", "I1", "")
	if errs != nil {
		return "", errs
	}

	for i, V := range res {
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("A%d", i+2), V.IDPersonal)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("B%d", i+2), V.Nama)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("C%d", i+2), V.JmlDitawarkan)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("D%d", i+2), V.JmlBlmDiputuskan)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("E%d", i+2), V.JmlDitolak)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("F%d", i+2), V.JmlDiterima)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("G%d", i+2), V.JmlBelumdilaksanakan)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("H%d", i+2), V.JmlDilaksanakan)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("I%d", i+2), V.JmlDitetapkan)
	}

    //remark by agus (geberate & upload to google storage)
	// namaFile := fmt.Sprintf("ARJUNA-Kinerja.xlsx")
	// path := "./files/excel/" + namaFile
	// err = xlsx.SaveAs(path)
	// if err != nil {
	// 	return "", err
	// }
	// result := baseUrl + "/file/excel/" + namaFile
	// return result, nil

	// namaFile := fmt.Sprintf("%s_ARJUNA-Kinerja.xlsx", time.Now().Format("20060130"))
	namaFile := fmt.Sprintf("%s_ARJUNA-Kinerja-%s.xlsx", time.Now().Format("20060130"),text)
	excelBytes, err := xlsx.WriteToBuffer()
	path, err  := helper.UploadFileXls("file/excel/", namaFile, excelBytes.Bytes())
	if err != nil {
		return "", err
	}

	result := baseUrl + "/file/excel/" + path
	return result, nil

}

//Add By agus 20230604
func  ExportKelolaUsulanDistribusiPenilaian(search string, status string, baseUrl string) (string, error) {
	var res []entity.DistribusiPenilaian

	db := config.Con()
	defer config.CloseCon(db)
	// query := db.Raw("SELECT id_usulan_akreditasi, id_identitas_jurnal, nama_jurnal, eissn, pissn,publisher, society, nama_awal_jurnal, tgl_pembuatan, url_jurnal, url_contact, url_editor, country, city, alamat_jurnal, no_telepon_jurnal, alamat_surel_jurnal, tgl_akhir_terakreditasi, id_pic, nama_pic, nama_institusi, email_pic, no_hand_phone_pic, id_eic, nama_eic, no_telepon_eic, alamat_surel_eic, alamat_oai, doi_jurnal, url_statistik_pengunjung, user_penilai, passwd_penilai, file_dokumen_usulan, tgl_pengajuan_usulan, tgl_desk_evaluasi, jml_artikel, jml_artikel_tdk_terpenuhi, jml_asessor_artikel_ditugaskan, jml_asessor_artikel_menerima, jml_asessor_artikel_menolak, jml_min_asessor_artikel, jml_asessor_mgmt_ditugaskan, jml_asessor_mgmt_menerima, jml_asessor_mgmt_menolak, jml_min_asessor_mgmt, nama_asessor, bidang_ilmu, status_usulan_akreditasi, jml_record  from z_arjuna_list_penilaian_belum_mulai (( ? ), '1', ( ? ), ( ? ))", 0, search, 10000)
	query := db.Raw("SELECT lp.id_usulan_akreditasi, lp.id_identitas_jurnal, lp.nama_jurnal, lp.eissn, lp.pissn,lp.publisher, lp.society, lp.nama_awal_jurnal, lp.tgl_pembuatan, lp.url_jurnal, lp.url_contact, lp.url_editor, lp.country, lp.city, lp.alamat_jurnal, lp.no_telepon_jurnal, lp.alamat_surel_jurnal, lp.tgl_akhir_terakreditasi, lp.id_pic, lp.nama_pic, lp.nama_institusi, lp.email_pic, lp.no_hand_phone_pic, lp.id_eic, lp.nama_eic, lp.no_telepon_eic, lp.alamat_surel_eic, lp.alamat_oai, lp.doi_jurnal, lp.url_statistik_pengunjung,lp.user_penilai, lp.passwd_penilai, lp.file_dokumen_usulan, lp.tgl_pengajuan_usulan, lp.tgl_desk_evaluasi, lp.jml_artikel, lp.jml_artikel_tdk_terpenuhi, lp.jml_asessor_artikel_ditugaskan, lp.jml_asessor_artikel_menerima, lp.jml_asessor_artikel_menolak, lp.jml_min_asessor_artikel, lp.jml_asessor_mgmt_ditugaskan, lp.jml_asessor_mgmt_menerima, lp.jml_asessor_mgmt_menolak, lp.jml_min_asessor_mgmt, lp.nama_asessor, lp.bidang_ilmu, lp.status_usulan_akreditasi,bf.nama_bidang_fokus as bidang_fokus, pt.institusi as kode_pt, ij.nama_pt as institusi,lp.jml_record from z_arjuna_list_penilaian_belum_mulai (( ? ), '1', ( ? ), ( ? )) AS lp LEFT JOIN identitas_jurnal as ij on lp.id_identitas_jurnal = ij.id_identitas_jurnal LEFT JOIN bidang_fokus as bf on ij.id_bidang_fokus = bf.id_bidang_fokus LEFT JOIN perguruan_tinggi as pt on ij.nama_pt = pt.nama_pt", 0, search, 10000)
	query.Scan(&res)

	if query.Error != nil {
		return "", query.Error
	}


	xlsx := excelize.NewFile()
	sheet1Name := "Sheet One"
	xlsx.SetSheetName(xlsx.GetSheetName(1), sheet1Name)

	xlsx.SetCellValue(sheet1Name, "A1", "ID Usulan Akreditasi")
	xlsx.SetCellValue(sheet1Name, "B1", "ID Identitas Jurnal")
	xlsx.SetCellValue(sheet1Name, "C1", "Nama Jurnal")
	xlsx.SetCellValue(sheet1Name, "D1", "e-ISSN")
	xlsx.SetCellValue(sheet1Name, "E1", "p-ISSN")
	xlsx.SetCellValue(sheet1Name, "F1", "Penerbit")
	xlsx.SetCellValue(sheet1Name, "G1", "Komunitas")
	xlsx.SetCellValue(sheet1Name, "H1", "Nama Terbitan Pertama Kali")
	xlsx.SetCellValue(sheet1Name, "I1", "Tanggal Pembuatan")
	xlsx.SetCellValue(sheet1Name, "J1", "URL Journal")
	xlsx.SetCellValue(sheet1Name, "K1", "URL Contact")
	xlsx.SetCellValue(sheet1Name, "L1", "URL Editor")
	xlsx.SetCellValue(sheet1Name, "M1", "Negara")
	xlsx.SetCellValue(sheet1Name, "N1", "Kota")
	xlsx.SetCellValue(sheet1Name, "O1", "Alamat Surat")
	xlsx.SetCellValue(sheet1Name, "P1", "Nomor telepon")
	xlsx.SetCellValue(sheet1Name, "Q1", "Email")
	xlsx.SetCellValue(sheet1Name, "R1", "Tanggal Selesai Terakreditasi")
	xlsx.SetCellValue(sheet1Name, "S1", "PIC ID")
	xlsx.SetCellValue(sheet1Name, "T1", "PIC Name")
	xlsx.SetCellValue(sheet1Name, "U1", "Nama Institusi")
	xlsx.SetCellValue(sheet1Name, "V1", "Alamat Surat PIC")
	xlsx.SetCellValue(sheet1Name, "W1", "Nomor telepon PIC")
	xlsx.SetCellValue(sheet1Name, "X1", "EIC ID")
	xlsx.SetCellValue(sheet1Name, "Y1", "EIC Name")
	xlsx.SetCellValue(sheet1Name, "Z1", "EIC Nomor telepon")
	xlsx.SetCellValue(sheet1Name, "AA1", "EIC Alamat Surat")
	xlsx.SetCellValue(sheet1Name, "AB1", "URL OAI")
	xlsx.SetCellValue(sheet1Name, "AC1", "DOI Journal")
	xlsx.SetCellValue(sheet1Name, "AD1", "URL Statistik")
	xlsx.SetCellValue(sheet1Name, "AE1", "Username Assesor")
	xlsx.SetCellValue(sheet1Name, "AF1", "Password Assesor")
	xlsx.SetCellValue(sheet1Name, "AG1", "File Docs")
	xlsx.SetCellValue(sheet1Name, "AH1", "Tanggal Terakhir Diajukan")
	xlsx.SetCellValue(sheet1Name, "AI1", "Tanggal Penetapan Desk Evaluasi")
	xlsx.SetCellValue(sheet1Name, "AJ1", "Total Artikel")
	xlsx.SetCellValue(sheet1Name, "AK1", "Total Artikel tidak terpenuhi")
	xlsx.SetCellValue(sheet1Name, "AL1", "Total Assessor Article Ditugaskan")
	xlsx.SetCellValue(sheet1Name, "AM1", "Assessor Artikel Diterima")
	xlsx.SetCellValue(sheet1Name, "AN1", "Assessor Artikel Ditolak")
	xlsx.SetCellValue(sheet1Name, "AO1", "Min Assessor Artikel")
	xlsx.SetCellValue(sheet1Name, "AP1", "Total Assessor Manajemen Ditugaskan")
	xlsx.SetCellValue(sheet1Name, "AQ1", "Assessor Manajemen Diterima")
	xlsx.SetCellValue(sheet1Name, "AR1", "Assessor Manajemen Ditolak")
	xlsx.SetCellValue(sheet1Name, "AS1", "Min Assessor Manajemen")
	xlsx.SetCellValue(sheet1Name, "AT1", "Assessors")
	xlsx.SetCellValue(sheet1Name, "AU1", "Bidang Ilmu")

	xlsx.SetCellValue(sheet1Name, "AV1", "Bidang Fokus")
	xlsx.SetCellValue(sheet1Name, "AW1", "Kode PT")
	xlsx.SetCellValue(sheet1Name, "AX1", "Institusi")

	err := xlsx.AutoFilter(sheet1Name, "A1", "AS1", "")
	if err != nil {
		return "", err
	}

	for i, V := range res {
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("A%d", i+2), V.IdUsulanAkreditasi)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("B%d", i+2), V.IdIdentitasJurnal)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("C%d", i+2), V.NamaJurnal)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("D%d", i+2), V.Eissn)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("E%d", i+2), V.Pissn)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("F%d", i+2), V.Publisher)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("G%d", i+2), V.Society)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("H%d", i+2), V.NamaAwalJurnal)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("I%d", i+2), V.TglPembuatan)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("J%d", i+2), V.UrlJurnal)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("K%d", i+2), V.UrlContact)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("L%d", i+2), V.UrlEditor)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("M%d", i+2), V.Country)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("N%d", i+2), V.City)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("O%d", i+2), V.AlamatSurelJurnal)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("P%d", i+2), V.NoTeleponJurnal)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("Q%d", i+2), V.AlamatSurelJurnal)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("R%d", i+2), V.TglAkhirTerakreditasi)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("S%d", i+2), V.IdPic)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("T%d", i+2), V.NamaPic)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("U%d", i+2), V.NamaInstitusi)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("V%d", i+2), V.EmailPic)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("W%d", i+2), V.NoHandPhonePic)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("X%d", i+2), V.IdEic)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("Y%d", i+2), V.NamaEic)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("Z%d", i+2), V.NoTeleponEic)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AA%d", i+2), V.AlamatSurelEic)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AB%d", i+2), V.AlamatOai)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AC%d", i+2), V.DoiJurnal)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AD%d", i+2), V.UrlStatistikPengunjung)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AE%d", i+2), V.UserPenilai)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AF%d", i+2), V.PasswdPenilai)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AG%d", i+2), V.FileDokumenUsulan)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AH%d", i+2), V.TglPengajuanUsulan)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AI%d", i+2), V.TglDeskEvaluasi)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AJ%d", i+2), V.JmlArtikel)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AK%d", i+2), V.JmlArtikelTdkTerpenuhi)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AL%d", i+2), V.JmlAsessorArtikelDitugaskan)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AM%d", i+2), V.JmlAsessorArtikelMenerima)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AN%d", i+2), V.JmlAsessorArtikelMenolak)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AO%d", i+2), V.JmlMinAsessorArtikel)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AP%d", i+2), V.JmlAsessorMgmtDitugaskan)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AQ%d", i+2), V.JmlAsessorMgmtMenerima)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AR%d", i+2), V.JmlAsessorMgmtMenolak)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AS%d", i+2), V.JmlMinAsessorMgmt)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AT%d", i+2), V.NamaAsessor)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AU%d", i+2), V.BidangIlmu)
		
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AV%d", i+2), V.BidangFokus)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AW%d", i+2), V.KodePT)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AX%d", i+2), V.Institusi)

	}
 
	//remark by agus (geberate & upload to google storage)
	// namaFile := fmt.Sprintf("ARJUNA-UsulanDiterima.xlsx")

	// path := "./files/excel/" + namaFile
	// err = xlsx.SaveAs(path)
	// if err != nil {
	// 	return "", err
	// }
	// result := baseUrl + "/file/excel/" + namaFile
	// return result, nil

    namaFile := fmt.Sprintf("%s_ARJUNA-UsulanDiterima.xlsx", time.Now().Format("20060130"))
	 
	excelBytes, err := xlsx.WriteToBuffer()
	path, err  := helper.UploadFileXls("file/excel/", namaFile, excelBytes.Bytes())
	if err != nil {
		return "", err
	}

	result := baseUrl + "/file/excel/" + path
	return result, nil

}

//func ExportKelolaUsulanHasilAkreditasi(search string, page string, row string, kode_proses string, status string) (string, error) {
func ExportKelolaUsulanHasilAkreditasi(search string, status string, baseUrl string) (string, error) {	var res []entity.ProgresPenilaian

	db := config.Con()
	defer config.CloseCon(db)

	// data := db.Table("hasil_akreditasi as t1")
	// data.Distinct("t1.id_usulan_akreditasi, t1.tgl_updated :: date AS tgl_penetapan_akreditasi, t5.url_statistik_pengunjung, t4.nama_jurnal, t4.eissn, t4.pissn, t4.publisher, t4.society, t5.nama_awal_jurnal,t5.tgl_pembuatan, t5.url_jurnal, t5.url_contact, t5.url_editor, t5.country, t5.city, t5.alamat, t5.no_telepon, t5.alamat_surel, t4.alamat_oai, t4.doi_jurnal, t5.tgl_akhir_terakreditasi, t7.nama AS nama_pic, t7.nama_institusi, t8.nama_eic, t8.alamat_surel AS alamat_surel_eic, t1.nilai_total, t1.grade_akreditasi, t10.id_sk_akreditasi, t10.sts_published")
	// data.Joins("INNER JOIN usulan_akreditasi AS t3 ON t3.id_usulan_akreditasi = t1.id_usulan_akreditasi")
	// data.Joins("INNER JOIN identitas_jurnal AS t4 ON t4.id_identitas_jurnal = t3.id_identitas_jurnal")
	// data.Joins("INNER JOIN jurnal AS t5 ON t5.id_jurnal = t4.id_jurnal")
	// data.Joins("INNER JOIN pic AS t6 ON t6.id_pic = t3.id_pic")
	// data.Joins("INNER JOIN personal AS t7 ON t7.id_personal = t6.id_personal")
	// data.Joins("INNER JOIN eic AS t8 ON t8.id_eic = t3.id_eic")
	// data.Joins("LEFT JOIN penetapan_akreditasi AS t9 ON t9.id_usulan_akreditasi = t1.id_usulan_akreditasi")
	// data.Joins("LEFT JOIN sk_akreditasi AS t10 ON t10.id_sk_akreditasi = t9.id_sk_akreditasi")
	// data.Where("t4.nama_jurnal IS NOT NULL")
	
	// //remark by agus (semua usulan baru diambil baik nilai >=70 or < 70)
	// /*if status == "1" {
	// 	data.Where("t1.nilai_total >= ?", "70")
	// } else {
	// 	data.Where("t1.nilai_total < ?", "70")
	// }*/
	
	// if search != "" {
	// 	data.Where("lower(t4.nama_jurnal) LIKE ?", "%"+strings.ToLower(search)+"%")
	// }
	// data.Order("tgl_penetapan_akreditasi DESC")
	// data.Find(&res)

	// query := db.Raw("SELECT * from z_arjuna_list_penilaian_blm_selesai_with_filter(( ? ),  ( ? ), ( ? ), '1', ( ? ))", (pages - 1), kode_proses, rows, search)
	query := db.Raw(" SELECT * from z_arjuna_list_penilaian_blm_selesai_with_filter(( 0 ),  ( '-1' ), ( 10000 ), '1', ( '' ))")
	data := db.Table("( ? ) as tb_temp", query)
	data.Find(&res)

	if data.Error != nil {
		return "", data.Error
	}

	xlsx := excelize.NewFile()
	sheet1Name := "Progres Penilaian"
	xlsx.SetSheetName(xlsx.GetSheetName(1), sheet1Name)

	xlsx.SetCellValue(sheet1Name, "A1", "No")
	xlsx.SetCellValue(sheet1Name, "B1", "IDUsulanAkreditasi")
	xlsx.SetCellValue(sheet1Name, "C1", "TglUsulan")
	xlsx.SetCellValue(sheet1Name, "D1", "UrlStatistikPengunjung")
	xlsx.SetCellValue(sheet1Name, "E1", "NamaJurnal")
	xlsx.SetCellValue(sheet1Name, "F1", "Eissn")
	xlsx.SetCellValue(sheet1Name, "G1", "Pissn")
	xlsx.SetCellValue(sheet1Name, "H1", "Publisher")
	xlsx.SetCellValue(sheet1Name, "I1", "Society")
	xlsx.SetCellValue(sheet1Name, "J1", "NamaAwalJurnal")
	
	xlsx.SetCellValue(sheet1Name, "K1", "TglPembuatan")
	xlsx.SetCellValue(sheet1Name, "L1", "UrlJurnal")
	xlsx.SetCellValue(sheet1Name, "M1", "UrlContact")
	xlsx.SetCellValue(sheet1Name, "N1", "UrlEditor")
	xlsx.SetCellValue(sheet1Name, "O1", "Negara")
	xlsx.SetCellValue(sheet1Name, "P1", "Kota")
	xlsx.SetCellValue(sheet1Name, "Q1", "Alamat")
	xlsx.SetCellValue(sheet1Name, "R1", "NoTelepon")
	xlsx.SetCellValue(sheet1Name, "S1", "AlamatSurel")
	xlsx.SetCellValue(sheet1Name, "T1", "TglAkhirTerakreditasi")
	
	xlsx.SetCellValue(sheet1Name, "U1", "NamaPic")
	xlsx.SetCellValue(sheet1Name, "V1", "Nama Institusi")
	xlsx.SetCellValue(sheet1Name, "W1", "Nama Eic")
	xlsx.SetCellValue(sheet1Name, "X1", "Alamat Surel Eic")
	xlsx.SetCellValue(sheet1Name, "Y1", "Alamat oai")
	xlsx.SetCellValue(sheet1Name, "Z1", "Doi Jurnal")
	xlsx.SetCellValue(sheet1Name, "AA1", "total_nilai_mgmt")
	xlsx.SetCellValue(sheet1Name, "AB1", "disinsentif_nilai_mgmt")
	xlsx.SetCellValue(sheet1Name, "AC1", "nilai_detil_mgmt")
	xlsx.SetCellValue(sheet1Name, "AD1", "semua_nilai_mgmt")
	
	xlsx.SetCellValue(sheet1Name, "AE1", "jml_item_nilai_mgmt")
	xlsx.SetCellValue(sheet1Name, "AF1", "jml_item_nilai_mgmt_selesai")
	xlsx.SetCellValue(sheet1Name, "AG1", "total_nilai_issue")
	xlsx.SetCellValue(sheet1Name, "AH1", "disinsentif_nilai_issue")
	xlsx.SetCellValue(sheet1Name, "AI1", "total_nilai")
	xlsx.SetCellValue(sheet1Name, "AJ1", "nilai_detil_issue")
	xlsx.SetCellValue(sheet1Name, "AK1", "semua_nilai_issue")
	xlsx.SetCellValue(sheet1Name, "AL1", "jml_item_nilai_issue")
	xlsx.SetCellValue(sheet1Name, "AM1", "jml_item_nilai_issue_selesai")
	xlsx.SetCellValue(sheet1Name, "AN1", "asesor_mgmt")
	
	xlsx.SetCellValue(sheet1Name, "AO1", "id_asesor_mgmt")
	xlsx.SetCellValue(sheet1Name, "AP1", "tgl_penugasan_asesor_mgmt")
	xlsx.SetCellValue(sheet1Name, "AQ1", "tgl_terima_penugasan_asesor_mgmt")
	xlsx.SetCellValue(sheet1Name, "AR1", "asesor_issue")
	xlsx.SetCellValue(sheet1Name, "AS1", "id_asesor_issue")
	xlsx.SetCellValue(sheet1Name, "AT1", "tgl_penugasan_asesor_issue")
	xlsx.SetCellValue(sheet1Name, "AU1", "tgl_terima_penugasan_asesor_issue")
	xlsx.SetCellValue(sheet1Name, "AV1", "bidang_ilmu")
	xlsx.SetCellValue(sheet1Name, "AW1", "user_penilai")
	xlsx.SetCellValue(sheet1Name, "AX1", "password_penilai")
	
	xlsx.SetCellValue(sheet1Name, "AY1", "rekap_issue")
	xlsx.SetCellValue(sheet1Name, "AZ1", "rekap_url_issue")
	xlsx.SetCellValue(sheet1Name, "BA1", "status_penyesuaian_nilai_issue")
	xlsx.SetCellValue(sheet1Name, "BB1", "status_penyesuaian_nilai_mgmt")
	xlsx.SetCellValue(sheet1Name, "BC1", "frekuensi_terbitan")
	xlsx.SetCellValue(sheet1Name, "BD1", "status_siap_permanen")
	xlsx.SetCellValue(sheet1Name, "BE1", "sts_penyesuaian_issue")
	xlsx.SetCellValue(sheet1Name, "BF1", "sts_penyesuaian_mgmt")
	xlsx.SetCellValue(sheet1Name, "BG1", "sts_akreditasi")
	xlsx.SetCellValue(sheet1Name, "BH1", "nilai_dan_peringkat_sebelumnya")

	err := xlsx.AutoFilter(sheet1Name, "A1", "AA1", "")
	if err != nil {
		return "", err
	}

	for i, V := range res {
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("A%d", i+2), V.No)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("B%d", i+2), V.IDUsulanAkreditasi)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("C%d", i+2), V.TglUsulan)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("D%d", i+2), V.UrlStatistikPengunjung)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("E%d", i+2), V.NamaJurnal)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("F%d", i+2), V.Eissn)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("G%d", i+2), V.Pissn)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("H%d", i+2), V.Publisher)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("I%d", i+2), V.Society)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("J%d", i+2), V.NamaAwalJurnal)
		
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("K%d", i+2), V.TglPembuatan)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("L%d", i+2), V.UrlJurnal)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("M%d", i+2), V.UrlContact)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("N%d", i+2), V.UrlEditor)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("O%d", i+2), V.Country)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("P%d", i+2), V.City)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("Q%d", i+2), V.Alamat)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("R%d", i+2), V.NoTelepon)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("S%d", i+2), V.AlamatSurel)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("T%d", i+2), V.TglAkhirTerakreditasi)
		
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("U%d", i+2), V.NamaPic)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("V%d", i+2), V.NamaInstitusi)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("W%d", i+2), V.NamaEic)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("X%d", i+2), V.AlamatSurelEic)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("Y%d", i+2), V.AlamatOai)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("Z%d", i+2), V.DoiJurnal)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AA%d", i+2), V.TotalNilaiMgmt)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AB%d", i+2), V.DisinsentifNilaiMgmt)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AC%d", i+2), V.NilaiDetilMgmt)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AD%d", i+2), V.SemuaNilaiMgmt)
		
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AE%d", i+2), V.JmlItemNilaiMgmt)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AF%d", i+2), V.JmlItemNilaiMgmtSelesai)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AG%d", i+2), V.TotalNilaiIssue)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AH%d", i+2), V.DisinsentifNilaiIssue)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AI%d", i+2), V.TotalNilai)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AJ%d", i+2), V.NilaiDetilIssue)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AK%d", i+2), V.SemuaNilaiIssue)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AL%d", i+2), V.JmlItemNilaiIssue)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AM%d", i+2), V.JmlItemNilaiIssueSelesai)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AN%d", i+2), V.AsesorMgmt)
		
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AO%d", i+2), V.IDAsesorMgmt)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AP%d", i+2), V.TglPenugasanAsesorMgmt)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AQ%d", i+2), V.TglTerimaPenugasanAsesorMgmt)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AR%d", i+2), V.AsesorIssue)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AS%d", i+2), V.IDAsesorIssue)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AT%d", i+2), V.TglPenugasanAsesorIssue)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AU%d", i+2), V.TglTerimaPenugasanAsesorIssue)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AV%d", i+2), V.BidangIlmu)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AW%d", i+2), V.UserPenilai)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AX%d", i+2), V.PasswordPenilai)
		
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AY%d", i+2), V.RekapIssue)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AZ%d", i+2), V.RekapUrlIssue)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("BA%d", i+2), V.StatusPenyesuaianNilaiIssue)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("BB%d", i+2), V.StatusPenyesuaianNilaiMgmt)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("BC%d", i+2), V.FrekuensiTerbitan)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("BD%d", i+2), V.StatusSiapPermanen)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("BE%d", i+2), V.StsPenyesuaianIssue)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("BF%d", i+2), V.StsPenyesuaianMgmt)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("BG%d", i+2), V.StsAkreditasi)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("BH%d", i+2), V.NilaiDanPeringkatSebelumnya)

	}
	//remark by agus (semua usulan baru diambil baik nilai >=70 or < 70)
	/*var text string	
	if status == "1" {
		text = "Higher70"
	} else {
		text = "Lower70"
	}*/

	//remark by agus (geberate & upload to google storage)
	//namaFile := fmt.Sprintf("ARJUNA-AccreditationResult%s.xlsx", text)
	namaFile := fmt.Sprintf("%s_ARJUNA-ProgresPenilaian.xlsx", time.Now().Format("20060130"))
	 
	excelBytes, err := xlsx.WriteToBuffer()

	// path := "./files/excel/" + namaFile
	// path := helper.UploadFileXls("file/excel/", namaFile,excelBytes)
	// err = xlsx.SaveAs(path)
	// if err != nil {
	// 	return "", err
	// }
	path, err  := helper.UploadFileXls("file/excel/", namaFile, excelBytes.Bytes())
	if err != nil {
		return "", err
	}

	// baseUrl = config.FilesPath()
	result := baseUrl + "/file/excel/" + path
	return result, nil
}


// func ExportKelolaUsulanHasilAkreditasi(search string, status string, baseUrl string) (string, error) {
// 	var res []entity.HasilAkreditasi

// 	db := config.Con()
// 	defer config.CloseCon(db)

// 	data := db.Table("hasil_akreditasi as t1")
// 	data.Distinct("t1.id_usulan_akreditasi, t1.tgl_updated :: date AS tgl_penetapan_akreditasi, t5.url_statistik_pengunjung, t4.nama_jurnal, t4.eissn, t4.pissn, t4.publisher, t4.society, t5.nama_awal_jurnal,t5.tgl_pembuatan, t5.url_jurnal, t5.url_contact, t5.url_editor, t5.country, t5.city, t5.alamat, t5.no_telepon, t5.alamat_surel, t4.alamat_oai, t4.doi_jurnal, t5.tgl_akhir_terakreditasi, t7.nama AS nama_pic, t7.nama_institusi, t8.nama_eic, t8.alamat_surel AS alamat_surel_eic, t1.nilai_total, t1.grade_akreditasi, t10.id_sk_akreditasi, t10.sts_published")
// 	data.Joins("INNER JOIN usulan_akreditasi AS t3 ON t3.id_usulan_akreditasi = t1.id_usulan_akreditasi")
// 	data.Joins("INNER JOIN identitas_jurnal AS t4 ON t4.id_identitas_jurnal = t3.id_identitas_jurnal")
// 	data.Joins("INNER JOIN jurnal AS t5 ON t5.id_jurnal = t4.id_jurnal")
// 	data.Joins("INNER JOIN pic AS t6 ON t6.id_pic = t3.id_pic")
// 	data.Joins("INNER JOIN personal AS t7 ON t7.id_personal = t6.id_personal")
// 	data.Joins("INNER JOIN eic AS t8 ON t8.id_eic = t3.id_eic")
// 	data.Joins("LEFT JOIN penetapan_akreditasi AS t9 ON t9.id_usulan_akreditasi = t1.id_usulan_akreditasi")
// 	data.Joins("LEFT JOIN sk_akreditasi AS t10 ON t10.id_sk_akreditasi = t9.id_sk_akreditasi")
// 	data.Where("t4.nama_jurnal IS NOT NULL")
	
// 	//remark by agus (semua usulan baru diambil baik nilai >=70 or < 70)
// 	/*if status == "1" {
// 		data.Where("t1.nilai_total >= ?", "70")
// 	} else {
// 		data.Where("t1.nilai_total < ?", "70")
// 	}*/
	
// 	if search != "" {
// 		data.Where("lower(t4.nama_jurnal) LIKE ?", "%"+strings.ToLower(search)+"%")
// 	}
// 	data.Order("tgl_penetapan_akreditasi DESC")
// 	data.Find(&res)

// 	if data.Error != nil {
// 		return "", data.Error
// 	}

// 	xlsx := excelize.NewFile()
// 	sheet1Name := "Sheet One"
// 	xlsx.SetSheetName(xlsx.GetSheetName(1), sheet1Name)

// 	xlsx.SetCellValue(sheet1Name, "A1", "ID Usulan Akreditasi")
// 	xlsx.SetCellValue(sheet1Name, "B1", "Tanggal Penetapan")
// 	xlsx.SetCellValue(sheet1Name, "C1", "URL Statistik")
// 	xlsx.SetCellValue(sheet1Name, "D1", "Nama Jurnal")
// 	xlsx.SetCellValue(sheet1Name, "E1", "eISSN")
// 	xlsx.SetCellValue(sheet1Name, "F1", "pISSN")
// 	xlsx.SetCellValue(sheet1Name, "G1", "Penerbit")
// 	xlsx.SetCellValue(sheet1Name, "H1", "Komunitas")
// 	xlsx.SetCellValue(sheet1Name, "I1", "Nama Terbitan Pertama Kali")
// 	xlsx.SetCellValue(sheet1Name, "J1", "Tanggal Pembuatan")
// 	xlsx.SetCellValue(sheet1Name, "K1", "URL Jurnal")
// 	xlsx.SetCellValue(sheet1Name, "L1", "URL Kontak")
// 	xlsx.SetCellValue(sheet1Name, "M1", "URL Editor")
// 	xlsx.SetCellValue(sheet1Name, "N1", "Negara")
// 	xlsx.SetCellValue(sheet1Name, "O1", "Kota")
// 	xlsx.SetCellValue(sheet1Name, "P1", "Alamat")
// 	xlsx.SetCellValue(sheet1Name, "Q1", "Nomor telepon")
// 	xlsx.SetCellValue(sheet1Name, "R1", "Email")
// 	xlsx.SetCellValue(sheet1Name, "S1", "URL OAI")
// 	xlsx.SetCellValue(sheet1Name, "T1", "DOI Jurnal")
// 	xlsx.SetCellValue(sheet1Name, "U1", "Tanggal Akhir Terakreditasi")
// 	xlsx.SetCellValue(sheet1Name, "V1", "Nama PIC Sekarang")
// 	xlsx.SetCellValue(sheet1Name, "W1", "Nama Institusi")
// 	xlsx.SetCellValue(sheet1Name, "X1", "EIC Name")
// 	xlsx.SetCellValue(sheet1Name, "Y1", "EIC Email")
// 	xlsx.SetCellValue(sheet1Name, "Z1", "Total Nilai")
// 	xlsx.SetCellValue(sheet1Name, "AA1", "Grade")

// 	err := xlsx.AutoFilter(sheet1Name, "A1", "AA1", "")
// 	if err != nil {
// 		return "", err
// 	}

// 	for i, V := range res {
// 		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("A%d", i+2), V.IdUsulanAkreditasi)
// 		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("B%d", i+2), V.TglPenetapanAkreditasi)
// 		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("C%d", i+2), V.UrlStatistikPengunjung)
// 		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("D%d", i+2), V.NamaJurnal)
// 		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("E%d", i+2), V.Eissn)
// 		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("F%d", i+2), V.Pissn)
// 		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("G%d", i+2), V.Publisher)
// 		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("H%d", i+2), V.Society)
// 		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("I%d", i+2), V.NamaAwalJurnal)
// 		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("J%d", i+2), V.TglPembuatan)
// 		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("K%d", i+2), V.UrlJurnal)
// 		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("L%d", i+2), V.UrlContact)
// 		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("M%d", i+2), V.UrlEditor)
// 		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("N%d", i+2), V.Country)
// 		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("O%d", i+2), V.City)
// 		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("P%d", i+2), V.Alamat)
// 		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("Q%d", i+2), V.NoTelepon)
// 		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("R%d", i+2), V.AlamatSurel)
// 		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("S%d", i+2), V.AlamatOai)
// 		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("T%d", i+2), V.DoiJurnal)
// 		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("U%d", i+2), V.TglAkhirTerakreditasi)
// 		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("V%d", i+2), V.NamaPic)
// 		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("W%d", i+2), V.NamaInstitusi)
// 		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("X%d", i+2), V.NamaEic)
// 		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("Y%d", i+2), V.AlamatSurelEic)
// 		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("Z%d", i+2), V.NilaiTotal)
// 		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AA%d", i+2), V.GradeAkreditasi)
// 	}
// 	//remark by agus (semua usulan baru diambil baik nilai >=70 or < 70)
// 	/*var text string	
// 	if status == "1" {
// 		text = "Higher70"
// 	} else {
// 		text = "Lower70"
// 	}*/

// 	//namaFile := fmt.Sprintf("ARJUNA-AccreditationResult%s.xlsx", text)
// 	namaFile := fmt.Sprintf("ARJUNA-HasilAkreditasi.xlsx")

// 	path := "./files/excel/" + namaFile
// 	err = xlsx.SaveAs(path)
// 	if err != nil {
// 		return "", err
// 	}
// 	result := baseUrl + "/file/excel/" + namaFile
// 	return result, nil
// }

func ExportSKAkreditasi(status string, baseUrl string) (string, error) {
	var res []entity.SKAkreditasi

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("sk_akreditasi")
	data.Select("*")
	data.Where("sts_published = ?", status)
	data.Order("tgl_sk DESC")
	data.Find(&res)

	if data.Error != nil {
		return "", data.Error
	}

	xlsx := excelize.NewFile()
	sheet1Name := "Sheet One"
	xlsx.SetSheetName(xlsx.GetSheetName(1), sheet1Name)

	xlsx.SetCellValue(sheet1Name, "A1", "ID SK Akreditasi")
	xlsx.SetCellValue(sheet1Name, "B1", "No. Surat Keterangan")
	xlsx.SetCellValue(sheet1Name, "C1", "Judul")
	xlsx.SetCellValue(sheet1Name, "D1", "Rencana Tanggal Terbit")

	err := xlsx.AutoFilter(sheet1Name, "A1", "D1", "")
	if err != nil {
		return "", err
	}

	for i, V := range res {
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("A%d", i+2), V.ID_SK_Akreditasi)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("B%d", i+2), V.NO_SK)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("C%d", i+2), V.JudulSK)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("D%d", i+2), V.TglSK)
	}
	//remark by agus (geberate & upload to google storage)
	 
	// namaFile := fmt.Sprintf("ARJUNA-ListCertificate.xlsx")
	// path := "./files/excel/" + namaFile
	// err = xlsx.SaveAs(path)
	// if err != nil {
	// 	return "", err
	// }
	// result := baseUrl + "/file/excel/" + namaFile
	// return result, nil
	namaFile := fmt.Sprintf("%s_ARJUNA-DaftarRencanaSKTerbit.xlsx", time.Now().Format("20060130"))
	 
	excelBytes, err := xlsx.WriteToBuffer()
	path, err  := helper.UploadFileXls("file/excel/", namaFile, excelBytes.Bytes())
	if err != nil {
		return "", err
	}

	result := baseUrl + "/file/excel/" + path
	return result, nil

}

func ExportListJurnalTerSK(baseUrl string, search string, id_sk string, id_bidang_ilmu string, grade string) (string, error) {
	var res []entity.HasilAkreditasi

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("hasil_akreditasi as t1")
	data.Distinct("t1.id_usulan_akreditasi, t1.tgl_updated :: date AS tgl_penetapan_akreditasi, t5.url_statistik_pengunjung, t4.nama_jurnal, t4.eissn, t4.pissn, t4.publisher, t4.society, t5.nama_awal_jurnal,t5.tgl_pembuatan, t5.url_jurnal, t5.url_contact, t5.url_editor, t5.country, t5.city, t5.alamat, t5.no_telepon, t5.alamat_surel, t4.alamat_oai, t4.doi_jurnal, t5.tgl_akhir_terakreditasi, t7.nama AS nama_pic, t7.nama_institusi, t8.nama_eic, t8.alamat_surel AS alamat_surel_eic, t1.nilai_total, t1.grade_akreditasi, t10.id_sk_akreditasi, t10.sts_published")
	data.Joins("INNER JOIN usulan_akreditasi AS t3 ON t3.id_usulan_akreditasi = t1.id_usulan_akreditasi")
	data.Joins("INNER JOIN identitas_jurnal AS t4 ON t4.id_identitas_jurnal = t3.id_identitas_jurnal")
	data.Joins("INNER JOIN jurnal AS t5 ON t5.id_jurnal = t4.id_jurnal")
	data.Joins("INNER JOIN pic AS t6 ON t6.id_pic = t3.id_pic")
	data.Joins("INNER JOIN personal AS t7 ON t7.id_personal = t6.id_personal")
	data.Joins("INNER JOIN eic AS t8 ON t8.id_eic = t3.id_eic")
	data.Joins("LEFT JOIN penetapan_akreditasi AS t9 ON t9.id_usulan_akreditasi = t1.id_usulan_akreditasi")
	data.Joins("LEFT JOIN sk_akreditasi AS t10 ON t10.id_sk_akreditasi = t9.id_sk_akreditasi")
	data.Joins("LEFT JOIN bidang_ilmu_jurnal as t11 on t11.id_jurnal = t5.id_jurnal")
	data.Where("t4.nama_jurnal IS NOT NULL")
	if id_sk != "" {
		data.Where("t10.id_sk_akreditasi = ?", id_sk)
	}
	if grade != "" {
		data.Where("t1.grade_akreditasi = ?", grade)
	}
	if id_bidang_ilmu != "" {
		data.Where("t11.id_bidang_ilmu = ?", id_bidang_ilmu)
	}
	if search != "" {
		data.Where("lower(t4.nama_jurnal) LIKE ?", "%"+strings.ToLower(search)+"%")
	}
	data.Order("tgl_penetapan_akreditasi DESC")
	data.Find(&res)

	if data.Error != nil {
		return "", data.Error
	}

	xlsx := excelize.NewFile()
	sheet1Name := "Sheet One"
	xlsx.SetSheetName(xlsx.GetSheetName(1), sheet1Name)

	xlsx.SetCellValue(sheet1Name, "A1", "ID Usulan Akreditasi")
	xlsx.SetCellValue(sheet1Name, "B1", "Tanggal Usulan")
	xlsx.SetCellValue(sheet1Name, "C1", "URL Statistik")
	xlsx.SetCellValue(sheet1Name, "D1", "Nama Jurnal")
	xlsx.SetCellValue(sheet1Name, "E1", "Eissn")
	xlsx.SetCellValue(sheet1Name, "F1", "Pissn")
	xlsx.SetCellValue(sheet1Name, "G1", "Penerbit")
	xlsx.SetCellValue(sheet1Name, "H1", "Komunitas")
	xlsx.SetCellValue(sheet1Name, "I1", "Nama Terbitan Pertama Kali")
	xlsx.SetCellValue(sheet1Name, "J1", "Tanggal Pembuatan")
	xlsx.SetCellValue(sheet1Name, "K1", "URL Jurnal")
	xlsx.SetCellValue(sheet1Name, "L1", "URL Kontak")
	xlsx.SetCellValue(sheet1Name, "M1", "URL Editor")
	xlsx.SetCellValue(sheet1Name, "N1", "Negara")
	xlsx.SetCellValue(sheet1Name, "O1", "Kota")
	xlsx.SetCellValue(sheet1Name, "P1", "Alamat")
	xlsx.SetCellValue(sheet1Name, "Q1", "No Telepon")
	xlsx.SetCellValue(sheet1Name, "R1", "Email")
	xlsx.SetCellValue(sheet1Name, "S1", "Tanggal Akhir Akreditasi")
	xlsx.SetCellValue(sheet1Name, "T1", "URL AOI")
	xlsx.SetCellValue(sheet1Name, "U1", "DOI Jurnal")
	xlsx.SetCellValue(sheet1Name, "V1", "Date Of Accreditation")
	xlsx.SetCellValue(sheet1Name, "W1", "Nama PIC Sekarang")
	xlsx.SetCellValue(sheet1Name, "X1", "Nama Institusi")
	xlsx.SetCellValue(sheet1Name, "Y1", "Nama EIC")
	xlsx.SetCellValue(sheet1Name, "Z1", "Email EIC")
	xlsx.SetCellValue(sheet1Name, "AA1", "Total Nilai")
	xlsx.SetCellValue(sheet1Name, "AB1", "Grade")
	xlsx.SetCellValue(sheet1Name, "AC1", "ID Certificate")
	xlsx.SetCellValue(sheet1Name, "AD1", "Published Status")
	// xlsx.SetCellValue(sheet1Name, "AE1", "Proposal Date")
	// xlsx.SetCellValue(sheet1Name, "AF1", "No Surat Keterangan")

	err := xlsx.AutoFilter(sheet1Name, "A1", "AD1", "")
	if err != nil {
		return "", err
	}

	for i, V := range res {

		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("A%d", i+2), V.IdUsulanAkreditasi)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("B%d", i+2), V.TglPenetapanAkreditasi)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("C%d", i+2), V.UrlStatistikPengunjung)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("D%d", i+2), V.NamaJurnal)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("E%d", i+2), V.Eissn)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("F%d", i+2), V.Pissn)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("G%d", i+2), V.NamaPic)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("H%d", i+2), V.Publisher)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("I%d", i+2), V.NamaAwalJurnal)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("J%d", i+2), V.TglPembuatan)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("K%d", i+2), V.UrlJurnal)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("L%d", i+2), V.UrlContact)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("M%d", i+2), V.UrlEditor)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("N%d", i+2), V.Country)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("O%d", i+2), V.City)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("P%d", i+2), V.Alamat)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("Q%d", i+2), V.NoTelepon)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("R%d", i+2), V.AlamatSurelEic)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("S%d", i+2), V.TglAkhirTerakreditasi)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("T%d", i+2), V.AlamatOai)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("U%d", i+2), V.DoiJurnal)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("V%d", i+2), V.TglPembuatan)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("W%d", i+2), V.NamaPic)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("X%d", i+2), V.Publisher)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("Y%d", i+2), V.NamaEic)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("Z%d", i+2), V.AlamatSurelEic)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AA%d", i+2), V.NilaiTotal)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AB%d", i+2), V.GradeAkreditasi)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AC%d", i+2), V.IdSkAkreditasi)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AD%d", i+2), V.StsPublished)
		// xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AE%d", i+2), "Proposal Date")
		// xlsx.SetCellValue(sheet1Name, fmt.Sprintf("AF%d", i+2), "No Surat Keterangan")
	}
	
	//remark by agus (geberate & upload to google storage)
	// namaFile := fmt.Sprintf("ARJUNA-CertifiedJournal.xlsx")
	// path := "./files/excel/" + namaFile
	// err = xlsx.SaveAs(path)
	// if err != nil {
	// 	return "", err
	// }
	// result := baseUrl + "/file/excel/" + namaFile
	// return result, nil
	app := fiber.New()
    app.Use(cors.New(cors.Config{
        AllowHeaders:     "Origin,Content-Type,Accept,Content-Length,Accept-Language,Accept-Encoding,Connection,Access-Control-Allow-Origin",
        AllowOrigins:     "*",
        AllowCredentials: true,
        AllowMethods:     "GET,POST,HEAD,PUT,DELETE,PATCH,OPTIONS",
    }))

	namaFile := fmt.Sprintf("%s_ARJUNA-DaftarJurnalTerSK.xlsx", time.Now().Format("20060130"))
	 
	excelBytes, err := xlsx.WriteToBuffer()
	path, err  := helper.UploadFileXls("file/excel/", namaFile, excelBytes.Bytes())
	if err != nil {
		return "", err
	}

	result := baseUrl + "/file/excel/" + path
	return result, nil
}

func ExportDetailChartPenilail(baseUrl, request, method, bidangIlmu, tahun, idPersonal, record string) (string, error) {
	var res []entity.DashboardDetailAsessor

	db := config.Con()
	defer config.CloseCon(db)

	records, _ := strconv.Atoi(record)

	query := db.Raw("SELECT * FROM arjuna_detail_chart_asessor(( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ))", request, method, bidangIlmu, tahun, idPersonal, 0, records).Scan(&res)

	data := db.Table("(?) as t1")
	data.Distinct("t1.eissn, t1.nama_jurnal, t1.url_jurnal, t1.publisher, t1.issue, t1.url_issue, t1.alasan_tolak, t1.jml_record as total")
	if tahun != "" {
		data = data.Where("t1.tahun = ?", tahun)
	}
	if query.Error != nil {
		return "", query.Error
	}

	xlsx := excelize.NewFile()
	sheet1Name := "Sheet One"
	xlsx.SetSheetName(xlsx.GetSheetName(1), sheet1Name)

	xlsx.SetCellValue(sheet1Name, "A1", "E-ISSN")
	xlsx.SetCellValue(sheet1Name, "B1", "Nama Jurnal")
	xlsx.SetCellValue(sheet1Name, "C1", "URL Jurnal")
	xlsx.SetCellValue(sheet1Name, "D1", "Publisher")
	xlsx.SetCellValue(sheet1Name, "E1", "Issue")
	xlsx.SetCellValue(sheet1Name, "F1", "Url Issue")
	xlsx.SetCellValue(sheet1Name, "G1", "Alasan Tolak")
	xlsx.SetCellValue(sheet1Name, "H1", "Jumlah Record")

	err := xlsx.AutoFilter(sheet1Name, "A1", "H1", "")
	if err != nil {
		return "", err
	}

	for i, v := range res {
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("A%d", i+2), v.Eissn)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("B%d", i+2), v.NamaJurnal)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("C%d", i+2), v.UrlJurnal)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("D%d", i+2), v.Publisher)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("E%d", i+2), v.Issue)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("F%d", i+2), v.UrlIssue)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("G%d", i+2), v.AlasanTolak)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("H%d", i+2), v.JmlRecord)
	}

	var text string
	if request == "1" {
		text = "konten"
	} else if request == "2" {
		text = "manajemen"
	} else {
		text = "evaluator"
	}
	//remark by agus (generate & upload to google storage)
	namaFile := fmt.Sprintf("%s_ARJUNA-DashboardDetailChartPenilai-%s.xlsx", time.Now().Format("20060130"), text)

	// namaFile := fmt.Sprintf("ARJUNA-DashboardDetailChartPenilai-%s.xlsx", text)
	// path := "./files/excel/" + namaFile
	// err = xlsx.SaveAs(path)
	// if err != nil {
	// 	return "", err
	// }
	// result := baseUrl + "/file/excel/" + namaFile
	// return result, nil

	excelBytes, err := xlsx.WriteToBuffer()
 
	path, err  := helper.UploadFileXls("file/excel/", namaFile, excelBytes.Bytes())
	if err != nil {
		return "", err
	}

	result := baseUrl + "/file/excel/" + path
	return result, nil

}

func ExportDetailPengusul(baseUrl, request, method, bidangIlmu, tahun, idPersonal, record string) (string, error) {
	var res []entity.DashboardDetailPengusul

	db := config.Con()
	defer config.CloseCon(db)

	records, _ := strconv.Atoi(record)

	query := db.Raw("SELECT * FROM z_arjuna_detail_dashboard(( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ))", request, method, tahun, idPersonal, 0, records).Scan(&res)

	data := db.Table("(?) as t1")
	data.Distinct("t1.eissn, t1.nama_jurnal, t1.url_jurnal, t1.publisher, t1.progres, t1.total_nilai, t1.grade_akreditasi, t1.jml_record as total")
	if tahun != "" {
		data = data.Where("t1.tahun = ?", tahun)
	}
	if query.Error != nil {
		return "", query.Error
	}

	xlsx := excelize.NewFile()
	sheet1Name := "Sheet One"
	xlsx.SetSheetName(xlsx.GetSheetName(1), sheet1Name)

	xlsx.SetCellValue(sheet1Name, "A1", "E-ISSN")
	xlsx.SetCellValue(sheet1Name, "B1", "Nama Jurnal")
	xlsx.SetCellValue(sheet1Name, "C1", "URL Jurnal")
	xlsx.SetCellValue(sheet1Name, "D1", "Publisher")
	xlsx.SetCellValue(sheet1Name, "E1", "Progres")
	xlsx.SetCellValue(sheet1Name, "F1", "Total Nilai")
	xlsx.SetCellValue(sheet1Name, "G1", "Grade Akreditasi")
	xlsx.SetCellValue(sheet1Name, "H1", "Jumlah Record")

	err := xlsx.AutoFilter(sheet1Name, "A1", "H1", "")
	if err != nil {
		return "", err
	}

	for i, v := range res {
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("A%d", i+2), v.Eissn)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("B%d", i+2), v.NamaJurnal)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("C%d", i+2), v.UrlJurnal)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("D%d", i+2), v.Publisher)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("E%d", i+2), v.Progres)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("F%d", i+2), v.TotalNilai)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("G%d", i+2), v.GradeAkreditasi)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("H%d", i+2), v.JmlRecord)
	}

	var text string
	if request == "1" {
		text = "header"
	} else if request == "2" {
		text = "jurnal-pertahun"
	} else {
		text = "akreditasi-tahunan"
	}

	//remark by agus (generate & upload to google storage)
	namaFile := fmt.Sprintf("%s_ARJUNA-DashboardDetailChartPengusul-%s.xlsx", time.Now().Format("20060130"), text)
 
	// namaFile := fmt.Sprintf("ARJUNA-DashboardDetailChartPengusul-%s.xlsx", text)
	// path := "./files/excel/" + namaFile
	// err = xlsx.SaveAs(path)
	// if err != nil {
	// 	return "", err
	// }
	// result := baseUrl + "/file/excel/" + namaFile
	// return result, nil

	excelBytes, err := xlsx.WriteToBuffer()
 
	path, err  := helper.UploadFileXls("file/excel/", namaFile, excelBytes.Bytes())
	if err != nil {
		return "", err
	}

	result := baseUrl + "/file/excel/" + path
	return result, nil
}
