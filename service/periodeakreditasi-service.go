package service

import (
	"arjuna-api/config"
	"arjuna-api/entity"
	"arjuna-api/helper"
	"strconv"
	"strings"

	"github.com/vcraescu/go-paginator/v2"
	"github.com/vcraescu/go-paginator/v2/adapter"
	"github.com/vcraescu/go-paginator/v2/view"
)

func ListPeriodeAkreditasi(search string, page string, row string) (helper.ResponsePagination, error) {
	var periode_akrediasi []entity.PeriodeAkreditasi

	rows, _ := strconv.Atoi(row)
	pages, _ := strconv.Atoi(page)
	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("periode_akreditasi")
	data.Select("*")
	if search != "" {
		data.Where("lower(periode) LIKE ?", "%"+strings.ToLower(search)+"%")
	}
	data.Order("id_periode_akreditasi desc")
	data.Find(&periode_akrediasi)
	if data.Error != nil {
		return helper.ResponsePagination{}, data.Error
	}

	total := len(periode_akrediasi)
	p := paginator.New(adapter.NewGORMAdapter(data), rows)
	p.SetPage(pages)
	view := view.New(p)
	if err := p.Results(&periode_akrediasi); err != nil {
		panic(err)
	}

	res := helper.BuildPaginationResponse(periode_akrediasi, view)
	res.PerPage = rows
	res.Total = total
	return res, nil
}

func GetPeriodeAkreditasiActive() (entity.PeriodeAkreditasi, error) {
	var periode_akrediasi entity.PeriodeAkreditasi

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("periode_akreditasi")
	data.Select("*")
	data.Where("sts_aktif_periode_akreditasi = ?", "1")
	data.Order("id_periode_akreditasi desc")
	data.Take(&periode_akrediasi)
	if data.Error != nil {
		return entity.PeriodeAkreditasi{}, data.Error
	}
	return periode_akrediasi, nil
}

func InsertPeriodeAkreditasi(req entity.PeriodeAkreditasiDTO) (entity.PeriodeAkreditasi, error) {
	var periode_akrediasi entity.PeriodeAkreditasi
	db := config.Con()
	defer config.CloseCon(db)
	db.Table("periode_akreditasi").Where("sts_aktif_periode_akreditasi = ?", "1").Update("sts_aktif_periode_akreditasi", "0")
	periode_akrediasi.Tahun = req.Tahun
	periode_akrediasi.Periode = req.Periode
	periode_akrediasi.StsAktifPeriodeAkreditasi = "1"

	Saveperiodeakreditasi := db.Table("periode_akreditasi").Create(&periode_akrediasi)

	if Saveperiodeakreditasi.Error != nil {
		return entity.PeriodeAkreditasi{}, Saveperiodeakreditasi.Error
	}

	return periode_akrediasi, nil
}

func ShowPeriodeAkreditasi(id string, baseUrl string) (interface{}, error) {
	var periode_akrediasi entity.PeriodeAkreditasi
	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("periode_akreditasi")
	data.Select("*")
	data.Where("id_periode_akreditasi = ?", id)
	data.Take(&periode_akrediasi)
	if data.Error != nil {
		return nil, data.Error
	}
	return periode_akrediasi, nil
}

func UpdatePeriodeAkreditasi(req entity.PeriodeAkreditasiDTO, old entity.PeriodeAkreditasi, id int64) (entity.PeriodeAkreditasi, error) {
	var periode_akrediasi entity.PeriodeAkreditasi
	db := config.Con()
	defer config.CloseCon(db)

	periode_akrediasi.IDPeriodeAkreditasi = id
	periode_akrediasi.Tahun = req.Tahun
	periode_akrediasi.Periode = req.Periode
	periode_akrediasi.StsAktifPeriodeAkreditasi = old.StsAktifPeriodeAkreditasi

	saveperiodeakreditasi := db.Table("periode_akreditasi").Where("id_periode_akreditasi = ?", id).Save(&periode_akrediasi)

	if saveperiodeakreditasi.Error != nil {
		return entity.PeriodeAkreditasi{}, saveperiodeakreditasi.Error
	}

	return periode_akrediasi, nil
}

func UpdateStatusPeriodeAkreditasi(req entity.PeriodeAkreditasiStatusDTO, old entity.PeriodeAkreditasi, id int64) (entity.PeriodeAkreditasi, error) {
	db := config.Con()
	defer config.CloseCon(db)

	if req.Status == "0" {
		updateStatus := db.Table("periode_akreditasi").Where("id_periode_akreditasi = ?", id).Update("sts_aktif_periode_akreditasi", req.Status)
		if updateStatus.Error != nil {
			return entity.PeriodeAkreditasi{}, updateStatus.Error
		}
	} else {
		db.Table("periode_akreditasi").Where("sts_aktif_periode_akreditasi = ?", "1").Update("sts_aktif_periode_akreditasi", "0")
		updateStatus := db.Table("periode_akreditasi").Where("id_periode_akreditasi = ?", id).Update("sts_aktif_periode_akreditasi", req.Status)
		if updateStatus.Error != nil {
			return entity.PeriodeAkreditasi{}, updateStatus.Error
		}
	}

	old.StsAktifPeriodeAkreditasi = req.Status
	return old, nil
}
