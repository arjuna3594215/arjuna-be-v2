package service

import (
	"arjuna-api/config"
	"arjuna-api/entity"
	"arjuna-api/helper"
	"strconv"
	"strings"

	"github.com/vcraescu/go-paginator/v2"
	"github.com/vcraescu/go-paginator/v2/adapter"
	"github.com/vcraescu/go-paginator/v2/view"
)

func ListPerguruanTinggi(search, page, row string) (helper.ResponsePagination, error) {
	var pt []entity.PerguruanTinggi

	db := config.Con()
	defer config.CloseCon(db)

	rows, _ := strconv.Atoi(row)
	pages, _ := strconv.Atoi(page)

	data := db.Table("perguruan_tinggi")
	if data.Error != nil {
		return helper.ResponsePagination{}, data.Error
	}
	if search != "" {
		//data.Where("lower(nama_pt) LIKE ?", "%"+strings.ToLower(search)+"%")
		data.Where("lower(nama_pt) LIKE ?", "%"+strings.ToLower(search)+"%").Or("lower(institusi) LIKE ?", "%"+strings.ToLower(search)+"%") 
	}
	data.Order("id_perguruan_tinggi desc")
	data.Find(&pt)

	total := len(pt)
	p := paginator.New(adapter.NewGORMAdapter(data), rows)
	p.SetPage(pages)
	view := view.New(p)
	if err := p.Results(&pt); err != nil {
		return helper.ResponsePagination{}, err
	}

	resp := helper.BuildPaginationResponse(pt, view)
	resp.PerPage = rows
	resp.Total = total

	return resp, nil
}

func InsertPerguruanTinggi(data entity.PerguruanTinggiDTO) bool {
	var res entity.PerguruanTinggi

	db := config.Con()
	defer config.CloseCon(db)

	dataSave := db.Table("perguruan_tinggi")
	dataSave.Create(&data)
	if dataSave.Error != nil {
		return false
	}
	dataSave.Find(&res)
	return true
}
