package service

import (
	"arjuna-api/config"
	"arjuna-api/entity"
)

func ListKewenangan() (l_kewenangan []entity.ListKewenangan, err error) {
	var kewenangan []entity.ListKewenangan
	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("kewenangan")
	data.Where("sts_aktif_kewenangan = ?", "1")
	data.Find(&kewenangan)
	if data.Error != nil {
		return nil, data.Error
	}
	return kewenangan, nil
}

func CheckKewenanganPengguna(id_pengguna int, id_kewenangan int, sts_aktif_kewenangan_pengguna string) bool {
	var kewenangan entity.KewenanganPengguna

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("kewenangan_pengguna")
	data.Where("id_pengguna = ?", id_pengguna)
	data.Where("id_kewenangan = ?", id_kewenangan)
	data.Where("sts_aktif_kewenangan_pengguna = ?", sts_aktif_kewenangan_pengguna)
	data.Take(&kewenangan)
	return !(data.Error == nil)
}

// func InsertKewenaganPengguna(id_pengguna int, id_kewenangan int) (a interface{}, err error) {
// 	var kewenangan entity.KewenanganPengguna

// 	db := config.Con()
// 	defer config.CloseCon(db)

// 	if !CheckKewenanganPengguna(id_pengguna, id_kewenangan, "0") {
// 		data := db.Table("kewenangan_pengguna")
// 		data.Where("id_pengguna = ?", id_pengguna)
// 		data.Where("id_kewenangan = ?", id_kewenangan)
// 		data.Updates()
// 	}
// }
