package service

import (
	"arjuna-api/config"
	"arjuna-api/entity"
)

func ListKota() ([]entity.Kota, error) {
	var res []entity.Kota
	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("kota")
	data.Find(&res)
	if data.Error != nil {
		return nil, data.Error
	}
	return res, nil
}

func ListCountry() ([]entity.Country, error) {
	var res []entity.Country
	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("country")
	data.Find(&res)
	if data.Error != nil {
		return nil, data.Error
	}
	return res, nil
}

func getCountryByCode(code string) string {
	var res string
	db := config.Con()
	defer config.CloseCon(db)

	data := db.Select("country").Table("country").Where("code_country = ?", code).Take(&res)
	data.Find(&res)
	if data.Error != nil {
		return ""
	}
	return res
}

func getKotaByCode(code string) string {
	var res string
	db := config.Con()
	defer config.CloseCon(db)

	data := db.Select("kota").Table("kota").Where("kode_kota = ?", code).Take(&res)
	data.Find(&res)
	if data.Error != nil {
		return ""
	}
	return res
}
