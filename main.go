package main

import (
	"arjuna-api/config"
	"arjuna-api/docs"
	"arjuna-api/router"
	"log"
)

// @securityDefinitions.apikey  bearerAuth
// @in                          header
// @name                        Authorization
func main() {
	docs.SwaggerInfo.Title = "ARJUNA API"
	// docs.SwaggerInfo.Description = "This is a API for LMS Loan Market Indonesia"
	docs.SwaggerInfo.Version = "1.0"
	// docs.SwaggerInfo.Host = "localhost:8080"
	docs.SwaggerInfo.Host = "https://arjuna-api-zmltmhkk4a-et.a.run.app"
	docs.SwaggerInfo.BasePath = "/api"
	docs.SwaggerInfo.Schemes = []string{"http", "https"}

	defer config.CloseCon(config.Con())

	router := router.Router()
	log.Fatal(router.Listen(":8080"))
}
