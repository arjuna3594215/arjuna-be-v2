package entity

import (
	"mime/multipart"
	"time"
)

type Download struct {
	ID            int64     `json:"id" gorm:"default:null"`
	DateCreated   time.Time `json:"date_created"`
	Imagename     string    `json:"imagename" gorm:"default:null"`
	Kategori      string    `json:"kategori"`
	Lampiran1     string    `json:"lampiran1" gorm:"default:null"`
	Lampiran2     string    `json:"lampiran2" gorm:"default:null"`
	Lampiran3     string    `json:"lampiran3" gorm:"default:null"`
	Status        string    `json:"status,omitempty" gorm:"->"`
	MostViewed    string    `json:"most_viewed,omitempty" gorm:"->"`
	Title         string    `json:"title" gorm:"->"`
	Isi           string    `json:"isi" gorm:"->"`
	Lang          string    `json:"lang" gorm:"->"`
	IDLang        int64     `json:"id_lang,omitempty" gorm:"->"`
	PathImagename string    `json:"path_imagename" gorm:"->"`
	PathLampiran1 string    `json:"path_lampiran1" gorm:"->"`
	PathLampiran2 string    `json:"path_lampiran2" gorm:"->"`
	PathLampiran3 string    `json:"path_lampiran3" gorm:"->"`
}

type DownloadLang struct {
	// IDLang   int64  `json:"id_lang"`
	ID       int64  `json:"id"`
	Title    string `json:"title"`
	Snapshot string `json:"snapshot"`
	Isi      string `json:"isi"`
	Lang     string `json:"lang"`
}

type DownloadDTO struct {
	Image *multipart.FileHeader `json:"image" form:"image"`
	Title string                `json:"title" form:"title" validate:"required"`
	Lang  string                `json:"lang" form:"lang" validate:"required"`
}

type DownloadStatusDTO struct {
	Status string `json:"status" form:"status" validate:"required"`
}
