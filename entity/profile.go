package entity

type ProfileDTO struct {
	Nama          string   `json:"nama" validate:"required"`
	NamaInstitusi string   `json:"nama_institusi" validate:"required"`
	Email         string   `json:"email" validate:"required,email"`
	NOHandPhone   string   `json:"no_hand_phone" validate:"required,min=11"`
	BidangIlmu    []string `json:"bidang_ilmu"`
}
