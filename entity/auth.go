package entity

type LoginDTO struct {
	Username string `json:"username" validate:"required"`
	Password string `json:"password" validate:"required"`
}

type Login struct {
	IDPersonal int64            `json:"id_personal"`
	IDPengguna int64            `json:"id_pengguna"`
	Username   string           `json:"username"`
	Nama       string           `json:"nama"`
	Image      string           `json:"image"`
	Kewenangan []ListKewenangan `json:"kewenangan" gorm:"foreignKey:IDPengguna;references:IDPengguna"`
	Token      string           `json:"token"`
}

type RegisterDTO struct {
	Nama          string `json:"nama" validate:"required"`
	NamaInstitusi string `json:"nama_institusi" validate:"required"`
	Email         string `json:"email" validate:"required,email"`
	NOHandphone   string `json:"no_handphone" validate:"required,min=11"`
	Username      string `json:"username" validate:"required,min=5"`
	Password      string `json:"password" validate:"required,min=8"`
}

type ForgotPassDTO struct {
	Email string `json:"email" validate:"required,email"`
}

type DataMailForgotPass struct {
	Name     string
	Username string
	Password string
	Email    string
}

type DataMailRegistration struct {
	Name     string
	Email    string
	Username string
	Date     int
}

type ChangePasswordDTO struct {
	OldPassword        string `json:"old_password" validate:"required,min=8"`
	NewPassword        string `json:"new_password" validate:"required,min=8"`
	ConfirmNewPassword string `json:"confirm_password" validate:"required,min=8"`
}
