package entity

import "time"

type SKAkreditasi struct {
	ID_SK_Akreditasi    int64     `json:"id_sk_akreditasi" gorm:"default:null"`
	NO_SK               string    `json:"no_sk"`
	JudulSK             string    `json:"judul_sk"`
	TglSK               string    `json:"tgl_sk"`
	StsPublished        string    `json:"sts_published"`
	TglCreated          time.Time `json:"-" gorm:"<-:create,default:current_timestamp"`
	TglUpdated          time.Time `json:"-" gorm:"<-:update,default:current_timestamp ON update current_timestamp"`
	IDPenandaTanganSK   *string   `json:"id_penanda_tangan_sk"`
	IDPeriodeAkreditasi int64     `json:"id_periode_akreditasi"`
	JumlahJurnal        int64     `json:"jumlah_jurnal" gorm:"->"`
	IDJurnal            int64     `json:"id_jurnal" gorm:"->"`
}

type SKAkreditasiDTO struct {
	NO_SK   string `json:"no_sk" validate:"required"`
	JudulSk string `json:"judul_sk" validate:"required"`
	TglSk   string `json:"tgl_sk" validate:"required"`
}

type TerakreditasiBelumSK struct {
	IDPenetapanAkreditasi  int64   `json:"id_penetapan_akreditasi"`
	IDUsulanAkreditasi     int64   `json:"id_usulan_akreditasi"`
	IDIdentitasJurnal      int64   `json:"id_identitas_jurnal"`
	TglPenetapanAkreditasi string  `json:"tgl_penetapan_akreditasi"`
	IDPic                  int64   `json:"id_pic"`
	IDEic                  int64   `json:"id_eic"`
	NamaJurnal             string  `json:"nama_jurnal"`
	URLJurnal              string  `json:"url_jurnal"`
	URLContact             string  `json:"url_contact"`
	URLEditor              string  `json:"url_editor"`
	Publisher              string  `json:"publisher"`
	Alamat                 string  `json:"alamat"`
	City                   string  `json:"city"`
	Country                string  `json:"country"`
	NoTelepon              string  `json:"no_telepon"`
	AlamatSurel            string  `json:"alamat_surel"`
	Eissn                  string  `json:"eissn"`
	Pissn                  string  `json:"pissn"`
	URLStatistikPengunjung string  `json:"url_statistik_pengunjung"`
	AlamatOai              string  `json:"alamat_oai"`
	DoiJurnal              string  `json:"doi_jurnal"`
	TglCreated             string  `json:"tgl_created"`
	TglUpdated             string  `json:"tgl_updated"`
	NilaiTotal             float32 `json:"nilai_total"`
	GradeAkreditasi        string  `json:"grade_akreditasi"`
	NilaiSA                float32 `json:"nilai_sa"`
	GradeSA                string  `json:"grade_sa"`
	JmlRecord              int     `json:"jml_record"`
}

type SetAkreditasiDTO struct {
	IDPenetapanAkreditasi int64  `json:"id_penetapan_akreditasi"  validate:"required"`
	IDSKAkreditasi        int    `json:"id_sk_akreditasi" validate:"required"`
	URLSinta              string `json:"url_sinta" validate:"required"`
	VolumeAwal            int    `json:"volume_awal" validate:"required"`
	VolumeAkhir           int    `json:"volume_akhir" validate:"required"`
	NomerAwal             int    `json:"nomer_awal" validate:"required"`
	NomerAkhir            int    `json:"nomer_akhir" validate:"required"`
	TahunAwal             int    `json:"tahun_awal" validate:"required"`
	TahunAkhir            int    `json:"tahun_akhir" validate:"required"`
}

type SetAkreditasi struct {
	IDPenetapanAkreditasi int64  `json:"id_penetapan_akreditasi"`
	IDSKAkreditasi        int    `json:"id_sk_akreditasi"`
	URLSinta              string `json:"url_sinta"`
	VolumeAwal            int    `json:"volume_awal"`
	VolumeAkhir           int    `json:"volume_akhir"`
	NomerAwal             int    `json:"nomer_awal"`
	NomerAkhir            int    `json:"nomer_akhir"`
	TahunAwal             int    `json:"tahun_awal"`
	TahunAkhir            int    `json:"tahun_akhir"`
	Status                int    `json:"status"`
}

type UnsetAkreditasiDTO struct {
	IDPenetapanAkreditasi int64 `json:"id_penetapan_akreditasi"`
	IDSKAkreditasi        int   `json:"id_sk_akreditasi"`
}

type UnsetAkreditasi struct {
	IDPenetapanAkreditasi int64 `json:"id_penetapan_akreditasi"`
	IDSKAkreditasi        int   `json:"id_sk_akreditasi"`
	Status                int   `json:"status"`
}
