package entity

import "mime/multipart"

type Penandatangan struct {
	IDPenandaTanganSK        string `json:"id_penanda_tangan_sk" gorm:"default:null"`
	NamaPenandaTanganSK      string `json:"nama_penanda_tangan_sk"`
	NipPenandaTanganSK       string `json:"nip_penanda_tangan_sk"`
	StatusAktifPenandaTangan string `json:"status_aktif_penanda_tangan"`
	File                     string `json:"file"`
	ImagePath                string `json:"image_path" gorm:"->"`
}

type PenandatanganDTO struct {
	Nama string                `json:"nama" form:"nama" validate:"required"`
	Nip  string                `json:"nip" form:"nip" validate:"required"`
	File *multipart.FileHeader `json:"file" form:"file"`
}

type PenandatanganStatusDTO struct {
	Status string `json:"status" form:"status" validate:"required"`
}
