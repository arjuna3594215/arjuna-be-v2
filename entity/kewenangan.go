package entity

type ListKewenangan struct {
	IDPengguna   int64    `json:"id_pengguna,omitempty" gorm:"foreignKey:IDPengguna;references:IDPengguna"`
	IDKewenangan int64    `json:"id_kewenangan"`
	Kewenangan   string   `json:"kewenangan"`
	Permission   []string `json:"permission" gorm:"-"`
}

type Permission struct {
	Name string
}

type KewenanganPengguna struct {
	IDKewenanganPengguna       int64  `json:"id_kewenangan_pengguna"`
	IDPengguna                 int64  `json:"id_pengguna"`
	IDKewenangan               int64  `json:"id_kewenangan"`
	StsAktifKewenanganPengguna string `json:"sts_aktif_kewenangan_pengguna"`
}
