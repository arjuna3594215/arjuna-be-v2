package entity

type PerguruanTinggi struct {
	IDPerguruanTinggi   int    `json:"id_perguruan_tinggi"`
	//KodePT            string `json:"kode_pt"`
	Institusi           string `json:"kode_pt"`
	NamaPT              string `json:"nama_pt"`
}

type PerguruanTinggiDTO struct {
	KodePT string `json:"kode_pt" validate:"required"`
	NamaPT string `json:"nama_pt" validate:"required"`
}
