package entity

import "time"

type UnsurPenilaian struct {
	IDUnsurPenilaian int64  `json:"id_unsur_penilaian"`
	NoUrut           int64  `json:"no_urut"`
	UnsurPenilaian   string `json:"unsur_penilaian"`
}

type SubUnsurPenilaian struct {
	IDSubUnsurPenilaian int64                `json:"id_sub_unsur_penilaian"`
	NoUrut              int64                `json:"no_urut"`
	SubUnsurPenilaian   string               `json:"unsur_penilaian"`
	Answer              interface{}          `json:"answer" gorm:"foreignKey:IDSubUnsurPenilaian;references:IDSubUnsurPenilaian"`
	IndikatorPenilaian  []IndikatorPenilaian `json:"indikator_penilaian" gorm:"foreignKey:IDSubUnsurPenilaian;references:IDSubUnsurPenilaian"`
}

type SubUnsurPenilaianPenugasanPenilaian struct {
	Komentar          string              `json:"komentar"`
	JmlSampleArtikel  int64               `json:"jml_sample_artikel"`
	SubUnsurPenilaian []SubUnsurPenilaian `json:"sub_unsur_penilaian"`
}

type IndikatorPenilaian struct {
	IDSubUnsurPenilaian  int64   `json:"id_sub_unsur_penilaian"`
	IDIndikatorPenilaian int64   `json:"id_indikator_penilaian"`
	NoUrut               int64   `json:"no_urut"`
	Bobot                float64 `json:"bobot"`
	IndikatorPenilaian   string  `json:"indikator_penilaian"`
}

type HasilSa struct {
	IdHasilSa          int64     `json:"id_hasil_sa" gorm:"default:null"`
	IdUsulanAkreditasi int64     `json:"id_usulan_akreditasi"`
	NilaiTotal         float64   `json:"nilai_total"`
	GradeAkreditasi    string    `json:"grade_akreditasi"`
	IDConfigurasi      int64     `json:"-,omitempty"`
	TglUpdated         time.Time `json:"-" gorm:"<-:update,default:current_timestamp ON update current_timestamp"`
}

type HasilPenilaianSa struct {
	IdHasilPenilaianSa  int64     `json:"id_hasil_penilaian_sa" gorm:"default:null"`
	IdUsulanAkreditasi  int64     `json:"id_usulan_akreditasi"`
	IdSubUnsurPenilaian int64     `json:"id_sub_unsur_penilaian"`
	Nilai               float64   `json:"nilai"`
	TglUpdated          time.Time `json:"-" gorm:"<-:update,default:current_timestamp ON update current_timestamp"`
}

type HasilPenilaianSaDTO struct {
	IdUsulanAkreditasi  int64     `json:"id_usulan_akreditasi" validate:"required"`
	IdSubUnsurPenilaian []int64   `json:"id_sub_unsur_penilaian" validate:"required"`
	Nilai               []float64 `json:"nilai" validate:"required"`
}

type PenilaianPenugasanDTO struct {
	IDPenugasanPenilaian int64     `json:"id_penugasan_penilaian" validate:"required"`
	IDUnsurPenilaian     int64     `json:"id_unsur_penilaian" validate:"required"`
	IDIssue              int64     `json:"id_issue"`
	IDSubUnsurPenilaian  []int64   `json:"id_sub_unsur_penilaian" validate:"required"`
	Nilai                []float64 `json:"nilai" validate:"required"`
	IDIndikatorPenilaian []int64   `json:"id_indikator_penilaian" validate:"required"`
	Komentar             string    `json:"komentar" validate:"required"`
	JumlahSample         int64     `json:"jumlah_sample"`
	Penilai              string    `json:"penilai" validate:"required"`
}

type HasilPenilaianMgmt struct {
	IDHasilPenilaianManajemen     int64     `json:"id_hasil_penilaian_manajemen" gorm:"default:null"`
	IDPenugasanPenilaianManajemen int64     `json:"id_penugasan_penilaian_manajemen"`
	IDSubUnsurPenilaian           int64     `json:"id_sub_unsur_penilaian"`
	Nilai                         float64   `json:"nilai"`
	StsPenetapanPenilaian         string    `json:"sts_penetapan_penilaian" gorm:"default:null"`
	TglCreated                    time.Time `json:"-" gorm:"<-:create,default:current_timestamp"`
	TglUpdated                    time.Time `json:"-" gorm:"<-:update,default:current_timestamp ON update current_timestamp"`
	IDIndikatorPenilaian          int64     `json:"id_indikator_penilaian"`
}

type KomentarPenilaianMgmt struct {
	IDKomentarPenilaianMgmt       int64     `json:"id_komentar_penilaian_mgmt" gorm:"default:null"`
	IDPenugasanPenilaianManajemen int64     `json:"id_penugasan_penilaian_manajemen"`
	IDUnsurPenilaian              int64     `json:"id_unsur_penilaian"`
	Komentar                      string    `json:"komentar"`
	TglCreated                    time.Time `json:"-" gorm:"<-:create,default:current_timestamp"`
	TglUpdated                    time.Time `json:"-" gorm:"<-:update,default:current_timestamp ON update current_timestamp"`
}

type HasilPenilaianEvaluator struct {
	IDHasilPenilaianEvaluator     int64     `json:"id_hasil_penilaian_evaluator" gorm:"default:null"`
	IDPenugasanPenilaianEvaluator int64     `json:"id_penugasan_penilaian_evaluator"`
	IDSubUnsurPenilaian           int64     `json:"id_sub_unsur_penilaian"`
	Nilai                         float64   `json:"nilai"`
	StsPenetapanPenilaian         string    `json:"sts_penetapan_penilaian" gorm:"default:null"`
	TglCreated                    time.Time `json:"-" gorm:"<-:create,default:current_timestamp"`
	TglUpdated                    time.Time `json:"-" gorm:"<-:update,default:current_timestamp ON update current_timestamp"`
	IDIndikatorPenilaian          int64     `json:"id_indikator_penilaian"`
}

type KomentarPenilaianEvaluator struct {
	IDKomentarPenilaianEvaluator  int64     `json:"id_komentar_penilaian_evaluator" gorm:"default:null"`
	IDPenugasanPenilaianEvaluator int64     `json:"id_penugasan_penilaian_evaluator"`
	IDUnsurPenilaian              int64     `json:"id_unsur_penilaian"`
	Komentar                      string    `json:"komentar"`
	TglCreated                    time.Time `json:"-" gorm:"<-:create,default:current_timestamp"`
	TglUpdated                    time.Time `json:"-" gorm:"<-:update,default:current_timestamp ON update current_timestamp"`
}

type HasilPenilaianIssue struct {
	IDHasilPenilaianIssue     int64     `json:"id_hasil_penilaian_issue" gorm:"default:null"`
	IDPenugasanPenilaianIssue int64     `json:"id_penugasan_penilaian_issue"`
	IDIssue                   int64     `json:"id_issue" gorm:"default:null"`
	IDSubUnsurPenilaian       int64     `json:"id_sub_unsur_penilaian"`
	Nilai                     float64   `json:"nilai"`
	StsPenetapanPenilaian     string    `json:"sts_penetapan_penilaian" gorm:"default:null"`
	TglCreated                time.Time `json:"-" gorm:"<-:create,default:current_timestamp"`
	TglUpdated                time.Time `json:"-" gorm:"<-:update,default:current_timestamp ON update current_timestamp"`
	IDIndikatorPenilaian      int64     `json:"id_indikator_penilaian"`
}

type KomentarPenilaianIssue struct {
	IDKomentarPenilaianIssue  int64     `json:"id_komentar_penilaian_issue" gorm:"default:null"`
	IDPenugasanPenilaianIssue int64     `json:"id_penugasan_penilaian_issue"`
	IDUnsurPenilaian          int64     `json:"id_unsur_penilaian"`
	Komentar                  string    `json:"komentar"`
	TglCreated                time.Time `json:"-" gorm:"<-:create,default:current_timestamp"`
	TglUpdated                time.Time `json:"-" gorm:"<-:update,default:current_timestamp ON update current_timestamp"`
}

type SubUnsurPenilaianDisinsentif struct {
	IDSubUnsurPenilaianDisinsentif int64                           `json:"id_sub_unsur_penilaian_disinsentif"`
	NoUrut                         int64                           `json:"no_urut"`
	SubUnsurPenilaianDisinsentif   string                          `json:"sub_unsur_penilaian_disinsentif"`
	Answer                         interface{}                     `json:"answer" gorm:"foreignKey:IDSubUnsurPenilaianDisinsentif;references:IDSubUnsurPenilaianDisinsentif"`
	IndikatorPenilaian             []IndikatorPenilaianDisinsentif `json:"indikator_penilaian" gorm:"foreignKey:IDSubUnsurPenilaianDisinsentif;references:IDSubUnsurPenilaianDisinsentif"`
}

type SubUnsurPenilaianPenugasanPenilaianDisinsentif struct {
	Komentar          string                         `json:"komentar"`
	JmlSampleArtikel  int64                          `json:"jml_sample_artikel"`
	SubUnsurPenilaian []SubUnsurPenilaianDisinsentif `json:"sub_unsur_penilaian"`
}

type IndikatorPenilaianDisinsentif struct {
	IDSubUnsurPenilaianDisinsentif  int64   `json:"id_sub_unsur_penilaian_disinsentif"`
	IDIndikatorPenilaianDisinsentif int64   `json:"id_indikator_penilaian_disinsentif"`
	NoUrut                          int64   `json:"no_urut"`
	Bobot                           float64 `json:"bobot"`
	IndikatorPenilaianDisinsentif   string  `json:"indikator_penilaian_disinsentif"`
}

type PenilaianPenugasanDisinsentifDTO struct {
	IDPenugasanPenilaian int64     `json:"id_penugasan_penilaian" validate:"required"`
	IDSubUnsurPenilaian  []int64   `json:"id_sub_unsur_penilaian" validate:"required"`
	Nilai                []float64 `json:"nilai" validate:"required"`
	IDIndikatorPenilaian []int64   `json:"id_indikator_penilaian" validate:"required"`
	Komentar             string    `json:"komentar" validate:"required"`
	Penilai              string    `json:"penilai" validate:"required"`
}

type DisintensifPenilaianIssue struct {
	IDDisinsentifPenilaianIssue int64     `json:"id_disinsentif_penilaian_issue" gorm:"default:null"`
	IDPenugasanPenilaianIssue   int64     `json:"id_penugasan_penilaian_issue"`
	IDSubUnsurDisinsentif       int64     `json:"id_sub_unsur_disinsentif"`
	Nilai                       float64   `json:"nilai"`
	StsPenetapanPenilaian       string    `json:"sts_penetapan_penilaian" gorm:"default:null"`
	TglCreated                  time.Time `json:"-" gorm:"<-:create,default:current_timestamp"`
	TglUpdated                  time.Time `json:"-" gorm:"<-:update,default:current_timestamp ON update current_timestamp"`
	IDIndikatorDisinsentif      int64     `json:"id_indikator_disinsentif"`
}

type KomentarDisintensifPenilaianIssue struct {
	IDKomentarDisinsentifIssue int64     `json:"id_komentar_disinsentif_issue" gorm:"default:null"`
	IDPenugasanPenilaianIssue  int64     `json:"id_penugasan_penilaian_issue"`
	IDSubUnsurDisinsentif      int64     `json:"id_sub_unsur_disinsentif"`
	Komentar                   string    `json:"komentar"`
	TglCreated                 time.Time `json:"-" gorm:"<-:create,default:current_timestamp"`
	TglUpdaated                time.Time `json:"-" gorm:"<-:update,default:current_timestamp ON update current_timestamp"`
}
